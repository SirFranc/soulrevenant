﻿using UnityEngine;
using System.Collections;

public class EyeMovement : MonoBehaviour {

	private Animator mAnimator;
	private float verticaltarget;
	private float horizontaltarget;
	private float vertical;
	private float horizontal;

	// Use this for initialization
	void Start () {
	
		mAnimator = gameObject.GetComponent<Animator> ();
		StartCoroutine (RandomValues ());
	}

	void Update (){
		vertical = Mathf.Lerp (vertical, verticaltarget, Time.deltaTime);
		horizontal = Mathf.Lerp (horizontal, horizontaltarget, Time.deltaTime);

		mAnimator.SetFloat("Vertical",vertical);
		mAnimator.SetFloat("Horizontal",horizontal);

	}


	IEnumerator RandomValues(){
		yield return new WaitForSeconds (Random.Range(0.5f,3f));
		verticaltarget = Random.Range (-1f, 1f);
		horizontaltarget = Random.Range (-1f, 1f);
		StartCoroutine (RandomValues ());
	}
}
