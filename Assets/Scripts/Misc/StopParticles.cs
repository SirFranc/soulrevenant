﻿using UnityEngine;
using System.Collections;

public class StopParticles : MonoBehaviour {

	private CharacterStats mChar;
	private bool PhyisicalRealm;

	public Color SpectralColor;
	
	private Color[] OriginalColor = new Color[15];

	// Use this for initialization
	void Start () {
		mChar = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterStats>();
		ParticleSystem[] mPart = GetComponentsInChildren<ParticleSystem>();
		for(int i = 0; i < mPart.Length; i++){
			if (mPart != null) OriginalColor[i] = mPart[i].startColor;
		}
		if(!mChar.PhyisicalRealm) SetSpectral();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(mChar.PhyisicalRealm && !PhyisicalRealm){
			PhyisicalRealm = true;
			ParticleSystem[] mPart = GetComponentsInChildren<ParticleSystem>();
			for(int i = 0; i < mPart.Length; i++){
				if (mPart != null){
					mPart[i].Play();
					ParticleSystem.Particle[] m_particle = new ParticleSystem.Particle[mPart[i].particleCount];
					mPart[i].GetParticles(m_particle);
					for(int b = 0; b < m_particle.Length; b++){
						m_particle[b].color = OriginalColor[0];
					}
					mPart[i].SetParticles(m_particle,m_particle.Length);
				} 
			}
		}
		else if(!mChar.PhyisicalRealm && PhyisicalRealm) {
			SetSpectral();
		}
	}
	
	void SetSpectral(){
		PhyisicalRealm = false;
		ParticleSystem[] mPart = GetComponentsInChildren<ParticleSystem>();
		for(int i = 0; i < mPart.Length; i++){
			if (mPart != null){
				mPart[i].Pause();
				ParticleSystem.Particle[] m_particle = new ParticleSystem.Particle[mPart[i].particleCount];
				mPart[i].GetParticles(m_particle);
				for(int b = 0; b < m_particle.Length; b++){
					m_particle[b].color = SpectralColor;
				}
				mPart[i].SetParticles(m_particle,m_particle.Length);
			} 
		}
	}
	
}
