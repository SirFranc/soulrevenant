﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(MeshRenderer))]
public class WaterZone : MonoBehaviour {

	private const string Icon = "..//Textures/System/WaterIcon.jpg";

	private BoxCollider mCollision;
	private Material OriginalWaterMaterial;
	private CharacterStats mChar;
	private MeshRenderer mRender;
	
	public Material SpectralMaterial;
	private bool PhyisicalRealm;

	// Use this for initialization
	void Start () {
		mRender = GetComponent<MeshRenderer>();
		OriginalWaterMaterial = mRender.material;
		
		mCollision = GetComponent<BoxCollider>();
		mCollision.isTrigger = true;
		
		mChar = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterStats>();
		if(!mChar.PhyisicalRealm) SetSpectral();
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider Other) {
		if(Other.tag == "Player" && PhyisicalRealm){
			if(!mChar.PowerUps.RahabDefeated){
				mChar.PhyisicalRealm = false;
				mChar.SetRealm();
			}
		}
	}
	
	void FixedUpdate(){
		if(mChar.PhyisicalRealm && !PhyisicalRealm){
			PhyisicalRealm = true;
			mRender.material = OriginalWaterMaterial;
		}
		else if(!mChar.PhyisicalRealm && PhyisicalRealm) {
			SetSpectral();
		}
	}
	
	void SetSpectral(){
		PhyisicalRealm = false;
		mRender.material = SpectralMaterial;
	}
	
	void OnDrawGizmos(){
		Gizmos.DrawIcon(transform.position,Icon);
	}
	
	void OnDrawGizmosSelected(){
		BoxCollider box = GetComponent<BoxCollider>();
		Vector3 Scale = transform.lossyScale;
		Gizmos.color = new Color(0,0,1f,0.5f);
		Gizmos.DrawCube(box.center*Scale.y + transform.position,new Vector3(box.size.x*Scale.x,box.size.y*Scale.y,box.size.z*Scale.z));
	}
}
