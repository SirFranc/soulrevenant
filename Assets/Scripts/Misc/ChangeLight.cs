﻿using UnityEngine;
using System.Collections;

public class ChangeLight : MonoBehaviour {

	public Color OriginalColor;
	public Color SpectralColor;

	private CharacterStats mChar;
	private bool PhyisicalRealm;
	
	// Use this for initialization
	void Start () {
		mChar = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterStats>();
		if(!mChar.PhyisicalRealm) SetSpectral();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(mChar.PhyisicalRealm && !PhyisicalRealm){
			PhyisicalRealm = true;
			Light[] mLight = GetComponentsInChildren<Light>();
			for(int i = 0; i < mLight.Length; i++){
				if (mLight != null) mLight[i].color = OriginalColor;
			}
		}
		else if(!mChar.PhyisicalRealm && PhyisicalRealm) {
			SetSpectral();
		}
	}
	
	void SetSpectral(){
		PhyisicalRealm = false;
		Light[] mLight = GetComponentsInChildren<Light>();
		for(int i = 0; i < mLight.Length; i++){
			if (mLight != null) mLight[i].color = SpectralColor;
		}
	}
}
