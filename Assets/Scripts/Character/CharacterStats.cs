﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
public class CharacterStats : MonoBehaviour {

	public bool PhyisicalRealm = true;

	public int Health = 10;
	public int MaxHealth = 10;
	public int Energy = 0;
	public int MaxEnergy = 0;

	[System.Serializable]
	public class mGlyphs {

	public bool PlaneShift = true;
	public bool PowerGlyph;
	public bool SoundGlyph;
	public bool LightGlyph;
	public bool WaterGlyph;
	public bool ShakeGlyph;

	}
	public mGlyphs Glyphs;

	[System.Serializable]
	public class mPowerUps {

	public bool MelchiahDefeated;
	public bool FirstKainDefeated;
	public bool ZephonDefeated;
	public bool MorlocDefeated;
	public bool RahabDefeated;
	public bool DumahDefeated;
	}
	public mPowerUps PowerUps;

	[Range(0,5)]public int LifeShards;
	[Range(0,5)]public int LifeGlyhps;

	[HideInInspector]public GameObject PhysicalCamera;
	[HideInInspector]public GameObject SpectralCamera;
    [HideInInspector]public CameraSystem CamSystem;

	[HideInInspector]public bool SteppingOnPortal;
	

	void Awake (){
		CamSystem = PhysicalCamera.GetComponentInParent<CameraSystem> ();
	}

	// Use this for initialization
	void Start () {
		if (LifeGlyhps != 0) {
			if (Health == MaxHealth)Health =(10 + LifeGlyhps*5);
			MaxHealth =(10 + LifeGlyhps*5);
		}
		PhysicalCamera = GameObject.FindGameObjectWithTag (Tags.MainCamera);
		SpectralCamera = GameObject.FindGameObjectWithTag (Tags.SpectralCamera);
        

		SetRealm();
	}

	public void UpdateShards () {
		if (LifeShards >= 5) {
			LifeShards = 0;
			LifeGlyhps += 1;
			if (Health == MaxHealth)Health =(10 + LifeGlyhps*5);
			MaxHealth =(10 + LifeGlyhps*5);
		}
	}
	
	void Update(){
		if(Input.GetKeyDown(KeyCode.Alpha1) && SteppingOnPortal){
			PhyisicalRealm = true;
			SetRealm();
		}
		if(Input.GetKeyDown(KeyCode.Alpha2)){
			PhyisicalRealm = false;
			SetRealm();
		}


	}
	
	public void SetRealm(){
		if(PhyisicalRealm){
			
			PhysicalCamera.SetActive(true);
            CamSystem.materialCameraEnvironment.SetActive(true);
			
			SpectralCamera.SetActive(false);
            CamSystem.spectralCameraEnvironment.SetActive(false);
		}
		else if(!PhyisicalRealm){
			
			PhysicalCamera.SetActive(false);
            CamSystem.materialCameraEnvironment.SetActive(false);
			
			SpectralCamera.SetActive(true);
            CamSystem.spectralCameraEnvironment.SetActive(true);
		}
	}
}
