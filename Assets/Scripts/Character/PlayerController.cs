﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//FIXME: climbs steep slopes

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(CharacterStats))]
public class PlayerController : MonoBehaviour 
{
    public float gravity = -20f;
    public float terminalVelocity = -50f;
    public float glideTerminalVelocity = -4f;
    public float breakFallSpeed = -20f;   // Falling speed threshold above which Raziel will pause a moment after landing.
    public float runSpeed = 5f;
    public float runRotateSpeed = 320f;
    public float walkSpeed = 2f;
    public float walkRotateSpeed = 450f;
    public float walkTimeout = 0.5f;
    public float minWalkDiffAngle = 20f;
    public float crawlSpeed = 2f;
    public float crawlRotateSpeed = 300f;
    public float minCrawlDiffAngle = 15f;
    public float standJumpMoveSpeed = 0.5f;
    public float runJumpMoveSpeed = 7f;
    public float jumpSpeed = 9.5f;
    public float highJumpSpeed = 14f;   // TODO: uncrouch highjump timeout
    public float minJumpDiffAngle = 20f;
    public float startJumpRotateSpeed = 1000f;
    public float jumpRotateSpeed = 400f;
    public float glideRotateSpeed = 250f;
    public float glideMoveSpeed = 6f;
    public float wingInflateTimeout = 0.5f;
    public float groundGravityMultiplier = 4.0f;
    public float standToWalkLerpParam = 0.2f;
    public float walkToRunLerpParam = 0.1f;
    public float runToWalkLerpParam = 0.4f;
    public float walkToStandLerpParam = 0.2f;
    public float crouchToCrawlLerpParam = 0.2f;
    public float crawlToCrouchLerpParam = 0.2f;
    public float airSpeedLerpParam = 0.2f;
    public float walkToStandSpeedTolerance = 0.005f;
    public float runToWalkSpeedTolerance = 0.01f;
    public float ledgeFwdRayLength = 0.8f;
    public float ledgeFaceMaxAngle = 40f;
    public float ledgeDownRayLength = 2f;
    public float ledgeDownRayFwdDist = 1.5f;
    public float ledgeTopMaxAngle = 10f;
    public float ledgeRayMinDist = 0.4f;
    public float ledgeRayMaxDist = 0.8f;
    public float hangRotateSpeed = 400f;
    public float hangForwardOffset = 0.4f;
    public float hangDownOffset = 1.3f;
    public float hangLerpParam = 0.3f;
    public float hangTimeout = 0.8f;
    public float pullupTimeout = 1f;

    public bool debugOn = true;

    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode forwardKey = KeyCode.W;
    public KeyCode backwardKey = KeyCode.S;
    public KeyCode leftKey = KeyCode.A;
    public KeyCode rightKey = KeyCode.D;
    public KeyCode crouchKey = KeyCode.LeftShift;
    public KeyCode soulSuckKey = KeyCode.R;

    private CameraSystem cameraSystem;
    private CharacterController charController;
    private CharacterStats stats;

    private bool jumpPressed = false;

    private float stateChangeTime = 0;

    private State state = State.RunningFall;

    private Vector3 ledgeEdge;
    private Vector3 ledgeNormal;
    // TODO: slide down steep slopes
    // TODO: seems to keep "falling" up steep slopes
    private enum State
    {
        Standing,
        Walking,
        Running,
        StopRunning,    // Slowing down after running
        StopWalking,    // Taking last step (or two) after slowing down after walking
        PreJumping,     // Either prepares for or transitions straight to jumping
        RunJumping,
        StandingJump,
        PreHighJumping,
        HighJumping,
        RunningFall,
        StandingFall,
        Landing,
        Gliding,        // TODO: wing inflate delay
        Crouching,
        Crawling,
        Hanging,
        PullingUp,
        Sliding,        // Slide down steep surfaces
        /*
        OnWall,
        ClimbingUp,
        ClimbingDown,
        ClimbingAcross,
        Feeding,
        Attacking11,
        Attacking12,
        Attacking13,
        Attacking21,
        Attacking22,
        Phasing,
        Blocking,
        Flinching,
        Dodging,
        SwitchingToMaterial,
        SwitchingToSpectral,
        Using,
        Dying,
        Floating,
        Swimming,
        LockOn,
        Aiming,
        Throwing,
        HoldingBlock,
        PushingBlock,
        FlippingBlock,
        Telekinesis,
        UseStoneGlyph,
        UseSoundGlyph,
        UseWaterGlyph,
        UseFireGlyph,
        UseSunlightGlyph,
        */
    }

    private static Dictionary<State, string> stateStrings = new Dictionary<State, string>()
    {
        { State.Standing, "Standing" },
        { State.Walking, "Walking" },
        { State.Running, "Running" },
        { State.StopRunning, "Stop Running" },
        { State.StopWalking, "Stop Walking" },
        { State.RunJumping, "Jumping" },
        { State.StandingJump, "Standing Jump" },
        { State.PreJumping, "Pre Jumping" },
        { State.HighJumping, "High Jumping" },
        { State.PreHighJumping, "Pre High Jumping" },
        { State.RunningFall, "Falling" },
        { State.StandingFall, "Standing Fall" },
        { State.Landing, "Landing" },
        { State.Gliding, "Gliding" },
        { State.Crouching, "Crouching" },
        { State.Crawling, "Crawling" },
        { State.Hanging, "Hanging" },
        { State.PullingUp, "Pulling Up" },
        { State.Sliding, "Sliding" },
        /*
        { State.OnWall, "On Wall" },
        { State.ClimbingUp, "Climbing Up" },
        { State.ClimbingDown, "Climbing Down" },
        { State.ClimbingAcross, "Climbing Across" },
        { State.Feeding, "Feeding" },
        { State.Attacking11, "Attacking11" },
        { State.Attacking12, "Attacking12" },
        { State.Attacking13, "Attacking13" },
        { State.Attacking21, "Attacking21" },
        { State.Attacking22, "Attacking22" },
        { State.Phasing, "Phasing" },
        { State.Blocking, "Blocking" },
        { State.Flinching, "Flinching" },
        { State.Dodging, "Dodging" },
        { State.SwitchingToMaterial, "Switching To Material" },
        { State.SwitchingToSpectral, "Switching To Spectral" },
        { State.Using, "Using" },
        { State.Dying, "Dying" },
        { State.Floating, "Floating" },
        { State.Swimming, "Swimming" },
        { State.LockOn, "Lock On" },
        { State.Aiming, "Aiming" },
        { State.Throwing, "Throwing" },
        { State.HoldingBlock, "Holding Block" },
        { State.PushingBlock, "Pushing Block" },
        { State.FlippingBlock, "Flipping Block" },
        { State.Telekinesis, "Telekinesis" },
        { State.UseStoneGlyph, "Use Stone Glyph" },
        { State.UseSoundGlyph, "Use Sound Glyph" },
        { State.UseWaterGlyph, "Use Water Glyph" },
        { State.UseFireGlyph, "Use Fire Glyph" },
        { State.UseSunlightGlyph, "Use Sunlight Glyph" },
        */
    };

    private Dictionary<State, Action> stateActions;



    void Awake()
    {

    }

    void Start()
    {
        charController = this.GetComponent<CharacterController>();
        stats = this.GetComponent<CharacterStats>();
        cameraSystem = stats.CamSystem;

        stateActions = new Dictionary<State, Action>()
        {
            { State.Standing, doStand },
            { State.Walking, doWalk },
            { State.Running, doRun },
            { State.StopRunning, doStopRunning },
            { State.StopWalking, doStopWalking },
            { State.RunJumping, doRunJump },
            { State.StandingJump, doStandingJump },
            { State.PreJumping, doPreJump },
            { State.HighJumping, doHighJump },
            { State.PreHighJumping, doPreHighJump },
            { State.RunningFall, doFall },
            { State.StandingFall, doStandingFall },
            { State.Landing, doLand },
            { State.Gliding, doGlide },
            { State.Crouching, doCrouch },
            { State.Crawling, doCrawl },
            { State.Hanging, doHang },
            { State.PullingUp, doPullUp },
            { State.Sliding, doSlide },
        };
    }

    void setState(State newState, bool doAction)
    {
        stateChangeTime = Time.time;
        state = newState;

        if (newState == State.PullingUp)
        {
            // Snap character to correct height, then try to move forward
            Vector3 moveVector = Vector3.up * (ledgeEdge.y - charController.transform.position.y);
            charController.Move(moveVector);
        }

        if (debugOn)
            Debug.LogFormat("STATE -> {0}", PlayerController.stateStrings[newState]);

        if (doAction)
            stateActions[state]();
    }

    // Continuous input should be fine to be handled in FixedUpdate()
    Vector2 GetInputDirection()
    {
        Vector2 inputDirection = Vector2.zero;

        if (Input.GetKey(leftKey))
            inputDirection.x -= 1;
        if (Input.GetKey(forwardKey))
            inputDirection.y += 1;
        if (Input.GetKey(rightKey))
            inputDirection.x += 1;
        if (Input.GetKey(backwardKey))
            inputDirection.y -= 1;

        if (inputDirection.magnitude > 0.5f)
            inputDirection.Normalize();
        else
            inputDirection = Vector2.zero;

        return inputDirection;
    }

    Vector3 getCamHorizDirection()
    {
        Vector3 camLookHorizDirection = cameraSystem.transform.forward;
        camLookHorizDirection.y = 0;
        camLookHorizDirection.Normalize();

        return camLookHorizDirection;
    }

    Vector3 getCharHorizDirection()
    {
        Vector3 faceDirection = charController.transform.forward;
        faceDirection.y = 0;
        faceDirection.Normalize();

        return faceDirection;
    }

    float getCharHorizSpeed()
    {
        Vector3 currentHorizVelocity = charController.velocity;
        currentHorizVelocity.y = 0;
        float currentHorizSpeed = currentHorizVelocity.magnitude;

        return currentHorizSpeed;
    }

    void resetDiscreteInput()
    {
        jumpPressed = false;
    }

    void rotateTowards(Vector3 targetDirection, float speed)
    {
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        charController.transform.rotation =
            Quaternion.RotateTowards(charController.transform.rotation, targetRotation, speed * Time.deltaTime);
    }

    void smoothChangeGroundSpeed(float targetSpeed, float speedLerpParam)
    {
        Vector3 charFaceDirection = getCharHorizDirection();
        float currentHorizSpeed = getCharHorizSpeed();

        // Move in direction that the character is facing
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, targetSpeed, speedLerpParam);
        // Add gravity to stay on the ground (multiplier ensures that character is grounded while moving down steep steps)
        moveVector += Vector3.up * (gravity * groundGravityMultiplier * Time.deltaTime);

        charController.Move(moveVector * Time.deltaTime);
    }

    void Update()
    {
		GetComponent<Animator>().SetFloat("Speed",getCharHorizSpeed());
        // Handle discrete input events
        // TODO: unity hangs forever after mashing jump
        if (Input.GetKeyDown(jumpKey))
            jumpPressed = true;
    }

    void doStand()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreJumping, true);
            return;
        }

        if (Input.GetKey(crouchKey))
        {
            setState(State.Crouching, true);
            return;
        }

        // If there has been directional input, start walking
        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();

            // Rotate towards input direction
            rotateTowards(targetDirection, walkRotateSpeed);

            // Only start walking if facing more or less the right way, otherwise just rotate
            float diffAngle = Vector3.Angle(charFaceDirection, targetDirection);
            if (diffAngle < minWalkDiffAngle)
            {
                setState(State.Walking, false);
            }
        }
        else
        {
            // Smoothly decrease any remaining movement to zero
            smoothChangeGroundSpeed(0, walkToStandLerpParam);
        }
    }

    void doCrouch()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreHighJumping, true);
            return;
        }

        if (!Input.GetKey(crouchKey))
        {
            setState(State.Standing, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();

            // Rotate towards input direction
            rotateTowards(targetDirection, crawlRotateSpeed);

            // Only start walking if facing more or less the right way, otherwise just rotate
            float diffAngle = Vector3.Angle(charFaceDirection, targetDirection);
            if (diffAngle < minCrawlDiffAngle)
            {
                setState(State.Crawling, false);
            }
        }
        else
        {
            // Smoothly decrease any remaining movement to zero
            smoothChangeGroundSpeed(0, crawlToCrouchLerpParam);
        }
    }

    void doCrawl()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreHighJumping, true);
            return;
        }

        if (!Input.GetKey(crouchKey))
        {
            setState(State.Walking, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();
            float currentHorizSpeed = getCharHorizSpeed();

            // Gradually rotate towards input direction
            rotateTowards(targetDirection, crawlRotateSpeed);

            // Move in direction that the character is facing
            Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, crawlSpeed, crouchToCrawlLerpParam);
            // Add gravity to stay on the ground
            moveVector += Vector3.up * (gravity * groundGravityMultiplier * Time.deltaTime);

            charController.Move(moveVector * Time.deltaTime);
        }
        else
        {
            // If there is no directional input, stop running
            setState(State.Crouching, true);
        }
    }

    void doRunJump()
    {
        if (charController.velocity.y <= 0)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (ledgeDetected())
        {
            setState(State.Hanging, true);
            return;
        }

        if (charController.isGrounded)
        {
            setState(State.Landing, true);
            return;
        }        

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;

            // Gradually rotate towards input direction
            rotateTowards(targetDirection, runRotateSpeed);
        }

        // Move in direction that the character is facing
        Vector3 charFaceDirection = getCharHorizDirection();
        float charHorizSpeed = getCharHorizSpeed();
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(charHorizSpeed, runJumpMoveSpeed, airSpeedLerpParam);
        moveVector += Vector3.up * (charController.velocity.y + gravity * Time.deltaTime);

        charController.Move(moveVector * Time.deltaTime);
    }

    bool ledgeDetected()
    {
        Vector3 forward = getCharHorizDirection();
        Vector3 position = charController.transform.position;

        bool detected = false;

        // See if we are near a wall
        RaycastHit hitinfo = new RaycastHit();
        if (Physics.Raycast(position, forward, out hitinfo, ledgeFwdRayLength, Layers.Collideable))
        {
            Vector3 wallPoint = hitinfo.point;
            Vector3 wallNormal = hitinfo.normal;

            if (Vector3.Angle(forward, -hitinfo.normal) < ledgeFaceMaxAngle)
            {
                // See if we are near a ledge
                if (Physics.Raycast(position + Vector3.up * ledgeDownRayLength + forward * ledgeDownRayFwdDist,
                                    Vector3.down, out hitinfo, ledgeDownRayLength, Layers.Collideable))
                {
                    Vector3 floorPoint = hitinfo.point;

                    if (Vector3.Angle(Vector3.down, -hitinfo.normal) < ledgeTopMaxAngle)
                    {
                        if (hitinfo.distance > ledgeRayMinDist && hitinfo.distance < ledgeRayMaxDist)
                        {
                            detected = true;
                            
                            ledgeEdge = new Vector3(wallPoint.x, floorPoint.y, wallPoint.z);

                            // Ledge normal should be flat, otherwise character might snap to a weird angle.
                            ledgeNormal = wallNormal;
                            ledgeNormal.y = 0;
                            ledgeNormal.Normalize();
                        }
                    }
                }
            }
        }

        if (debugOn)
        {
            Color debugLineColor;
            if (detected)
                debugLineColor = Color.green;
            else
                debugLineColor = Color.red;

            Debug.DrawLine(position, position + forward * ledgeFwdRayLength, debugLineColor);
            Debug.DrawLine(position + forward * ledgeFwdRayLength,
                           position + forward * ledgeFwdRayLength + Vector3.up * ledgeDownRayLength, debugLineColor);
        }

        return detected;
    }

    void tryFollowPath(Vector3[] waypoints, float speed)
    {
        
    }

    void doPullUp()
    {
        // TODO: sometimes gets stuck at wrong height then suddenly snaps to ledge position
        // TODO: sometimes detects ledges incorrectly
        // TODO: wait for certain timeout before moving forward over ledge
        // Move forward over the ledge
        Vector3 moveVector = (ledgeEdge - hangForwardOffset * ledgeNormal) - charController.transform.position;
        charController.Move(5 * moveVector * Time.deltaTime);

        if (Time.time - stateChangeTime > pullupTimeout)
        {
            if (charController.isGrounded)
            {
                setState(State.Standing, true);
            }
            else
            {
                setState(State.StandingFall, true);
            }
        }
    }

    void doHang()
    {
        rotateTowards(-ledgeNormal, hangRotateSpeed);
        charController.transform.position = 
            Vector3.Lerp(charController.transform.position, ledgeEdge + hangForwardOffset * ledgeNormal - hangDownOffset * Vector3.up, hangLerpParam);

        if (Time.time - stateChangeTime > hangTimeout)
        {
            Vector3 inputDirection = GetInputDirection();
            if (inputDirection.magnitude > 0)
            {
                Vector3 camLookHorizDirection = getCamHorizDirection();
                Vector3 targetDirection =
                    camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;

                if (Vector3.Angle(targetDirection, -ledgeNormal) < 45)
                {
                    setState(State.PullingUp, true);
                }
            }
        }
    }

    void doStandingJump()
    {
        if (charController.velocity.y <= 0)
        {
            setState(State.StandingFall, true);
            return;
        }

        if (ledgeDetected())
        {
            setState(State.Hanging, true);
            return;
        }

        if (charController.isGrounded)
        {
            setState(State.Landing, true);
            return;
        }    

        // Move in direction that the character is facing
        Vector3 charFaceDirection = getCharHorizDirection();
        float charHorizSpeed = getCharHorizSpeed();
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(charHorizSpeed, standJumpMoveSpeed, airSpeedLerpParam);
        moveVector += Vector3.up * (charController.velocity.y + gravity * Time.deltaTime);
        
        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;

            moveVector += targetDirection * standJumpMoveSpeed;
        }

        charController.Move(moveVector * Time.deltaTime);
    }

    void doPreJump()
    {
        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();

            // Rotate towards input direction
            rotateTowards(targetDirection, startJumpRotateSpeed);

            // Only start walking if facing more or less the right way, otherwise just rotate
            float diffAngle = Vector3.Angle(charFaceDirection, targetDirection);
            if (diffAngle < minJumpDiffAngle)
            {
                Vector3 moveVector = charFaceDirection * runJumpMoveSpeed;
                moveVector += Vector3.up * (jumpSpeed + gravity * Time.deltaTime);

                charController.Move(moveVector * Time.deltaTime);

                setState(State.RunJumping, false);
            }
        }
        else
        {
            Vector3 charFaceDirection = getCharHorizDirection();
            Vector3 moveVector = charFaceDirection * standJumpMoveSpeed;
            moveVector += Vector3.up * (jumpSpeed + gravity * Time.deltaTime);

            charController.Move(moveVector * Time.deltaTime);

            setState(State.StandingJump, false);
        }
    }

    // TODO: just use doStandingJump, but set flag to show air trails
    void doHighJump()
    {
        if (charController.velocity.y <= 0)
        {
            setState(State.StandingFall, true);
            return;
        }

        if (ledgeDetected())
        {
            setState(State.Hanging, true);
            return;
        }

        if (charController.isGrounded)
        {
            setState(State.Landing, true);
            return;
        }    

        // Move in direction that the character is facing
        Vector3 charFaceDirection = getCharHorizDirection();
        float charHorizSpeed = getCharHorizSpeed();
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(charHorizSpeed, standJumpMoveSpeed, airSpeedLerpParam);
        moveVector += Vector3.up * (charController.velocity.y + gravity * Time.deltaTime);

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;

            moveVector += targetDirection * standJumpMoveSpeed;
        }

        charController.Move(moveVector * Time.deltaTime);
    }

    // TODO: just use doPreJump, but set flag to high jump
    void doPreHighJump()
    {
        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();

            // Rotate towards input direction
            rotateTowards(targetDirection, startJumpRotateSpeed);

            // Only start walking if facing more or less the right way, otherwise just rotate
            float diffAngle = Vector3.Angle(charFaceDirection, targetDirection);
            if (diffAngle < minJumpDiffAngle)
            {
                Vector3 moveVector = charFaceDirection * standJumpMoveSpeed;
                moveVector += Vector3.up * (highJumpSpeed + gravity * Time.deltaTime);

                charController.Move(moveVector * Time.deltaTime);

                setState(State.HighJumping, false);
            }
        }
        else
        {
            Vector3 charFaceDirection = getCharHorizDirection();
            Vector3 moveVector = charFaceDirection * standJumpMoveSpeed;
            moveVector += Vector3.up * (highJumpSpeed + gravity * Time.deltaTime);

            charController.Move(moveVector * Time.deltaTime);

            setState(State.HighJumping, false);
        }
    }

    void doWalk()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreJumping, true);
            return;
        }

        if (Input.GetKey(crouchKey))
        {
            setState(State.Crouching, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();

            // Rotate towards input direction
            rotateTowards(targetDirection, walkRotateSpeed);

            // Only start walking if facing more or less the right way, otherwise just rotate
            float diffAngle = Vector3.Angle(charFaceDirection, targetDirection);
            if (diffAngle < minWalkDiffAngle)
            {
                float currentHorizSpeed = getCharHorizSpeed();

                Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, walkSpeed, standToWalkLerpParam);
                // Add gravity to stay on the ground
                moveVector += Vector3.up * (gravity * groundGravityMultiplier * Time.deltaTime);

                charController.Move(moveVector * Time.deltaTime);
            }

            // If there is still directional input after a short period, start running
            if (Time.time - stateChangeTime > walkTimeout)
            {
                setState(State.Running, false);
            }
        }
        else
        {
            // If directional input stops while walking, slow to a standstill
            setState(State.StopWalking, true);
        }
    }

    void doRun()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreJumping, true);
            return;
        }

        if (Input.GetKey(crouchKey))
        {
            setState(State.Crouching, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            Vector3 charFaceDirection = getCharHorizDirection();
            float currentHorizSpeed = getCharHorizSpeed();

            // Gradually rotate towards input direction
            rotateTowards(targetDirection, runRotateSpeed);

            // Move in direction that the character is facing
            Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, runSpeed, walkToRunLerpParam);
            // Add gravity to stay on the ground
            moveVector += Vector3.up * (gravity * groundGravityMultiplier * Time.deltaTime);
            
            charController.Move(moveVector * Time.deltaTime);
        }
        else
        {
            // If there is no directional input, stop running
            setState(State.StopRunning, true);
        }
    }

    void doStopWalking()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreJumping, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            setState(State.Walking, true);
        }
        else
        {
            smoothChangeGroundSpeed(0, walkToStandLerpParam);

            // If speed is below a certain threshold, transition to standing
            if (getCharHorizSpeed() < walkToStandSpeedTolerance)
            {
                setState(State.Standing, false);
            }
        }
    }

    void doStopRunning()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        if (jumpPressed)
        {
            setState(State.PreJumping, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            setState(State.Running, true);
        }
        else
        {
            smoothChangeGroundSpeed(walkSpeed, runToWalkLerpParam);

            // If we're at walking speed, transition to the 'stop walking' state
            if (getCharHorizSpeed() < walkSpeed + runToWalkSpeedTolerance)
            {
                setState(State.StopWalking, true);
            }
        }
    }

    void doGlide()
    {
        if (charController.isGrounded)
        {
            setState(State.Landing, true);
            return;
        }

        if (!Input.GetKey(jumpKey))
        {
            setState(State.StandingFall, true);
            return;
        }

        if (ledgeDetected())
        {
            setState(State.Hanging, true);
            return;
        }

        Vector2 inputDirection = GetInputDirection();
        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;

            // Gradually rotate towards input direction
            rotateTowards(targetDirection, glideRotateSpeed);
        }

        // Move in direction that the character is facing
        Vector3 charFaceDirection = getCharHorizDirection();
        float currentHorizSpeed = getCharHorizSpeed();
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, glideMoveSpeed, airSpeedLerpParam);
        moveVector += Vector3.up * (charController.velocity.y + gravity * Time.deltaTime);
        moveVector.y = Mathf.Max(moveVector.y, glideTerminalVelocity);

        charController.Move(moveVector * Time.deltaTime);
    }

    void doFall()
    {
        Vector2 inputDirection = GetInputDirection();

        if (charController.isGrounded)
        {
            /*
            Vector3 position = charController.transform.position;

            RaycastHit hitinfo = new RaycastHit();
            if (Physics.Raycast(position, Vector3.down, out hitinfo, 2f, Layers.Collideable))
            {
                Vector3 normal = hitinfo.normal;

                if (Vector3.Angle(normal, Vector3.up) > 45)
                {
                    setState(State.Sliding, true);
                    return;
                }
            }
             * */

            if (charController.velocity.y < breakFallSpeed)
            {
                setState(State.Landing, true);
            }
            else
            {
                if (inputDirection.magnitude > 0)
                {
                    setState(State.Running, true);
                }
                else
                {
                    setState(State.StopRunning, true);
                }
            }

            return;
        }

        if (jumpPressed)
        {
            setState(State.Gliding, true);
            return;
        }

        if (ledgeDetected())
        {
            setState(State.Hanging, true);
            return;
        }

        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;

            // Gradually rotate towards input direction
            rotateTowards(targetDirection, jumpRotateSpeed);
        }

        // Move in direction that the character is facing
        Vector3 charFaceDirection = getCharHorizDirection();
        float currentHorizSpeed = getCharHorizSpeed();
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, runJumpMoveSpeed, airSpeedLerpParam); 
        moveVector += Vector3.up * (charController.velocity.y + gravity * Time.deltaTime);
        moveVector.y = Mathf.Max(moveVector.y, terminalVelocity);

        charController.Move(moveVector * Time.deltaTime);
    }

    void doStandingFall()
    {
        Vector2 inputDirection = GetInputDirection();

        if (charController.isGrounded)
        {
            /*
            Vector3 position = charController.transform.position;

            RaycastHit hitinfo = new RaycastHit();
            if (Physics.Raycast(position, Vector3.down, out hitinfo, 2f, Layers.Collideable))
            {
                Vector3 normal = hitinfo.normal;

                if (Vector3.Angle(normal, Vector3.up) > 45)
                {
                    setState(State.Sliding, true);
                    return;
                }
            }*/

            if (charController.velocity.y < breakFallSpeed)
            {
                setState(State.Landing, true);
            }
            else
            {
                if (inputDirection.magnitude > 0)
                {
                    setState(State.Walking, true);
                }
                else
                {
                    setState(State.Standing, true);
                }
            }

            return;
        }

        if (jumpPressed)
        {
            setState(State.Gliding, true);
            return;
        }

        if (ledgeDetected())
        {
            setState(State.Hanging, true);
            return;
        }

        // Move in direction that the character is facing
        Vector3 charFaceDirection = getCharHorizDirection();
        float currentHorizSpeed = getCharHorizSpeed();
        Vector3 moveVector = charFaceDirection * Mathf.Lerp(currentHorizSpeed, standJumpMoveSpeed, airSpeedLerpParam);

        if (inputDirection.magnitude > 0)
        {
            Vector3 camLookHorizDirection = getCamHorizDirection();
            Vector3 targetDirection =
                camLookHorizDirection * inputDirection.y + cameraSystem.transform.right * inputDirection.x;
            moveVector += targetDirection * standJumpMoveSpeed;
        }

        moveVector += Vector3.up * (charController.velocity.y + gravity * Time.deltaTime);
        moveVector.y = Mathf.Max(moveVector.y, terminalVelocity);

        charController.Move(moveVector * Time.deltaTime);
    }

    void doSlide()
    {
        // If we're not on the ground, start falling
        if (!charController.isGrounded)
        {
            setState(State.RunningFall, true);
            return;
        }

        Vector3 position = charController.transform.position;

        RaycastHit hitinfo = new RaycastHit();
        if (Physics.Raycast(position, Vector3.down, out hitinfo, 2f, Layers.Collideable))
        {
            Vector3 normal = hitinfo.normal;

            if (Vector3.Angle(normal, Vector3.up) < 45)
            {
                setState(State.Standing, true);
                return;
            }

            Vector3 moveVector = normal;
            moveVector.y = 0;
            moveVector.Normalize();
            //moveVector *= 1f;
            //moveVector += Vector3.down * 1f;

            charController.Move(moveVector * Time.time);
        }
    }

    void doLand()
    {
        setState(State.Standing, true);
    }

    void FixedUpdate()
    {
        if (stateActions.ContainsKey(state))
            stateActions[state]();

        resetDiscreteInput();
    }
}
