﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class CharacterControls : MonoBehaviour {
    /*
	public float speed = 6f;				//Velocità normale del personaggio
	public float airSpeed = 4f;
	public float grabbingSpeed = 3f;
	public float turnSmooth = 15f; 			//Velocità della rotazione del player
	public float distanceToFallDown = 5f;
	public bool isGrabbing = false;
	public Vector3 gravity = new Vector3(0,-15,0); 			//Per far cadere il personaggio quando non ha terra sotto di se
	private Transform relativeCamera;	//Movimento relativo alla camera passata


	public bool canMove = true, canRotate = true;
	private bool movementButtonClick, movementButtonPress;

	private Vector3 gravityForce;
	private float initialSpeed, originalSpeed, initialRotateSpeed;
	private Vector3 moveDirection; 				//Vettore che identifica la direzione di movimento del personaggio
	private bool isFallDown = false;
	private bool previousCanDash, check = false;
	private bool movementConsume =true;
	private bool isJumping;

	private Transform mTransform;
	
	private CharacterController mController;

	void Awake () {
		mController = gameObject.GetComponent<CharacterController> ();
		mTransform = GetComponent<Transform>();


		initialSpeed = originalSpeed = speed;
		initialRotateSpeed = turnSmooth;
		gravityForce = Vector3.down;
		relativeCamera = GameObject.FindWithTag (Tags.MainCamera).transform;
	}

	// Use this for initialization
	public void SetMoveDirection(Vector3 direction){
		movementConsume = false;
		moveDirection = direction;
	}
	
	public Transform getRelativeCamera(){
		return relativeCamera;
	}
	
	// Update is called once per frame
	void Update () {
		movementButtonPress = Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0;

		WalkFixedUpdate();

		bool isCollidedDown =(mController.collisionFlags & CollisionFlags.Below) != CollisionFlags.None;
		bool needToFall = !Physics.Raycast(mTransform.position, Vector3.down, distanceToFallDown);
		if(!isFallDown && !isCollidedDown && !isJumping && needToFall){
			isFallDown = true;
		}
		else if(isFallDown && isCollidedDown){
			isFallDown = false;
		}

	}
	void FixedUpdate(){
				if (movementConsume)
						moveDirection = Vector3.zero;
				else
						movementConsume = true;
		
		if (mController.isGrounded) {
						//Se il character è a terra
						gravityForce = Vector3.down;	//Setto la gravità ad un valore minimo di default
						speed = initialSpeed;			//Setto lo speed al valore iniziale
				} else {
						//Se il character è in aria
						speed = airSpeed;							//Setto la velocità uguale al valore airSpeed
						gravityForce += gravity * Time.deltaTime;	//Aggiungo accellerazione alla gravita += gravity
				}
				moveDirection += gravityForce;
		mController.Move (moveDirection * Time.deltaTime);
		}

	public void WalkFixedUpdate () {
		bool isMoving = false;
		if (movementButtonPress) {
			float horizontalAxes = Input.GetAxis ("Horizontal");
			float verticalAxes = Input.GetAxis ("Vertical");
			//grid movement in grabbing
			if (canMove) {
				ApplyMovement (horizontalAxes, verticalAxes);
				isMoving = true;
				
			}
			if (canRotate) 
				ApplyRotation (horizontalAxes, verticalAxes);
		}
		
		
		
		ManageAnimations (isMoving);
	}
	

	//Applica il movimento al  moveDirection
	void ApplyMovement (float horizontal, float vertical) {
		
		Vector3 targetDirection = getRelativeCamera ().transform.TransformDirection (new Vector3 (horizontal, 0f, vertical));
		targetDirection -= new Vector3 (0, targetDirection.y, 0);
		AdjustMoveDirection (targetDirection * speed);
	}
	
	//Applica la rotazione al character
	void ApplyRotation (float horizontal, float vertical) {
		
		Vector3 targetDirection = getRelativeCamera ().transform.TransformDirection (new Vector3 (horizontal, 0f, vertical));
		targetDirection -= new Vector3 (0, targetDirection.y, 0);
		if (targetDirection != Vector3.zero) {
			Quaternion targetRotation = Quaternion.LookRotation (targetDirection, Vector3.up);
			Quaternion newRotation = Quaternion.Lerp (mTransform.rotation, targetRotation, turnSmooth * Time.deltaTime);
			mTransform.rotation = newRotation;
		}
	}
	
	//Setta la giusta animazione in base a isMoving
	void ManageAnimations (bool isMoving) {
		if (isMoving) {
				} 
		else {
				}

	}
	public Vector3 AdjustMoveDirection(Vector3 direction){
		moveDirection += direction;
		return moveDirection;
	}
	public void SetCanWalk(bool canWalk){
		canMove = canWalk;
	}
	
	public void SetCanRotate(bool canRotate){
		canRotate = canRotate;
	}
	public bool CanRotate(){
		return canRotate;
	}
	
	public bool CanMove(){
		return canMove;
	}
     */
}
