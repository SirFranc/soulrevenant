﻿using UnityEngine;
using System.Collections;

public class CameraSystem : MonoBehaviour {
	
	public float camHeightOffset = 1.6f;
	public float camFollowDistance = 3.0f;
	public float mouseXSmoothing = 10.0f;
	public float mouseYSmoothing = 10.0f;
	public float camXSensitivity = 3.0f;
	public float camYSensitivity = 2.0f;
	public float zoomFactor = 2.0f;
	public float camDownTiltAngleThreshold = 30.0f;
	public float camUpTiltAngleThreshold = 120.0f;
	public float minCamFollowDistance = 2.0f;
	public float maxCamFollowDistance = 10.0f;
	public float positionSlerpParam = 0.2f;
	public float rotationSlerpParam = 0.2f;
	public float camOcclusionSideMargin = 0.2f;
	public float camOcclusionBackMargin = 0.2f;
	public float camOcclusionLerpParam = 0.3f;
	public float targetOcclusionLerpBackParam = 0.05f;
	public float mouseDeadzone = 0.01f;
	
	public GameObject materialCameraEnvironment;
	public GameObject spectralCameraEnvironment;
	
	public bool debugOn = true;
	
	private Vector3 targetOffset = Vector3.zero;
	private float mouseXAmount = 0;
	private float mouseYAmount = 0;
	SSAOPro SSAo;
	private GameObject subject;
	
	// Use this for initialization
	void Start()
	{
		subject = GameObject.FindGameObjectWithTag(Tags.Player);
		SSAo = GetComponentInChildren<SSAOPro>();
		if(SSAo != null) SSAo.CutoffDistance = (RenderSettings.fogStartDistance + RenderSettings.fogEndDistance)/2;
		targetOffset = new Vector3(0, 0, camFollowDistance);
		this.transform.position = subject.transform.position + targetOffset;
	}
	
	void FixedUpdate()
	{
		mouseXAmount = Mathf.Lerp(mouseXAmount, camXSensitivity * Input.GetAxis("Mouse X"), mouseXSmoothing * Time.deltaTime);
		mouseYAmount = Mathf.Lerp(mouseYAmount, camYSensitivity * Input.GetAxis("Mouse Y"), mouseYSmoothing * Time.deltaTime);
		float mouseScrollAmount = zoomFactor * Input.GetAxis("Mouse ScrollWheel");
		bool bMouseMoved = Mathf.Abs(mouseXAmount) > 0.01f || Mathf.Abs(mouseYAmount) > 0.01f;
		bool bMouseScrolled = Mathf.Abs(mouseScrollAmount) > 0.01f;
		Vector3 subjectCentre = subject.transform.position + Vector3.up * camHeightOffset;
		
		if (bMouseMoved)
		{
			// Rotate camera using mouse
			// TODO: add panning-only controls like original (good option with gamepad, i.e. shoulder buttons)
			// Pan
			Quaternion panQuat = Quaternion.AngleAxis(mouseXAmount, Vector3.up);
			targetOffset = panQuat * targetOffset;
			// Tilt (clamp between thresholds)
			float camAngle = Vector3.Angle(targetOffset, Vector3.up);
			float tiltAmount = Mathf.Clamp(mouseYAmount, camDownTiltAngleThreshold - camAngle, camUpTiltAngleThreshold - camAngle);
			Vector3 rightVec = Vector3.Cross(Vector3.up, targetOffset);	// targetOffset faces backwards
			Quaternion tiltQuat = Quaternion.AngleAxis(tiltAmount, rightVec);
			targetOffset = tiltQuat * targetOffset;
		}
		else
		{
			// Drag camera behind character
			Vector3 targetHorizOffset = targetOffset;
			targetHorizOffset.y = 0;
			float targetHorizDist = targetHorizOffset.magnitude;
			Vector3 currentHorizOffset = this.transform.position - subject.transform.position;
			currentHorizOffset.y = 0;
			targetOffset = currentHorizOffset.normalized * targetHorizDist + Vector3.up * targetOffset.y;
		}
		
		// Zoom in/out
		if (bMouseScrolled)
			camFollowDistance = Mathf.Clamp(camFollowDistance + mouseScrollAmount, minCamFollowDistance, maxCamFollowDistance);
		
		// If nominal follow distance is longer than the current target offset, this means we are moving back 
		// to the maximum distance after no longer being occluded. In this case, move backwards slowly.
		if (camFollowDistance > targetOffset.magnitude)
			targetOffset = Vector3.Lerp(targetOffset, targetOffset.normalized * camFollowDistance, targetOcclusionLerpBackParam);
		else
			targetOffset = targetOffset.normalized * camFollowDistance;
		
		// Move target position out of occlusion so that the camera ideally moves smoothly towards the
		// non-occluded position.
		RaycastHit raycastHit = new RaycastHit();
		Vector3 targetRayDirection = targetOffset.normalized;
		Vector3 targetRayRight = (Vector3.Cross(targetRayDirection, Vector3.up)).normalized;
		Vector3 targetRayUp = (Vector3.Cross(targetRayRight, targetRayDirection)).normalized;
		Vector3 targetSubjectBtmLeft = subjectCentre - (targetRayUp + targetRayRight) * camOcclusionSideMargin;
		Vector3 targetSubjectBtmRight = subjectCentre - (targetRayUp - targetRayRight) * camOcclusionSideMargin;
		Vector3 targetSubjectTopLeft = subjectCentre + (targetRayUp - targetRayRight) * camOcclusionSideMargin;
		Vector3 targetSubjectTopRight = subjectCentre + (targetRayUp + targetRayRight) * camOcclusionSideMargin;
		float targetRayLength = targetOffset.magnitude + camOcclusionBackMargin;
		if (debugOn)
		{
			Debug.DrawLine(targetSubjectBtmLeft, targetSubjectBtmLeft + targetRayDirection * targetRayLength, Color.yellow);
			Debug.DrawLine(targetSubjectBtmRight, targetSubjectBtmRight + targetRayDirection * targetRayLength, Color.yellow);
			Debug.DrawLine(targetSubjectTopLeft, targetSubjectTopLeft + targetRayDirection * targetRayLength, Color.yellow);
			Debug.DrawLine(targetSubjectTopRight, targetSubjectTopRight + targetRayDirection * targetRayLength, Color.yellow);
		}
		if (Physics.Raycast(targetSubjectBtmLeft, targetRayDirection, out raycastHit, targetRayLength, Layers.Collideable))
			targetRayLength = Mathf.Min(targetRayLength, raycastHit.distance);
		if (Physics.Raycast(targetSubjectBtmRight, targetRayDirection, out raycastHit, targetRayLength, Layers.Collideable))
			targetRayLength = Mathf.Min(targetRayLength, raycastHit.distance);
		if (Physics.Raycast(targetSubjectTopLeft, targetRayDirection, out raycastHit, targetRayLength, Layers.Collideable))
			targetRayLength = Mathf.Min(targetRayLength, raycastHit.distance);
		if (Physics.Raycast(targetSubjectTopRight, targetRayDirection, out raycastHit, targetRayLength, Layers.Collideable))
			targetRayLength = Mathf.Min(targetRayLength, raycastHit.distance);
		targetOffset = targetRayDirection * (targetRayLength - camOcclusionBackMargin);
		
		// Slerp camera towards target position
		Vector3 lerpedCamOffset =
			Vector3.Slerp(this.transform.position - subjectCentre, targetOffset, positionSlerpParam);
		
		// TODO: follow character around corners
		
		//// Handle occlusion
		// If the area about the near clipping plane is intersected, slide the camera forwards.
		//Vector3 clipCentre = currentCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, currentCamera.nearClipPlane));
		Vector3 clipRayDirection = lerpedCamOffset.normalized;
		Vector3 clipRayRight = (Vector3.Cross(clipRayDirection, Vector3.up)).normalized;
		Vector3 clipRayUp = (Vector3.Cross(clipRayRight, clipRayDirection)).normalized;
		Vector3 clipSubjectBtmLeft = subjectCentre - (clipRayUp + clipRayRight) * camOcclusionSideMargin;
		Vector3 clipSubjectBtmRight = subjectCentre - (clipRayUp - clipRayRight) * camOcclusionSideMargin;
		Vector3 clipSubjectTopLeft = subjectCentre + (clipRayUp - clipRayRight) * camOcclusionSideMargin;
		Vector3 clipSubjectTopRight = subjectCentre + (clipRayUp + clipRayRight) * camOcclusionSideMargin;
		float clipRayLength = lerpedCamOffset.magnitude + camOcclusionBackMargin;
		if (debugOn)
		{
			Debug.DrawLine(clipSubjectBtmLeft, clipSubjectBtmLeft + clipRayDirection * clipRayLength, Color.red);
			Debug.DrawLine(clipSubjectBtmRight, clipSubjectBtmRight + clipRayDirection * clipRayLength, Color.red);
			Debug.DrawLine(clipSubjectTopLeft, clipSubjectTopLeft + clipRayDirection * clipRayLength, Color.red);
			Debug.DrawLine(clipSubjectTopRight, clipSubjectTopRight + clipRayDirection * clipRayLength, Color.red);
		}
		if (Physics.Raycast(clipSubjectBtmLeft, clipRayDirection, out raycastHit, clipRayLength, Layers.Collideable))
			clipRayLength = Mathf.Min(clipRayLength, raycastHit.distance);
		if (Physics.Raycast(clipSubjectBtmRight, clipRayDirection, out raycastHit, clipRayLength, Layers.Collideable))
			clipRayLength = Mathf.Min(clipRayLength, raycastHit.distance);
		if (Physics.Raycast(clipSubjectTopLeft, clipRayDirection, out raycastHit, clipRayLength, Layers.Collideable))
			clipRayLength = Mathf.Min(clipRayLength, raycastHit.distance);
		if (Physics.Raycast(clipSubjectTopRight, clipRayDirection, out raycastHit, clipRayLength, Layers.Collideable))
			clipRayLength = Mathf.Min(clipRayLength, raycastHit.distance);
		Vector3 unoccludedCamOffset = clipRayDirection * (clipRayLength - camOcclusionBackMargin);
		
		// Lerp camera towards unoccluded position
		this.transform.position = 
			Vector3.Lerp(lerpedCamOffset, unoccludedCamOffset, camOcclusionLerpParam) + subjectCentre;
		
		// Slerp camera rotation to look in right position
		// Rotate towards target direction, not towards the character (no bouncy tilt effect)
		// TODO: if character grounded, tilt camera when following (i.e. walking up stairs)
		this.transform.rotation =
			Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(-targetOffset, Vector3.up),
			                 rotationSlerpParam);
	}
}