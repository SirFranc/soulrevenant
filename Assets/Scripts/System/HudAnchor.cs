﻿using UnityEngine;
using System.Collections;

public class HudAnchor : MonoBehaviour {

	private const string Icon1 = "../Textures/Misc/placeholder.png";

	public enum mScreenAnchor
	{
		TopLeft		= 0,
		TopRight	= 1,
		BottomLeft	= 2,
		BottomRight	= 3,
		Center		= 4,
	}
	public mScreenAnchor AnchorPoint = mScreenAnchor.TopLeft;

	public float Xoffset;
	public float Yoffset;
	public bool ContinuousUpdate;
	public bool ScaleOnResolution;

	private GameObject MainCamera;
	private Camera mCamera;

	private float ScreenProportion;

	[Range(0,24)]public float CameraDistance = 24;

	public Vector2 ScreenCenter;

	// Use this for initialization
	void Start () {
		MainCamera = GameObject.FindGameObjectWithTag (Tags.MainCamera);
		mCamera = MainCamera.GetComponent<Camera> ();
		transform.position = new Vector3 (0, 0, 0);

		ScreenProportion =  (Screen.width/1920f);
		ScreenCenter = new Vector2 (Screen.width / 2, Screen.height / 2);
		if (ScaleOnResolution) {
			transform.localScale = new Vector3(ScreenProportion,ScreenProportion,ScreenProportion);		
		}
	}


	
	// Update is called once per frame
	void Update () {
		if (ContinuousUpdate) {
			ScreenProportion =  (Screen.width/1920f);
			ScreenCenter = new Vector2 (Screen.width / 2, Screen.height / 2);
			if (ScaleOnResolution) {
				transform.localScale = new Vector3(ScreenProportion,ScreenProportion,ScreenProportion);		
			}

		}

		if (AnchorPoint == mScreenAnchor.Center) {
			Vector3 ScreenPosition = mCamera.ScreenToWorldPoint(new Vector3(ScreenCenter.x + Xoffset ,ScreenCenter.y +Yoffset , CameraDistance));	
			transform.position = ScreenPosition;
		}

		if (AnchorPoint == mScreenAnchor.TopLeft) {
			Vector3 ScreenPosition = mCamera.ScreenToWorldPoint(new Vector3(0 + Xoffset ,Screen.height +Yoffset , CameraDistance));	
			transform.position = ScreenPosition;
		}
		if (AnchorPoint == mScreenAnchor.TopRight) {
			Vector3 ScreenPosition = mCamera.ScreenToWorldPoint(new Vector3(Screen.width + Xoffset ,Screen.height +Yoffset , CameraDistance));	
			transform.position = ScreenPosition;
		}
		if (AnchorPoint == mScreenAnchor.BottomLeft) {
			Vector3 ScreenPosition = mCamera.ScreenToWorldPoint(new Vector3(0 + Xoffset ,0  +Yoffset, CameraDistance));	
			transform.position = ScreenPosition;
		}
		if (AnchorPoint == mScreenAnchor.BottomRight) {
			Vector3 ScreenPosition = mCamera.ScreenToWorldPoint(new Vector3(Screen.width + Xoffset ,0  +Yoffset, CameraDistance));	
			transform.position = ScreenPosition;
		}


	}

	void FixedUpdate (){

	}
	void OnDrawGizmos (){
		Gizmos.DrawIcon (transform.position, Icon1);
	}
}
