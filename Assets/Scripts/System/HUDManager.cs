﻿using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour {

	private GameObject mCamera;

	// Use this for initialization
	void Start () {
		mCamera = GameObject.FindGameObjectWithTag (Tags.MainCamera);
		Transform mCameraTransform = mCamera.transform;
		transform.parent = mCameraTransform;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
