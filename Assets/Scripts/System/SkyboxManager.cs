﻿using UnityEngine;
using System.Collections;

public class SkyboxManager : MonoBehaviour {

	private GameObject mPlayer;

	// Use this for initialization
	void Start () {
		mPlayer = GameObject.FindGameObjectWithTag (Tags.Player);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = mPlayer.transform.position;
	}
}
