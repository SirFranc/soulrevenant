﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CapsuleCollider))]
public class WorldTransition : MonoBehaviour {

	private const string Icon = "..//Textures/System/WorldShiftIcon.jpg";

	private CharacterStats mChar;
	private GameObject Portal;
	private bool PhyisicalRealm;

	// Use this for initialization
	void Start () {
		mChar = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterStats>();
		GetComponent<CapsuleCollider>().isTrigger = true;
		Portal = transform.GetChild(0).gameObject;
		if(!mChar.PhyisicalRealm) SetSpectral();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(mChar.PhyisicalRealm && !PhyisicalRealm){
			PhyisicalRealm = true;
			Portal.SetActive(false);
		}
		else if(!mChar.PhyisicalRealm && PhyisicalRealm) {
			SetSpectral();
		}
	}
	
	void OnTriggerStay(Collider other){
		if(other.tag == "Player" && !PhyisicalRealm && !mChar.SteppingOnPortal){
			mChar.SteppingOnPortal = true;
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.tag == "Player"){
			mChar.SteppingOnPortal = false;
		}
	}
	
	void SetSpectral(){
		PhyisicalRealm = false;
		Portal.SetActive(true);
	}
	
	void OnDrawGizmos(){
		Gizmos.DrawIcon(transform.position,Icon);
	}
}
