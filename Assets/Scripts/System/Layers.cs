﻿public static class Layers {
	
	public const int Underworld = 1 << 8;
	public const int Indoor = 1 << 9;
	public const int Environment = 1 << 10;
	
	public static int Collideable
	{
		get { return Underworld | Indoor | Environment; }
	}
}
