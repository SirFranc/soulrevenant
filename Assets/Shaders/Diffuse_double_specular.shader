Shader "Dixidiasoft/Detailed Specular" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
	_SaturationAmount ("Saturation Amount", Range(0.0, 1.0)) = 1.0
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_Detail ("Detail (normal) (RGB)", 2D) = "gray" {}
	_Detail2 ("Detail (grain)(RGB)", 2D) = "gray" {}
	_Detail3 ("Detail (decal) (RGB)", 2D) = "gray" {}
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 250
	
CGPROGRAM
#pragma target 3.0
#pragma surface surf BlinnPhong

sampler2D _MainTex;
sampler2D _BumpMap;
sampler2D _Detail;
sampler2D _Detail2;
sampler2D _Detail3;
fixed4 _Color;
half _Shininess;
uniform float _SaturationAmount;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap;
	float2 uv_Detail;
	float2 uv_Detail2;
	float2 uv_Detail3;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
											
	c.rgb *= tex2D(_Detail,IN.uv_Detail).rgb*2;
	c.rgb *= tex2D(_Detail2,IN.uv_Detail2).rgb*2;
	
	fixed4 g = tex2D(_Detail3,IN.uv_Detail3) * _Color;
	fixed4 f = (0,0,0,0);

	o.Albedo = lerp(c,g,f);
	o.Gloss = c.a;
	o.Alpha = c.a;
	o.Specular = _Shininess;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	
}
ENDCG
}
Fallback "Diffuse"
}
