Shader "Dixidiasoft/Cliff Shader4Texture" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Detail ("Detail (normal) (RGB)", 2D) = "gray" {}
	_Detail3 ("Detail (grass) (RGB)", 2D) = "gray" {}
	_DetailMask ("Detail (grass mask) (RGB)", 2D) = "gray" {}
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 250
	
CGPROGRAM
#pragma surface surf Lambert
#pragma target 3.0

sampler2D _MainTex;
sampler2D _Detail;
sampler2D _Detail2;
sampler2D _Detail3;
sampler2D _DetailMask;
fixed4 _Color;
uniform float _SaturationAmount;

struct Input {
	float2 uv_MainTex:TEXCOORD0;
	float2 uv_Detail:TEXCOORD1;
	float2 uv_Detail3:TEXCOORD2;
	float2 uv_DetailMask:TEXCOORD3;
};
void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	fixed4 p = tex2D(_Detail, IN.uv_Detail) * 2;	
	
	fixed4 result =c*p;
	
	fixed4 g = tex2D(_Detail3,IN.uv_Detail3) * _Color;
	fixed4 f = tex2D(_DetailMask,IN.uv_DetailMask) * _Color;

	o.Albedo = lerp(result,g,f);

	o.Alpha = c.a;
}
ENDCG
}

//SubShader {
//	Tags { "RenderType"="Opaque" }
//	LOD 250
//	
//CGPROGRAM
//#pragma surface surf Lambert
//
//sampler2D _MainTex;
//sampler2D _Detail;
//sampler2D _Detail2;
//sampler2D _Detail3;
//sampler2D _DetailMask;
//fixed4 _Color;
//uniform float _SaturationAmount;
//
//struct Input {
//	float2 uv_MainTex;
//	float2 uv_Detail;
//	float2 uv_Detail2;
//	float2 uv_Detail3;
//	float2 uv_DetailMask;
//};
//void surf (Input IN, inout SurfaceOutput o) {
//	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
//	fixed4 p = tex2D(_Detail, IN.uv_Detail) * 2;	
//	
//	fixed4 result =c*p;
//	
//	fixed4 g = tex2D(_Detail3,IN.uv_Detail3) * _Color;
//	fixed4 f = tex2D(_DetailMask,IN.uv_DetailMask) * _Color;
//
//	o.Albedo = lerp(result,g,f);
//
//	o.Alpha = c.a;
//}
//ENDCG
//}

Fallback "Diffuse"
}
