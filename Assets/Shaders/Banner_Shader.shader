// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:0,nrsp:0,vomd:0,spxs:False,tesm:0,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:False,qofs:3,qpre:1,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|diff-6744-OUT,normal-90-RGB,clip-4473-A,voffset-7713-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32200,y:32532,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Time,id:7448,x:30940,y:33032,varname:node_7448,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1409,x:32271,y:33336,varname:node_1409,prsc:2|A-9596-OUT,B-4642-OUT;n:type:ShaderForge.SFN_Multiply,id:3421,x:31544,y:33032,varname:node_3421,prsc:2|A-776-OUT,B-2814-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:7041,x:31359,y:33295,varname:node_7041,prsc:2;n:type:ShaderForge.SFN_Slider,id:8,x:31319,y:33455,ptovrint:False,ptlb:Wind Wave,ptin:_WindWave,varname:node_8,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:0.1,max:1;n:type:ShaderForge.SFN_Divide,id:7625,x:31686,y:33263,varname:node_7625,prsc:2|A-7041-X,B-8-OUT;n:type:ShaderForge.SFN_Add,id:5372,x:31846,y:33263,varname:node_5372,prsc:2|A-3421-OUT,B-7625-OUT;n:type:ShaderForge.SFN_Sin,id:9596,x:32030,y:33263,varname:node_9596,prsc:2|IN-5372-OUT;n:type:ShaderForge.SFN_Add,id:7940,x:31877,y:33428,varname:node_7940,prsc:2|A-3421-OUT,B-7759-OUT;n:type:ShaderForge.SFN_Divide,id:7759,x:31712,y:33428,varname:node_7759,prsc:2|A-7041-Z,B-8-OUT;n:type:ShaderForge.SFN_Multiply,id:1090,x:32453,y:33242,varname:node_1090,prsc:2|A-5453-R,B-1409-OUT;n:type:ShaderForge.SFN_Tex2d,id:4473,x:32215,y:32780,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_4473,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:362336d1e588fce4aa402cac5f339627,ntxv:2,isnm:False|UVIN-8043-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:5453,x:32296,y:33088,ptovrint:False,ptlb:DistortionMask,ptin:_DistortionMask,varname:_node_4473_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4a056241e2722dc46a7262a8e7073fd9,ntxv:0,isnm:False|UVIN-1975-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6744,x:32404,y:32668,varname:node_6744,prsc:2|A-4473-RGB,B-1304-RGB;n:type:ShaderForge.SFN_TexCoord,id:8043,x:31955,y:32737,varname:node_8043,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:1975,x:32051,y:33047,varname:node_1975,prsc:2,uv:1;n:type:ShaderForge.SFN_Tex2d,id:90,x:32387,y:32894,ptovrint:False,ptlb:NormalMap,ptin:_NormalMap,varname:node_90,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:150c8d600a0473f47b1b8800cf243bfd,ntxv:3,isnm:True|UVIN-8043-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2814,x:31363,y:33110,varname:node_2814,prsc:2|A-776-OUT,B-7448-T;n:type:ShaderForge.SFN_Slider,id:5935,x:32351,y:33500,ptovrint:False,ptlb:Wave Multiplier,ptin:_WaveMultiplier,varname:node_5935,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:7713,x:32616,y:33242,varname:node_7713,prsc:2|A-1090-OUT,B-5935-OUT;n:type:ShaderForge.SFN_Slider,id:776,x:31023,y:32871,ptovrint:False,ptlb:Wind Power,ptin:_WindPower,varname:_WindWave_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.2,cur:1,max:5;n:type:ShaderForge.SFN_Cos,id:4642,x:32064,y:33419,varname:node_4642,prsc:2|IN-7940-OUT;proporder:1304-8-5453-4473-90-5935-776;pass:END;sub:END;*/

Shader "Shader Forge/Banner_Shader" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _WindWave ("Wind Wave", Range(0.1, 1)) = 0.1
        _DistortionMask ("DistortionMask", 2D) = "white" {}
        _Diffuse ("Diffuse", 2D) = "black" {}
        _NormalMap ("NormalMap", 2D) = "bump" {}
        _WaveMultiplier ("Wave Multiplier", Range(0, 1)) = 0.5
        _WindPower ("Wind Power", Range(0.2, 5)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="Geometry+3"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float _WindWave;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _DistortionMask; uniform float4 _DistortionMask_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _WaveMultiplier;
            uniform float _WindPower;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
            #endif
            #ifdef DYNAMICLIGHTMAP_ON
                o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
            #endif
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
            o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
            float4 _DistortionMask_var = tex2Dlod(_DistortionMask,float4(TRANSFORM_TEX(o.uv1, _DistortionMask),0.0,0));
            float4 node_7448 = _Time + _TimeEditor;
            float node_3421 = (_WindPower*(_WindPower*node_7448.g));
            float node_7713 = ((_DistortionMask_var.r*(sin((node_3421+(mul(unity_ObjectToWorld, v.vertex).r/_WindWave)))*cos((node_3421+(mul(unity_ObjectToWorld, v.vertex).b/_WindWave)))))*_WaveMultiplier);
            v.vertex.xyz += float3(node_7713,node_7713,node_7713);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            float3 lightColor = _LightColor0.rgb;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            UNITY_TRANSFER_FOG(o,o.pos);
            TRANSFER_VERTEX_TO_FRAGMENT(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
            float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
            float3 normalLocal = _NormalMap_var.rgb;
            float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
            
            float nSign = sign( dot( viewDirection, i.normalDir ) ); // Reverse normal if this is a backface
            i.normalDir *= nSign;
            normalDirection *= nSign;
            
            float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
            clip(_Diffuse_var.a - 0.5);
            float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
            float3 lightColor = _LightColor0.rgb;
// Lighting:
            float attenuation = LIGHT_ATTENUATION(i);
            float3 attenColor = attenuation * _LightColor0.xyz;
/// GI Data:
            UnityLight light;
            #ifdef LIGHTMAP_OFF
                light.color = lightColor;
                light.dir = lightDirection;
                light.ndotl = LambertTerm (normalDirection, light.dir);
            #else
                light.color = half3(0.f, 0.f, 0.f);
                light.ndotl = 0.0f;
                light.dir = half3(0.f, 0.f, 0.f);
            #endif
            UnityGIInput d;
            d.light = light;
            d.worldPos = i.posWorld.xyz;
            d.worldViewDir = viewDirection;
            d.atten = attenuation;
            #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                d.ambient = 0;
                d.lightmapUV = i.ambientOrLightmapUV;
            #else
                d.ambient = i.ambientOrLightmapUV;
            #endif
            UnityGI gi = UnityGlobalIllumination (d, 1, 0, normalDirection);
            lightDirection = gi.light.dir;
            lightColor = gi.light.color;
/// Diffuse:
            float NdotL = max(0.0,dot( normalDirection, lightDirection ));
            float3 directDiffuse = max( 0.0, NdotL) * attenColor;
            float3 indirectDiffuse = float3(0,0,0);
            indirectDiffuse += gi.indirect.diffuse;
            float3 diffuseColor = (_Diffuse_var.rgb*_Color.rgb);
            float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
// Final Color:
            float3 finalColor = diffuse;
            fixed4 finalRGBA = fixed4(finalColor,1);
            UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
            return finalRGBA;
        }
        ENDCG
    }
    Pass {
        Name "FORWARD_DELTA"
        Tags {
            "LightMode"="ForwardAdd"
        }
        Blend One One
        Cull Off
        
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_FORWARDADD
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #include "UnityCG.cginc"
        #include "AutoLight.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #pragma multi_compile_fwdadd_fullshadows
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
        #pragma target 3.0
        #pragma glsl
        uniform float4 _TimeEditor;
        uniform float4 _Color;
        uniform float _WindWave;
        uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
        uniform sampler2D _DistortionMask; uniform float4 _DistortionMask_ST;
        uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
        uniform float _WaveMultiplier;
        uniform float _WindPower;
        struct VertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 tangent : TANGENT;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float2 uv0 : TEXCOORD0;
            float2 uv1 : TEXCOORD1;
            float2 uv2 : TEXCOORD2;
            float4 posWorld : TEXCOORD3;
            float3 normalDir : TEXCOORD4;
            float3 tangentDir : TEXCOORD5;
            float3 bitangentDir : TEXCOORD6;
            LIGHTING_COORDS(7,8)
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
            o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
            float4 _DistortionMask_var = tex2Dlod(_DistortionMask,float4(TRANSFORM_TEX(o.uv1, _DistortionMask),0.0,0));
            float4 node_7448 = _Time + _TimeEditor;
            float node_3421 = (_WindPower*(_WindPower*node_7448.g));
            float node_7713 = ((_DistortionMask_var.r*(sin((node_3421+(mul(unity_ObjectToWorld, v.vertex).r/_WindWave)))*cos((node_3421+(mul(unity_ObjectToWorld, v.vertex).b/_WindWave)))))*_WaveMultiplier);
            v.vertex.xyz += float3(node_7713,node_7713,node_7713);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            float3 lightColor = _LightColor0.rgb;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            TRANSFER_VERTEX_TO_FRAGMENT(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
            float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
            float3 normalLocal = _NormalMap_var.rgb;
            float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
            
            float nSign = sign( dot( viewDirection, i.normalDir ) ); // Reverse normal if this is a backface
            i.normalDir *= nSign;
            normalDirection *= nSign;
            
            float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
            clip(_Diffuse_var.a - 0.5);
            float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
            float3 lightColor = _LightColor0.rgb;
// Lighting:
            float attenuation = LIGHT_ATTENUATION(i);
            float3 attenColor = attenuation * _LightColor0.xyz;
/// Diffuse:
            float NdotL = max(0.0,dot( normalDirection, lightDirection ));
            float3 directDiffuse = max( 0.0, NdotL) * attenColor;
            float3 diffuseColor = (_Diffuse_var.rgb*_Color.rgb);
            float3 diffuse = directDiffuse * diffuseColor;
// Final Color:
            float3 finalColor = diffuse;
            return fixed4(finalColor * 1,0);
        }
        ENDCG
    }
    Pass {
        Name "ShadowCaster"
        Tags {
            "LightMode"="ShadowCaster"
        }
        Offset 1, 1
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_SHADOWCASTER
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #include "UnityCG.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #pragma fragmentoption ARB_precision_hint_fastest
        #pragma multi_compile_shadowcaster
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
        #pragma target 3.0
        #pragma glsl
        uniform float4 _TimeEditor;
        uniform float _WindWave;
        uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
        uniform sampler2D _DistortionMask; uniform float4 _DistortionMask_ST;
        uniform float _WaveMultiplier;
        uniform float _WindPower;
        struct VertexInput {
            float4 vertex : POSITION;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            V2F_SHADOW_CASTER;
            float2 uv0 : TEXCOORD1;
            float2 uv1 : TEXCOORD2;
            float2 uv2 : TEXCOORD3;
            float4 posWorld : TEXCOORD4;
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            float4 _DistortionMask_var = tex2Dlod(_DistortionMask,float4(TRANSFORM_TEX(o.uv1, _DistortionMask),0.0,0));
            float4 node_7448 = _Time + _TimeEditor;
            float node_3421 = (_WindPower*(_WindPower*node_7448.g));
            float node_7713 = ((_DistortionMask_var.r*(sin((node_3421+(mul(unity_ObjectToWorld, v.vertex).r/_WindWave)))*cos((node_3421+(mul(unity_ObjectToWorld, v.vertex).b/_WindWave)))))*_WaveMultiplier);
            v.vertex.xyz += float3(node_7713,node_7713,node_7713);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            TRANSFER_SHADOW_CASTER(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
            clip(_Diffuse_var.a - 0.5);
            SHADOW_CASTER_FRAGMENT(i)
        }
        ENDCG
    }
    Pass {
        Name "Meta"
        Tags {
            "LightMode"="Meta"
        }
        Cull Off
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_META 1
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #include "UnityCG.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #include "UnityMetaPass.cginc"
        #pragma fragmentoption ARB_precision_hint_fastest
        #pragma multi_compile_shadowcaster
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
        #pragma target 3.0
        #pragma glsl
        uniform float4 _TimeEditor;
        uniform float4 _Color;
        uniform float _WindWave;
        uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
        uniform sampler2D _DistortionMask; uniform float4 _DistortionMask_ST;
        uniform float _WaveMultiplier;
        uniform float _WindPower;
        struct VertexInput {
            float4 vertex : POSITION;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float2 uv0 : TEXCOORD0;
            float2 uv1 : TEXCOORD1;
            float2 uv2 : TEXCOORD2;
            float4 posWorld : TEXCOORD3;
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            float4 _DistortionMask_var = tex2Dlod(_DistortionMask,float4(TRANSFORM_TEX(o.uv1, _DistortionMask),0.0,0));
            float4 node_7448 = _Time + _TimeEditor;
            float node_3421 = (_WindPower*(_WindPower*node_7448.g));
            float node_7713 = ((_DistortionMask_var.r*(sin((node_3421+(mul(unity_ObjectToWorld, v.vertex).r/_WindWave)))*cos((node_3421+(mul(unity_ObjectToWorld, v.vertex).b/_WindWave)))))*_WaveMultiplier);
            v.vertex.xyz += float3(node_7713,node_7713,node_7713);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
            return o;
        }
        float4 frag(VertexOutput i) : SV_Target {
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            UnityMetaInput o;
            UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
            
            o.Emission = 0;
            
            float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
            float3 diffColor = (_Diffuse_var.rgb*_Color.rgb);
            o.Albedo = diffColor;
            
            return UnityMetaFragment( o );
        }
        ENDCG
    }
}
FallBack "Diffuse"
CustomEditor "ShaderForgeMaterialInspector"
}
