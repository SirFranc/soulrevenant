// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:1,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33378,y:32668,varname:node_9361,prsc:2|normal-345-OUT,emission-588-OUT,custl-8554-OUT,alpha-5933-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8068,x:32302,y:33113,varname:node_8068,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3406,x:32302,y:32979,varname:node_3406,prsc:2;n:type:ShaderForge.SFN_LightVector,id:6869,x:31426,y:32681,varname:node_6869,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9684,x:31426,y:32809,prsc:2,pt:True;n:type:ShaderForge.SFN_HalfVector,id:9471,x:31426,y:32960,varname:node_9471,prsc:2;n:type:ShaderForge.SFN_Dot,id:7782,x:31638,y:32724,cmnt:Lambert,varname:node_7782,prsc:2,dt:1|A-6869-OUT,B-9684-OUT;n:type:ShaderForge.SFN_Dot,id:3269,x:31638,y:32898,cmnt:Blinn-Phong,varname:node_3269,prsc:2,dt:1|A-9684-OUT,B-9471-OUT;n:type:ShaderForge.SFN_Multiply,id:2746,x:32033,y:32893,cmnt:Specular Contribution,varname:node_2746,prsc:2|A-7782-OUT,B-5267-OUT,C-4865-RGB;n:type:ShaderForge.SFN_Tex2d,id:851,x:31732,y:32217,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_851,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ef72f0e604d74e449aaf85d2c919a982,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1941,x:32033,y:32720,cmnt:Diffuse Contribution,varname:node_1941,prsc:2|A-3969-OUT,B-7782-OUT;n:type:ShaderForge.SFN_Color,id:5927,x:31732,y:32402,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5927,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Exp,id:1700,x:31638,y:33081,varname:node_1700,prsc:2,et:1|IN-9978-OUT;n:type:ShaderForge.SFN_Slider,id:5328,x:31097,y:33083,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_5328,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Power,id:5267,x:31836,y:32967,varname:node_5267,prsc:2|VAL-3269-OUT,EXP-1700-OUT;n:type:ShaderForge.SFN_Add,id:2159,x:32302,y:32839,cmnt:Combine,varname:node_2159,prsc:2|A-1941-OUT,B-2746-OUT;n:type:ShaderForge.SFN_Multiply,id:5085,x:32535,y:33003,cmnt:Attenuate and Color,varname:node_5085,prsc:2|A-2159-OUT,B-3406-RGB,C-8068-OUT;n:type:ShaderForge.SFN_ConstantLerp,id:9978,x:31426,y:33083,varname:node_9978,prsc:2,a:1,b:11|IN-5328-OUT;n:type:ShaderForge.SFN_Color,id:4865,x:31836,y:33122,ptovrint:False,ptlb:Spec Color,ptin:_SpecColor,varname:node_4865,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_AmbientLight,id:7528,x:32597,y:32682,varname:node_7528,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2460,x:32790,y:32634,cmnt:Ambient Light,varname:node_2460,prsc:2|A-2224-OUT,B-7528-RGB;n:type:ShaderForge.SFN_Multiply,id:544,x:31990,y:32132,cmnt:Diffuse Color,varname:node_544,prsc:2|A-851-RGB,B-5927-RGB;n:type:ShaderForge.SFN_Tex2d,id:6501,x:33772,y:33139,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_6501,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:64dce7fe45b60774584851b5c5575ed1,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:6769,x:32067,y:32421,varname:node_6769,prsc:2|A-851-A,B-5927-A;n:type:ShaderForge.SFN_DepthBlend,id:70,x:32507,y:33520,varname:node_70,prsc:2|DIST-7270-OUT;n:type:ShaderForge.SFN_Get,id:7815,x:32657,y:33429,varname:node_7815,prsc:2|IN-8287-OUT;n:type:ShaderForge.SFN_Set,id:8287,x:32245,y:32421,varname:BaseOpacity,prsc:2|IN-6769-OUT;n:type:ShaderForge.SFN_Multiply,id:5933,x:32870,y:33462,varname:node_5933,prsc:2|A-7815-OUT,B-70-OUT;n:type:ShaderForge.SFN_Slider,id:7270,x:32126,y:33545,ptovrint:False,ptlb:Fade Strength,ptin:_FadeStrength,varname:node_7270,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:0.3;n:type:ShaderForge.SFN_Set,id:985,x:32627,y:32050,varname:Diffuse,prsc:2|IN-3804-OUT;n:type:ShaderForge.SFN_Get,id:2224,x:32583,y:32611,varname:node_2224,prsc:2|IN-985-OUT;n:type:ShaderForge.SFN_Get,id:3969,x:31844,y:32709,varname:node_3969,prsc:2|IN-985-OUT;n:type:ShaderForge.SFN_Get,id:345,x:33164,y:32779,varname:node_345,prsc:2|IN-6275-OUT;n:type:ShaderForge.SFN_Set,id:6275,x:34193,y:33336,varname:Normal,prsc:2|IN-9876-OUT;n:type:ShaderForge.SFN_Set,id:8734,x:33236,y:33610,varname:Border,prsc:2|IN-9667-OUT;n:type:ShaderForge.SFN_DepthBlend,id:7417,x:32520,y:33672,varname:node_7417,prsc:2|DIST-3891-OUT;n:type:ShaderForge.SFN_Subtract,id:4247,x:32710,y:33622,varname:node_4247,prsc:2|A-70-OUT,B-7417-OUT;n:type:ShaderForge.SFN_Vector1,id:6927,x:32127,y:33732,varname:node_6927,prsc:2,v1:0.05;n:type:ShaderForge.SFN_Add,id:3891,x:32329,y:33666,varname:node_3891,prsc:2|A-7270-OUT,B-6927-OUT;n:type:ShaderForge.SFN_Get,id:7564,x:31278,y:31797,varname:node_7564,prsc:2|IN-8734-OUT;n:type:ShaderForge.SFN_Add,id:4993,x:32946,y:32656,varname:node_4993,prsc:2|A-2460-OUT,B-5489-RGB;n:type:ShaderForge.SFN_Color,id:5489,x:32738,y:32809,ptovrint:False,ptlb:Extra Emissive,ptin:_ExtraEmissive,varname:node_5489,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Clamp01,id:588,x:33148,y:32656,varname:node_588,prsc:2|IN-4993-OUT;n:type:ShaderForge.SFN_Clamp01,id:9667,x:32976,y:33618,varname:node_9667,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Slider,id:1737,x:33621,y:33448,ptovrint:False,ptlb:Normal Intensity,ptin:_NormalIntensity,varname:node_1737,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:9876,x:34015,y:33295,varname:node_9876,prsc:2|A-4476-OUT,B-6501-RGB,T-1737-OUT;n:type:ShaderForge.SFN_Vector3,id:4476,x:33757,y:33324,varname:node_4476,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_Clamp01,id:3804,x:32446,y:32009,varname:node_3804,prsc:2|IN-4169-OUT;n:type:ShaderForge.SFN_Tex2d,id:2609,x:31801,y:31728,ptovrint:False,ptlb:Border,ptin:_Border,varname:node_2609,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:340320a6644c6d4419790c2c3c83d8a1,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Lerp,id:4169,x:32281,y:32002,varname:node_4169,prsc:2|A-544-OUT,B-3823-OUT,T-9615-OUT;n:type:ShaderForge.SFN_Multiply,id:3823,x:31998,y:31793,cmnt:Diffuse Color,varname:node_3823,prsc:2|A-2609-RGB,B-5927-RGB;n:type:ShaderForge.SFN_Add,id:3466,x:31485,y:31797,varname:node_3466,prsc:2|A-7564-OUT,B-7564-OUT;n:type:ShaderForge.SFN_Clamp01,id:1656,x:31641,y:31852,varname:node_1656,prsc:2|IN-3466-OUT;n:type:ShaderForge.SFN_Multiply,id:9615,x:31813,y:31885,varname:node_9615,prsc:2|A-1656-OUT,B-8005-OUT;n:type:ShaderForge.SFN_Slider,id:8005,x:31373,y:31984,ptovrint:False,ptlb:Border Intensity,ptin:_BorderIntensity,varname:node_8005,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Cubemap,id:144,x:32527,y:33150,ptovrint:False,ptlb:Reflection,ptin:_Reflection,varname:node_144,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,pvfc:0;n:type:ShaderForge.SFN_Multiply,id:5839,x:32744,y:33135,varname:node_5839,prsc:2|A-144-RGB,B-4013-OUT;n:type:ShaderForge.SFN_Slider,id:4013,x:32383,y:33315,ptovrint:False,ptlb:Reflection Intensity,ptin:_ReflectionIntensity,varname:node_4013,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Add,id:8554,x:32942,y:33056,varname:node_8554,prsc:2|A-5085-OUT,B-5839-OUT;proporder:5927-4865-5489-5328-7270-1737-8005-851-6501-2609-144-4013;pass:END;sub:END;*/

Shader "Shader Forge/Pond" {
    Properties {
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _SpecColor ("Spec Color", Color) = (1,1,1,1)
        _ExtraEmissive ("Extra Emissive", Color) = (0,0,0,1)
        _Gloss ("Gloss", Range(0, 1)) = 0.5
        _FadeStrength ("Fade Strength", Range(0, 0.3)) = 0
        _NormalIntensity ("Normal Intensity", Range(0, 1)) = 1
        _BorderIntensity ("Border Intensity", Range(0, 1)) = 1
        _Diffuse ("Diffuse", 2D) = "black" {}
        _Normal ("Normal", 2D) = "bump" {}
        _Border ("Border", 2D) = "gray" {}
        _Reflection ("Reflection", Cube) = "_Skybox" {}
        _ReflectionIntensity ("Reflection Intensity", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            uniform float _Gloss;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _FadeStrength;
            uniform float4 _ExtraEmissive;
            uniform float _NormalIntensity;
            uniform sampler2D _Border; uniform float4 _Border_ST;
            uniform float _BorderIntensity;
            uniform samplerCUBE _Reflection;
            uniform float _ReflectionIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 projPos : TEXCOORD5;
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 Normal = lerp(float3(0,0,1),_Normal_var.rgb,_NormalIntensity);
                float3 normalLocal = Normal;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
////// Emissive:
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float4 _Border_var = tex2D(_Border,TRANSFORM_TEX(i.uv0, _Border));
                float node_70 = saturate((sceneZ-partZ)/_FadeStrength);
                float Border = saturate((node_70-saturate((sceneZ-partZ)/(_FadeStrength+0.05))));
                float node_7564 = Border;
                float3 Diffuse = saturate(lerp((_Diffuse_var.rgb*_Color.rgb),(_Border_var.rgb*_Color.rgb),(saturate((node_7564+node_7564))*_BorderIntensity)));
                float3 emissive = saturate(((Diffuse*UNITY_LIGHTMODEL_AMBIENT.rgb)+_ExtraEmissive.rgb));
                float node_7782 = max(0,dot(lightDirection,normalDirection)); // Lambert
                float3 finalColor = emissive + ((((Diffuse*node_7782)+(node_7782*pow(max(0,dot(normalDirection,halfDirection)),exp2(lerp(1,11,_Gloss)))*_SpecColor.rgb))*_LightColor0.rgb*attenuation)+(texCUBE(_Reflection,viewReflectDirection).rgb*_ReflectionIntensity));
                float BaseOpacity = (_Diffuse_var.a*_Color.a);
                fixed4 finalRGBA = fixed4(finalColor,(BaseOpacity*node_70));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            uniform float _Gloss;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _FadeStrength;
            uniform float4 _ExtraEmissive;
            uniform float _NormalIntensity;
            uniform sampler2D _Border; uniform float4 _Border_ST;
            uniform float _BorderIntensity;
            uniform samplerCUBE _Reflection;
            uniform float _ReflectionIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 projPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 Normal = lerp(float3(0,0,1),_Normal_var.rgb,_NormalIntensity);
                float3 normalLocal = Normal;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float4 _Border_var = tex2D(_Border,TRANSFORM_TEX(i.uv0, _Border));
                float node_70 = saturate((sceneZ-partZ)/_FadeStrength);
                float Border = saturate((node_70-saturate((sceneZ-partZ)/(_FadeStrength+0.05))));
                float node_7564 = Border;
                float3 Diffuse = saturate(lerp((_Diffuse_var.rgb*_Color.rgb),(_Border_var.rgb*_Color.rgb),(saturate((node_7564+node_7564))*_BorderIntensity)));
                float node_7782 = max(0,dot(lightDirection,normalDirection)); // Lambert
                float3 finalColor = ((((Diffuse*node_7782)+(node_7782*pow(max(0,dot(normalDirection,halfDirection)),exp2(lerp(1,11,_Gloss)))*_SpecColor.rgb))*_LightColor0.rgb*attenuation)+(texCUBE(_Reflection,viewReflectDirection).rgb*_ReflectionIntensity));
                float BaseOpacity = (_Diffuse_var.a*_Color.a);
                fixed4 finalRGBA = fixed4(finalColor * (BaseOpacity*node_70),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
