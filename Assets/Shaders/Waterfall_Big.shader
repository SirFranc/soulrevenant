// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:1,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32716,y:32678,varname:node_4795,prsc:2|emission-2393-OUT,alpha-4398-OUT,refract-6294-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:31725,y:32439,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8cae10014bc934e4a8b38576f45ea463,ntxv:0,isnm:False|UVIN-5650-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32230,y:32609,varname:node_2393,prsc:2|A-6074-RGB,B-797-RGB,C-1595-RGB,D-3640-RGB;n:type:ShaderForge.SFN_Color,id:797,x:31454,y:32679,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Panner,id:5650,x:31535,y:32439,varname:node_5650,prsc:2,spu:0,spv:0.5|UVIN-7605-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3640,x:31633,y:32885,ptovrint:False,ptlb:Alpha01,ptin:_Alpha01,varname:node_3640,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:258ce449510dd0840ad08093800b6aaf,ntxv:0,isnm:False|UVIN-5650-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:1595,x:31400,y:32885,ptovrint:False,ptlb:Alpha02,ptin:_Alpha02,varname:_Alpha02,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e7ed57555fc1e3942a935531ce4cbf3d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:7605,x:31212,y:32468,varname:node_7605,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:9924,x:31674,y:33187,ptovrint:False,ptlb:Distortion,ptin:_Distortion,varname:node_9924,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True|UVIN-5650-UVOUT;n:type:ShaderForge.SFN_Multiply,id:8152,x:31927,y:33227,varname:node_8152,prsc:2|A-7605-UVOUT,B-9924-RGB,C-9926-OUT,D-3796-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3286,x:32247,y:32758,varname:node_3286,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5529-OUT;n:type:ShaderForge.SFN_Multiply,id:3796,x:32033,y:33029,varname:node_3796,prsc:2|A-1595-RGB,B-6074-RGB,C-797-RGB,D-3640-RGB;n:type:ShaderForge.SFN_ValueProperty,id:9926,x:31674,y:33369,ptovrint:False,ptlb:DistortionAmount,ptin:_DistortionAmount,varname:node_9926,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:5529,x:32059,y:32727,varname:node_5529,prsc:2|A-6074-RGB,B-3640-RGB,C-1595-RGB;n:type:ShaderForge.SFN_ComponentMask,id:6294,x:32119,y:33227,varname:node_6294,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-8152-OUT;n:type:ShaderForge.SFN_DepthBlend,id:9584,x:32247,y:32910,varname:node_9584,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4398,x:32448,y:32853,varname:node_4398,prsc:2|A-3286-OUT,B-9584-OUT;proporder:6074-797-3640-1595-9924-9926;pass:END;sub:END;*/

Shader "Shader Forge/Waterfall_Big" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _Alpha01 ("Alpha01", 2D) = "white" {}
        _Alpha02 ("Alpha02", 2D) = "white" {}
        _Distortion ("Distortion", 2D) = "bump" {}
        _DistortionAmount ("DistortionAmount", Float ) = 0.5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent+1"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform sampler2D _Alpha01; uniform float4 _Alpha01_ST;
            uniform sampler2D _Alpha02; uniform float4 _Alpha02_ST;
            uniform sampler2D _Distortion; uniform float4 _Distortion_ST;
            uniform float _DistortionAmount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                float4 projPos : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 node_7759 = _Time + _TimeEditor;
                float2 node_5650 = (i.uv0+node_7759.g*float2(0,0.5));
                float3 _Distortion_var = UnpackNormal(tex2D(_Distortion,TRANSFORM_TEX(node_5650, _Distortion)));
                float4 _Alpha02_var = tex2D(_Alpha02,TRANSFORM_TEX(i.uv0, _Alpha02));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_5650, _MainTex));
                float4 _Alpha01_var = tex2D(_Alpha01,TRANSFORM_TEX(node_5650, _Alpha01));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (float3(i.uv0,0.0)*_Distortion_var.rgb*_DistortionAmount*(_Alpha02_var.rgb*_MainTex_var.rgb*_TintColor.rgb*_Alpha01_var.rgb)).rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
////// Lighting:
////// Emissive:
                float3 emissive = (_MainTex_var.rgb*_TintColor.rgb*_Alpha02_var.rgb*_Alpha01_var.rgb);
                float3 finalColor = emissive;
                return fixed4(lerp(sceneColor.rgb, finalColor,((_MainTex_var.rgb*_Alpha01_var.rgb*_Alpha02_var.rgb).r*saturate((sceneZ-partZ)))),1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
