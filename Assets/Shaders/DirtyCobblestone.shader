// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:2865,x:32823,y:32662,varname:node_2865,prsc:2|diff-2664-OUT,spec-7205-OUT,gloss-1813-OUT,normal-9611-RGB,amdfl-668-OUT,amspl-668-OUT;n:type:ShaderForge.SFN_Multiply,id:6343,x:31917,y:32584,varname:node_6343,prsc:2|A-7736-RGB,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:31730,y:32442,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.875,c2:0.875,c3:0.875,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7736,x:31584,y:32442,ptovrint:True,ptlb:Base Color,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:19d8498e9930c2b40805606d3533346f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:1813,x:32111,y:32312,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Metallic_copy,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4454095,max:1;n:type:ShaderForge.SFN_AmbientLight,id:8734,x:32214,y:32404,varname:node_8734,prsc:2;n:type:ShaderForge.SFN_Multiply,id:668,x:32435,y:32394,varname:node_668,prsc:2|A-1813-OUT,B-8734-RGB;n:type:ShaderForge.SFN_Lerp,id:7981,x:32114,y:32662,varname:node_7981,prsc:2|A-6343-OUT,B-4278-RGB,T-1669-OUT;n:type:ShaderForge.SFN_Tex2d,id:4278,x:31437,y:32641,ptovrint:False,ptlb:Dirt_01,ptin:_Dirt_01,varname:node_4278,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:12412015666effd48a7d1bce7259312b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:7205,x:32260,y:32571,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:_Gloss_copy,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Tex2d,id:7409,x:31185,y:33217,ptovrint:False,ptlb:Cobble_Mask,ptin:_Cobble_Mask,varname:_Noise_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:770914d3db88f1c46aaa3f56ece69aa1,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5608,x:31437,y:32827,ptovrint:False,ptlb:Dirt_02,ptin:_Dirt_02,varname:_Dirt_02,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5d740f60cf83ddb40b41fdea5c98d7ec,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:850,x:31437,y:33010,ptovrint:False,ptlb:Dirt_03,ptin:_Dirt_03,varname:node_850,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:2a2c38022de47254d95b69af1ef9c6f2,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:1005,x:32287,y:32662,varname:node_1005,prsc:2|A-7981-OUT,B-5608-RGB,T-9911-OUT;n:type:ShaderForge.SFN_Lerp,id:2664,x:32458,y:32662,varname:node_2664,prsc:2|A-1005-OUT,B-850-RGB,T-9131-OUT;n:type:ShaderForge.SFN_Multiply,id:1669,x:31702,y:32891,varname:node_1669,prsc:2|A-3323-R,B-7409-RGB;n:type:ShaderForge.SFN_Multiply,id:9911,x:31702,y:33031,varname:node_9911,prsc:2|A-3323-G,B-7409-RGB;n:type:ShaderForge.SFN_Multiply,id:9131,x:31702,y:33165,varname:node_9131,prsc:2|A-3323-B,B-7409-RGB;n:type:ShaderForge.SFN_Tex2d,id:9611,x:31860,y:33337,ptovrint:False,ptlb:NormalMap,ptin:_NormalMap,varname:node_9611,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:2e33a3e7ef7160b448e0fd9f0b4cece4,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:3323,x:30986,y:32980,ptovrint:False,ptlb:node_3323,ptin:_node_3323,varname:node_3323,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:49a25b33c1f0b6f4598302f27b2e21ae,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1904,x:31873,y:32891,varname:node_1904,prsc:2|A-1669-OUT,B-1669-OUT;n:type:ShaderForge.SFN_Multiply,id:7888,x:31873,y:33031,varname:node_7888,prsc:2|A-9911-OUT,B-9911-OUT;n:type:ShaderForge.SFN_Multiply,id:3695,x:31873,y:33165,varname:node_3695,prsc:2|A-9131-OUT,B-9131-OUT;n:type:ShaderForge.SFN_Add,id:4757,x:32048,y:32891,varname:node_4757,prsc:2|A-1904-OUT,B-1904-OUT;n:type:ShaderForge.SFN_Add,id:1909,x:32048,y:33031,varname:node_1909,prsc:2|A-7888-OUT,B-7888-OUT;n:type:ShaderForge.SFN_Add,id:6116,x:32048,y:33165,varname:node_6116,prsc:2|A-3695-OUT,B-3695-OUT;proporder:6665-7736-9611-1813-4278-7205-7409-5608-850-3323;pass:END;sub:END;*/

Shader "Shader Forge/DirtyCobblestone" {
    Properties {
        _Color ("Color", Color) = (0.875,0.875,0.875,1)
        _MainTex ("Base Color", 2D) = "white" {}
        _NormalMap ("NormalMap", 2D) = "bump" {}
        [HideInInspector]_Gloss ("Gloss", Range(0, 1)) = 0.4454095
        _Dirt_01 ("Dirt_01", 2D) = "white" {}
        [HideInInspector]_Metallic ("Metallic", Range(0, 1)) = 0
        _Cobble_Mask ("Cobble_Mask", 2D) = "white" {}
        _Dirt_02 ("Dirt_02", 2D) = "white" {}
        _Dirt_03 ("Dirt_03", 2D) = "white" {}
        _node_3323 ("node_3323", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Gloss;
            uniform sampler2D _Dirt_01; uniform float4 _Dirt_01_ST;
            uniform float _Metallic;
            uniform sampler2D _Cobble_Mask; uniform float4 _Cobble_Mask_ST;
            uniform sampler2D _Dirt_02; uniform float4 _Dirt_02_ST;
            uniform sampler2D _Dirt_03; uniform float4 _Dirt_03_ST;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform sampler2D _node_3323; uniform float4 _node_3323_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
            #endif
            #ifdef DYNAMICLIGHTMAP_ON
                o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
            #endif
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
            o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            float3 lightColor = _LightColor0.rgb;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            UNITY_TRANSFER_FOG(o,o.pos);
            TRANSFER_VERTEX_TO_FRAGMENT(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
            i.normalDir = normalize(i.normalDir);
            float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
            float3 normalLocal = _NormalMap_var.rgb;
            float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
            float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
            float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
            float3 lightColor = _LightColor0.rgb;
            float3 halfDirection = normalize(viewDirection+lightDirection);
// Lighting:
            float attenuation = LIGHT_ATTENUATION(i);
            float3 attenColor = attenuation * _LightColor0.xyz;
            float Pi = 3.141592654;
            float InvPi = 0.31830988618;
///// Gloss:
            float gloss = _Gloss;
            float specPow = exp2( gloss * 10.0+1.0);
/// GI Data:
            UnityLight light;
            #ifdef LIGHTMAP_OFF
                light.color = lightColor;
                light.dir = lightDirection;
                light.ndotl = LambertTerm (normalDirection, light.dir);
            #else
                light.color = half3(0.f, 0.f, 0.f);
                light.ndotl = 0.0f;
                light.dir = half3(0.f, 0.f, 0.f);
            #endif
            UnityGIInput d;
            d.light = light;
            d.worldPos = i.posWorld.xyz;
            d.worldViewDir = viewDirection;
            d.atten = attenuation;
            #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                d.ambient = 0;
                d.lightmapUV = i.ambientOrLightmapUV;
            #else
                d.ambient = i.ambientOrLightmapUV;
            #endif
            d.boxMax[0] = unity_SpecCube0_BoxMax;
            d.boxMin[0] = unity_SpecCube0_BoxMin;
            d.probePosition[0] = unity_SpecCube0_ProbePosition;
            d.probeHDR[0] = unity_SpecCube0_HDR;
            d.boxMax[1] = unity_SpecCube1_BoxMax;
            d.boxMin[1] = unity_SpecCube1_BoxMin;
            d.probePosition[1] = unity_SpecCube1_ProbePosition;
            d.probeHDR[1] = unity_SpecCube1_HDR;
            UnityGI gi = UnityGlobalIllumination (d, 1, gloss, normalDirection);
            lightDirection = gi.light.dir;
            lightColor = gi.light.color;
// Specular:
            float NdotL = max(0, dot( normalDirection, lightDirection ));
            float3 node_668 = (_Gloss*UNITY_LIGHTMODEL_AMBIENT.rgb);
            float LdotH = max(0.0,dot(lightDirection, halfDirection));
            float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
            float4 _Dirt_01_var = tex2D(_Dirt_01,TRANSFORM_TEX(i.uv0, _Dirt_01));
            float4 _node_3323_var = tex2D(_node_3323,TRANSFORM_TEX(i.uv0, _node_3323));
            float4 _Cobble_Mask_var = tex2D(_Cobble_Mask,TRANSFORM_TEX(i.uv0, _Cobble_Mask));
            float3 node_1669 = (_node_3323_var.r*_Cobble_Mask_var.rgb);
            float4 _Dirt_02_var = tex2D(_Dirt_02,TRANSFORM_TEX(i.uv0, _Dirt_02));
            float3 node_9911 = (_node_3323_var.g*_Cobble_Mask_var.rgb);
            float4 _Dirt_03_var = tex2D(_Dirt_03,TRANSFORM_TEX(i.uv0, _Dirt_03));
            float3 node_9131 = (_node_3323_var.b*_Cobble_Mask_var.rgb);
            float3 diffuseColor = lerp(lerp(lerp((_MainTex_var.rgb*_Color.rgb),_Dirt_01_var.rgb,node_1669),_Dirt_02_var.rgb,node_9911),_Dirt_03_var.rgb,node_9131); // Need this for specular when using metallic
            float specularMonochrome;
            float3 specularColor;
            diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, _Metallic, specularColor, specularMonochrome );
            specularMonochrome = 1-specularMonochrome;
            float NdotV = max(0.0,dot( normalDirection, viewDirection ));
            float NdotH = max(0.0,dot( normalDirection, halfDirection ));
            float VdotH = max(0.0,dot( viewDirection, halfDirection ));
            float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
            float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
            float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
            float3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
            half grazingTerm = saturate( gloss + specularMonochrome );
            float3 indirectSpecular = (gi.indirect.specular + node_668);
            indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
            float3 specular = (directSpecular + indirectSpecular);
/// Diffuse:
            NdotL = max(0.0,dot( normalDirection, lightDirection ));
            half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
            float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
            float3 indirectDiffuse = float3(0,0,0);
            indirectDiffuse += node_668; // Diffuse Ambient Light
            indirectDiffuse += gi.indirect.diffuse;
            float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
// Final Color:
            float3 finalColor = diffuse + specular;
            fixed4 finalRGBA = fixed4(finalColor,1);
            UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
            return finalRGBA;
        }
        ENDCG
    }
    Pass {
        Name "FORWARD_DELTA"
        Tags {
            "LightMode"="ForwardAdd"
        }
        Blend One One
        
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_FORWARDADD
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #define _GLOSSYENV 1
        #include "UnityCG.cginc"
        #include "AutoLight.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #pragma multi_compile_fwdadd_fullshadows
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
        #pragma target 3.0
        uniform float4 _Color;
        uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
        uniform float _Gloss;
        uniform sampler2D _Dirt_01; uniform float4 _Dirt_01_ST;
        uniform float _Metallic;
        uniform sampler2D _Cobble_Mask; uniform float4 _Cobble_Mask_ST;
        uniform sampler2D _Dirt_02; uniform float4 _Dirt_02_ST;
        uniform sampler2D _Dirt_03; uniform float4 _Dirt_03_ST;
        uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
        uniform sampler2D _node_3323; uniform float4 _node_3323_ST;
        struct VertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 tangent : TANGENT;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float2 uv0 : TEXCOORD0;
            float2 uv1 : TEXCOORD1;
            float2 uv2 : TEXCOORD2;
            float4 posWorld : TEXCOORD3;
            float3 normalDir : TEXCOORD4;
            float3 tangentDir : TEXCOORD5;
            float3 bitangentDir : TEXCOORD6;
            LIGHTING_COORDS(7,8)
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            o.normalDir = UnityObjectToWorldNormal(v.normal);
            o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
            o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            float3 lightColor = _LightColor0.rgb;
            o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
            TRANSFER_VERTEX_TO_FRAGMENT(o)
            return o;
        }
        float4 frag(VertexOutput i) : COLOR {
            i.normalDir = normalize(i.normalDir);
            float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
            float3 normalLocal = _NormalMap_var.rgb;
            float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
            float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
            float3 lightColor = _LightColor0.rgb;
            float3 halfDirection = normalize(viewDirection+lightDirection);
// Lighting:
            float attenuation = LIGHT_ATTENUATION(i);
            float3 attenColor = attenuation * _LightColor0.xyz;
            float Pi = 3.141592654;
            float InvPi = 0.31830988618;
///// Gloss:
            float gloss = _Gloss;
            float specPow = exp2( gloss * 10.0+1.0);
// Specular:
            float NdotL = max(0, dot( normalDirection, lightDirection ));
            float LdotH = max(0.0,dot(lightDirection, halfDirection));
            float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
            float4 _Dirt_01_var = tex2D(_Dirt_01,TRANSFORM_TEX(i.uv0, _Dirt_01));
            float4 _node_3323_var = tex2D(_node_3323,TRANSFORM_TEX(i.uv0, _node_3323));
            float4 _Cobble_Mask_var = tex2D(_Cobble_Mask,TRANSFORM_TEX(i.uv0, _Cobble_Mask));
            float3 node_1669 = (_node_3323_var.r*_Cobble_Mask_var.rgb);
            float4 _Dirt_02_var = tex2D(_Dirt_02,TRANSFORM_TEX(i.uv0, _Dirt_02));
            float3 node_9911 = (_node_3323_var.g*_Cobble_Mask_var.rgb);
            float4 _Dirt_03_var = tex2D(_Dirt_03,TRANSFORM_TEX(i.uv0, _Dirt_03));
            float3 node_9131 = (_node_3323_var.b*_Cobble_Mask_var.rgb);
            float3 diffuseColor = lerp(lerp(lerp((_MainTex_var.rgb*_Color.rgb),_Dirt_01_var.rgb,node_1669),_Dirt_02_var.rgb,node_9911),_Dirt_03_var.rgb,node_9131); // Need this for specular when using metallic
            float specularMonochrome;
            float3 specularColor;
            diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, _Metallic, specularColor, specularMonochrome );
            specularMonochrome = 1-specularMonochrome;
            float NdotV = max(0.0,dot( normalDirection, viewDirection ));
            float NdotH = max(0.0,dot( normalDirection, halfDirection ));
            float VdotH = max(0.0,dot( viewDirection, halfDirection ));
            float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
            float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
            float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
            float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
            float3 specular = directSpecular;
/// Diffuse:
            NdotL = max(0.0,dot( normalDirection, lightDirection ));
            half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
            float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
            float3 diffuse = directDiffuse * diffuseColor;
// Final Color:
            float3 finalColor = diffuse + specular;
            return fixed4(finalColor * 1,0);
        }
        ENDCG
    }
    Pass {
        Name "Meta"
        Tags {
            "LightMode"="Meta"
        }
        Cull Off
        
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #define UNITY_PASS_META 1
        #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
        #define _GLOSSYENV 1
        #include "UnityCG.cginc"
        #include "Lighting.cginc"
        #include "UnityPBSLighting.cginc"
        #include "UnityStandardBRDF.cginc"
        #include "UnityMetaPass.cginc"
        #pragma fragmentoption ARB_precision_hint_fastest
        #pragma multi_compile_shadowcaster
        #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
        #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
        #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
        #pragma multi_compile_fog
        #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
        #pragma target 3.0
        uniform float4 _Color;
        uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
        uniform float _Gloss;
        uniform sampler2D _Dirt_01; uniform float4 _Dirt_01_ST;
        uniform float _Metallic;
        uniform sampler2D _Cobble_Mask; uniform float4 _Cobble_Mask_ST;
        uniform sampler2D _Dirt_02; uniform float4 _Dirt_02_ST;
        uniform sampler2D _Dirt_03; uniform float4 _Dirt_03_ST;
        uniform sampler2D _node_3323; uniform float4 _node_3323_ST;
        struct VertexInput {
            float4 vertex : POSITION;
            float2 texcoord0 : TEXCOORD0;
            float2 texcoord1 : TEXCOORD1;
            float2 texcoord2 : TEXCOORD2;
        };
        struct VertexOutput {
            float4 pos : SV_POSITION;
            float2 uv0 : TEXCOORD0;
            float2 uv1 : TEXCOORD1;
            float2 uv2 : TEXCOORD2;
            float4 posWorld : TEXCOORD3;
        };
        VertexOutput vert (VertexInput v) {
            VertexOutput o = (VertexOutput)0;
            o.uv0 = v.texcoord0;
            o.uv1 = v.texcoord1;
            o.uv2 = v.texcoord2;
            o.posWorld = mul(unity_ObjectToWorld, v.vertex);
            o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
            return o;
        }
        float4 frag(VertexOutput i) : SV_Target {
/// Vectors:
            float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
            UnityMetaInput o;
            UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
            
            o.Emission = 0;
            
            float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
            float4 _Dirt_01_var = tex2D(_Dirt_01,TRANSFORM_TEX(i.uv0, _Dirt_01));
            float4 _node_3323_var = tex2D(_node_3323,TRANSFORM_TEX(i.uv0, _node_3323));
            float4 _Cobble_Mask_var = tex2D(_Cobble_Mask,TRANSFORM_TEX(i.uv0, _Cobble_Mask));
            float3 node_1669 = (_node_3323_var.r*_Cobble_Mask_var.rgb);
            float4 _Dirt_02_var = tex2D(_Dirt_02,TRANSFORM_TEX(i.uv0, _Dirt_02));
            float3 node_9911 = (_node_3323_var.g*_Cobble_Mask_var.rgb);
            float4 _Dirt_03_var = tex2D(_Dirt_03,TRANSFORM_TEX(i.uv0, _Dirt_03));
            float3 node_9131 = (_node_3323_var.b*_Cobble_Mask_var.rgb);
            float3 diffColor = lerp(lerp(lerp((_MainTex_var.rgb*_Color.rgb),_Dirt_01_var.rgb,node_1669),_Dirt_02_var.rgb,node_9911),_Dirt_03_var.rgb,node_9131);
            float specularMonochrome;
            float3 specColor;
            diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metallic, specColor, specularMonochrome );
            float roughness = 1.0 - _Gloss;
            o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
            
            return UnityMetaFragment( o );
        }
        ENDCG
    }
}
FallBack "Diffuse"
CustomEditor "ShaderForgeMaterialInspector"
}
