﻿Shader "Dixidiasoft/WaterfallShader" {
	Properties {
		_MainTex("Diffuse (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (0.5,0.5,0.5)
		_Alpha1 ("Alpha1 (RGB)", 2D) = "white" {}
		_Alpha1CTRL("Alpha1CTRL",Range(0,1))=0.5
		_Alpha2 ("Alpha2 (RGB)", 2D) = "white" {}
		_Alpha2CTRL("Alpha2CTRL",Range(0,1))=0.5
		_Opacity ("Opacità",Range(0,1))=0.5
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent"}
		
		cull off
		
		CGPROGRAM
		#pragma surface surf BlinnPhong alpha:blend
		#pragma target 3.0

		sampler2D _Alpha1;
		sampler2D _Alpha2;
		sampler2D _MainTex;
		float _Opacity;
		float _Alpha1CTRL;
		float _Alpha2CTRL;
		float3 _Color;

		struct Input {
			float2 uv_Alpha1;
			float2 uv_Alpha2;
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 colore= tex2D (_MainTex,IN.uv_MainTex);
			o.Albedo= colore.rgb*_Color;
			
			fixed3 first = tex2D (_Alpha1, IN.uv_Alpha1).rgb;
			fixed3 second= tex2D (_Alpha2, IN.uv_Alpha2).rgb;
			fixed3 firstLerp=lerp (first,1,_Alpha1CTRL);
			fixed3 secondLerp=lerp (second,1,_Alpha2CTRL);
			fixed3 product = dot(firstLerp,secondLerp);
			o.Alpha = product.rgb*_Opacity;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
