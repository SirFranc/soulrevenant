﻿Shader "Dixidiasoft/GrayscaleTexture" {
	Properties 
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Blend 	("BlendValue", Range(0,1)) = 0.5
	}
	SubShader {
	   	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	     LOD 200
	 
	     CGPROGRAM
	     #pragma surface surf Lambert alpha
	 
	         sampler2D _MainTex;
	 		 float _Blend;
	 		
	         struct Input {
	             float2 uv_MainTex;
	         };
	 
	         void surf (Input IN, inout SurfaceOutput o) {
	             half4 c = tex2D(_MainTex, IN.uv_MainTex);
	             fixed3 grayscale=(c.r + c.g + c.b)/3;
	             fixed3 truecolor=c.rgb;
	             o.Albedo = lerp(grayscale,truecolor,_Blend);
	             o.Alpha = c.a;
	         }
	 
	     ENDCG
	 }
 
 	Fallback "Transparent/VertexLit"
}
