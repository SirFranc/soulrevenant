Shader "Dixidiasoft/Cliff Shader" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SaturationAmount ("Saturation Amount", Range(0.0, 1.0)) = 1.0
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Detail ("Detail (normal) (RGB)", 2D) = "gray" {}
	_Detail2 ("Detail (grain)(RGB)", 2D) = "gray" {}
	_Detail3 ("Detail (grass) (RGB)", 2D) = "gray" {}
	_DetailMask ("Detail (grass mask) (RGB)", 2D) = "gray" {}
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 500
	
CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
sampler2D _Detail;
sampler2D _Detail2;
sampler2D _Detail3;
sampler2D _DetailMask;
fixed4 _Color;
uniform float _SaturationAmount;

struct Input {
	float2 uv_MainTex;
	float2 uv_Detail;
	float2 uv_Detail2;
	float2 uv_Detail3;
	float2 uv_DetailMask;
};
void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	c*= tex2D(_Detail, IN.uv_MainTex) * 2;	
	c *= tex2D(_Detail2, IN.uv_Detail2) * 2;	
	
	fixed4 g = tex2D(_Detail3,IN.uv_Detail3) * _Color;
	fixed4 f = tex2D(_DetailMask,IN.uv_DetailMask) * _Color;

	o.Albedo = lerp(c,g,f);

	o.Alpha = c.a;
}
ENDCG
}



Fallback "Diffuse"
}