// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:0,trmd:1,grmd:1,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:False,qofs:1,qpre:2,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:32574,y:32920,varname:node_2865,prsc:2|diff-4151-OUT,spec-7132-OUT,gloss-1813-OUT,normal-6496-OUT,amdfl-5490-RGB,amspl-5490-RGB,alpha-9508-OUT,refract-601-OUT;n:type:ShaderForge.SFN_Multiply,id:6343,x:31911,y:32222,varname:node_6343,prsc:2|A-7788-OUT,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:31544,y:32236,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Slider,id:358,x:31327,y:32494,ptovrint:False,ptlb:Shiny,ptin:_Shiny,varname:node_358,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.7112041,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:30851,y:32675,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Metallic_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6674564,max:1;n:type:ShaderForge.SFN_Vector1,id:2232,x:31265,y:32605,varname:node_2232,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Slider,id:1746,x:31383,y:33467,ptovrint:False,ptlb:Distortion,ptin:_Distortion,varname:node_1746,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.1,cur:5,max:5;n:type:ShaderForge.SFN_Slider,id:3260,x:31247,y:33004,ptovrint:False,ptlb:Normals,ptin:_Normals,varname:_Softness_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:4,max:4;n:type:ShaderForge.SFN_Panner,id:6690,x:30660,y:33198,varname:node_6690,prsc:2,spu:0.01,spv:-0.02|UVIN-6546-UVOUT;n:type:ShaderForge.SFN_Panner,id:1334,x:30660,y:33448,varname:node_1334,prsc:2,spu:-0.015,spv:0.01|UVIN-6546-UVOUT;n:type:ShaderForge.SFN_Multiply,id:819,x:31975,y:33400,varname:node_819,prsc:2|A-8927-UVOUT,B-1327-OUT,C-608-OUT;n:type:ShaderForge.SFN_Multiply,id:6496,x:31617,y:33059,varname:node_6496,prsc:2|A-9542-OUT,B-3260-OUT;n:type:ShaderForge.SFN_Distance,id:3135,x:31778,y:33632,varname:node_3135,prsc:2|A-1812-XYZ,B-1859-XYZ;n:type:ShaderForge.SFN_ViewPosition,id:1812,x:31602,y:33557,varname:node_1812,prsc:2;n:type:ShaderForge.SFN_FragmentPosition,id:1859,x:31602,y:33701,varname:node_1859,prsc:2;n:type:ShaderForge.SFN_Divide,id:601,x:32166,y:33509,varname:node_601,prsc:2|A-819-OUT,B-424-OUT;n:type:ShaderForge.SFN_Multiply,id:424,x:31948,y:33669,varname:node_424,prsc:2|A-3135-OUT,B-5445-OUT;n:type:ShaderForge.SFN_Vector1,id:5445,x:31778,y:33767,varname:node_5445,prsc:2,v1:0.7;n:type:ShaderForge.SFN_Tex2dAsset,id:1388,x:30471,y:33337,ptovrint:False,ptlb:NormalBump,ptin:_NormalBump,varname:node_1388,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Add,id:8875,x:31233,y:33248,varname:node_8875,prsc:2|A-5699-RGB,B-9099-RGB;n:type:ShaderForge.SFN_Multiply,id:1327,x:31801,y:33426,varname:node_1327,prsc:2|A-9542-OUT,B-1746-OUT;n:type:ShaderForge.SFN_AmbientLight,id:5490,x:31456,y:32795,varname:node_5490,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:8927,x:31781,y:33275,varname:node_8927,prsc:2,uv:0;n:type:ShaderForge.SFN_DepthBlend,id:7802,x:31247,y:32734,varname:node_7802,prsc:2|DIST-7195-OUT;n:type:ShaderForge.SFN_Multiply,id:9616,x:31484,y:32648,varname:node_9616,prsc:2|A-2232-OUT,B-7802-OUT;n:type:ShaderForge.SFN_Slider,id:7195,x:30812,y:32798,ptovrint:False,ptlb:Fade,ptin:_Fade,varname:node_7195,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:20;n:type:ShaderForge.SFN_Multiply,id:7132,x:31978,y:32630,varname:node_7132,prsc:2|A-358-OUT,B-5490-RGB,C-3785-OUT;n:type:ShaderForge.SFN_Vector1,id:3785,x:31758,y:32689,varname:node_3785,prsc:2,v1:5;n:type:ShaderForge.SFN_NormalVector,id:4953,x:30763,y:32087,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:3980,x:30953,y:32087,varname:node_3980,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4953-OUT;n:type:ShaderForge.SFN_Multiply,id:8805,x:31136,y:32087,varname:node_8805,prsc:2|A-3980-OUT,B-3980-OUT;n:type:ShaderForge.SFN_Panner,id:3228,x:30583,y:32908,varname:node_3228,prsc:2,spu:0,spv:0.5|UVIN-6546-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:9099,x:30929,y:33395,varname:node_9099,prsc:2,ntxv:0,isnm:False|UVIN-1334-UVOUT,TEX-1388-TEX;n:type:ShaderForge.SFN_Tex2d,id:5699,x:30933,y:33207,varname:node_5699,prsc:2,ntxv:0,isnm:False|UVIN-6690-UVOUT,TEX-1388-TEX;n:type:ShaderForge.SFN_Tex2d,id:3575,x:30945,y:33012,varname:node_3575,prsc:2,ntxv:0,isnm:False|UVIN-3228-UVOUT,TEX-6839-TEX;n:type:ShaderForge.SFN_Lerp,id:9542,x:31448,y:33289,varname:node_9542,prsc:2|A-3575-RGB,B-8875-OUT,T-8805-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:6839,x:30342,y:32937,ptovrint:False,ptlb:node_6839,ptin:_node_6839,varname:node_6839,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:5110,x:31566,y:31803,ptovrint:False,ptlb:Waterfall,ptin:_Waterfall,varname:node_5110,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-3228-UVOUT;n:type:ShaderForge.SFN_Color,id:6075,x:31566,y:31619,ptovrint:False,ptlb:Waterfall Color,ptin:_WaterfallColor,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Multiply,id:8480,x:31804,y:31820,varname:node_8480,prsc:2|A-6075-RGB,B-5110-RGB;n:type:ShaderForge.SFN_Lerp,id:4151,x:32238,y:32589,varname:node_4151,prsc:2|A-8480-OUT,B-6343-OUT,T-8805-OUT;n:type:ShaderForge.SFN_Lerp,id:9508,x:31911,y:32830,varname:node_9508,prsc:2|A-2506-OUT,B-7802-OUT,T-8805-OUT;n:type:ShaderForge.SFN_Vector1,id:2506,x:31706,y:32830,varname:node_2506,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Tex2dAsset,id:4030,x:30924,y:31882,ptovrint:False,ptlb:Base Color,ptin:_BaseColor,varname:node_4030,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:56b9db537814c554c9aa6d8c08457720,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:8036,x:31417,y:32082,varname:_MainTex,prsc:2,tex:56b9db537814c554c9aa6d8c08457720,ntxv:2,isnm:False|UVIN-1334-UVOUT,TEX-4030-TEX;n:type:ShaderForge.SFN_Add,id:5227,x:31587,y:32039,varname:node_5227,prsc:2|A-2260-RGB,B-8036-RGB;n:type:ShaderForge.SFN_Clamp01,id:7788,x:31739,y:32074,varname:node_7788,prsc:2|IN-5227-OUT;n:type:ShaderForge.SFN_Tex2d,id:2260,x:31412,y:31930,varname:node_2260,prsc:2,tex:56b9db537814c554c9aa6d8c08457720,ntxv:0,isnm:False|UVIN-6690-UVOUT,TEX-4030-TEX;n:type:ShaderForge.SFN_Depth,id:608,x:31328,y:33606,varname:node_608,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:6546,x:30337,y:33140,varname:node_6546,prsc:2,uv:0;proporder:6665-1388-358-1813-1746-3260-7195-6839-5110-6075-4030;pass:END;sub:END;*/

Shader "Shader Forge/FountainWater" {
    Properties {
        _Color ("Color", Color) = (0.5019608,0.5019608,0.5019608,1)
        _NormalBump ("NormalBump", 2D) = "bump" {}
        _Shiny ("Shiny", Range(0, 1)) = 0.7112041
        _Gloss ("Gloss", Range(0, 1)) = 0.6674564
        _Distortion ("Distortion", Range(0.1, 5)) = 5
        _Normals ("Normals", Range(0, 4)) = 4
        _Fade ("Fade", Range(0, 20)) = 0
        _node_6839 ("node_6839", 2D) = "bump" {}
        _Waterfall ("Waterfall", 2D) = "white" {}
        _WaterfallColor ("Waterfall Color", Color) = (0.5019608,0.5019608,0.5019608,1)
        _BaseColor ("Base Color", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest+1"
            "RenderType"="Opaque"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float _Shiny;
            uniform float _Gloss;
            uniform float _Distortion;
            uniform float _Normals;
            uniform sampler2D _NormalBump; uniform float4 _NormalBump_ST;
            uniform float _Fade;
            uniform sampler2D _node_6839; uniform float4 _node_6839_ST;
            uniform sampler2D _Waterfall; uniform float4 _Waterfall_ST;
            uniform float4 _WaterfallColor;
            uniform sampler2D _BaseColor; uniform float4 _BaseColor_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                LIGHTING_COORDS(9,10)
                UNITY_FOG_COORDS(11)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD12;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_3018 = _Time + _TimeEditor;
                float2 node_3228 = (i.uv0+node_3018.g*float2(0,0.5));
                float3 node_3575 = UnpackNormal(tex2D(_node_6839,TRANSFORM_TEX(node_3228, _node_6839)));
                float2 node_6690 = (i.uv0+node_3018.g*float2(0.01,-0.02));
                float3 node_5699 = UnpackNormal(tex2D(_NormalBump,TRANSFORM_TEX(node_6690, _NormalBump)));
                float2 node_1334 = (i.uv0+node_3018.g*float2(-0.015,0.01));
                float3 node_9099 = UnpackNormal(tex2D(_NormalBump,TRANSFORM_TEX(node_1334, _NormalBump)));
                float node_3980 = i.normalDir.g;
                float node_8805 = (node_3980*node_3980);
                float3 node_9542 = lerp(node_3575.rgb,(node_5699.rgb+node_9099.rgb),node_8805);
                float3 normalLocal = (node_9542*_Normals);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((float3(i.uv0,0.0)*(node_9542*_Distortion)*partZ)/(distance(_WorldSpaceCameraPos,i.posWorld.rgb)*0.7)).rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 1.0 - _Gloss; // Convert roughness to gloss
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                d.boxMax[0] = unity_SpecCube0_BoxMax;
                d.boxMin[0] = unity_SpecCube0_BoxMin;
                d.probePosition[0] = unity_SpecCube0_ProbePosition;
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.boxMax[1] = unity_SpecCube1_BoxMax;
                d.boxMin[1] = unity_SpecCube1_BoxMin;
                d.probePosition[1] = unity_SpecCube1_ProbePosition;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float3 specularColor = (_Shiny*UNITY_LIGHTMODEL_AMBIENT.rgb*5.0);
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular + UNITY_LIGHTMODEL_AMBIENT.rgb);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Diffuse Ambient Light
                indirectDiffuse += gi.indirect.diffuse;
                float4 _Waterfall_var = tex2D(_Waterfall,TRANSFORM_TEX(node_3228, _Waterfall));
                float4 node_2260 = tex2D(_BaseColor,TRANSFORM_TEX(node_6690, _BaseColor));
                float4 _MainTex = tex2D(_BaseColor,TRANSFORM_TEX(node_1334, _BaseColor));
                float3 diffuseColor = lerp((_WaterfallColor.rgb*_Waterfall_var.rgb),(saturate((node_2260.rgb+_MainTex.rgb))*_Color.rgb),node_8805);
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float node_7802 = saturate((sceneZ-partZ)/_Fade);
                float3 finalColor = diffuse * lerp(0.3,node_7802,node_8805) + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,lerp(0.3,node_7802,node_8805)),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float _Shiny;
            uniform float _Gloss;
            uniform float _Distortion;
            uniform float _Normals;
            uniform sampler2D _NormalBump; uniform float4 _NormalBump_ST;
            uniform float _Fade;
            uniform sampler2D _node_6839; uniform float4 _node_6839_ST;
            uniform sampler2D _Waterfall; uniform float4 _Waterfall_ST;
            uniform float4 _WaterfallColor;
            uniform sampler2D _BaseColor; uniform float4 _BaseColor_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 screenPos : TEXCOORD7;
                float4 projPos : TEXCOORD8;
                LIGHTING_COORDS(9,10)
                UNITY_FOG_COORDS(11)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_8633 = _Time + _TimeEditor;
                float2 node_3228 = (i.uv0+node_8633.g*float2(0,0.5));
                float3 node_3575 = UnpackNormal(tex2D(_node_6839,TRANSFORM_TEX(node_3228, _node_6839)));
                float2 node_6690 = (i.uv0+node_8633.g*float2(0.01,-0.02));
                float3 node_5699 = UnpackNormal(tex2D(_NormalBump,TRANSFORM_TEX(node_6690, _NormalBump)));
                float2 node_1334 = (i.uv0+node_8633.g*float2(-0.015,0.01));
                float3 node_9099 = UnpackNormal(tex2D(_NormalBump,TRANSFORM_TEX(node_1334, _NormalBump)));
                float node_3980 = i.normalDir.g;
                float node_8805 = (node_3980*node_3980);
                float3 node_9542 = lerp(node_3575.rgb,(node_5699.rgb+node_9099.rgb),node_8805);
                float3 normalLocal = (node_9542*_Normals);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + ((float3(i.uv0,0.0)*(node_9542*_Distortion)*partZ)/(distance(_WorldSpaceCameraPos,i.posWorld.rgb)*0.7)).rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = 1.0 - _Gloss; // Convert roughness to gloss
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float3 specularColor = (_Shiny*UNITY_LIGHTMODEL_AMBIENT.rgb*5.0);
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float4 _Waterfall_var = tex2D(_Waterfall,TRANSFORM_TEX(node_3228, _Waterfall));
                float4 node_2260 = tex2D(_BaseColor,TRANSFORM_TEX(node_6690, _BaseColor));
                float4 _MainTex = tex2D(_BaseColor,TRANSFORM_TEX(node_1334, _BaseColor));
                float3 diffuseColor = lerp((_WaterfallColor.rgb*_Waterfall_var.rgb),(saturate((node_2260.rgb+_MainTex.rgb))*_Color.rgb),node_8805);
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float node_7802 = saturate((sceneZ-partZ)/_Fade);
                float3 finalColor = diffuse * lerp(0.3,node_7802,node_8805) + specular;
                fixed4 finalRGBA = fixed4(finalColor,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform float _Shiny;
            uniform float _Gloss;
            uniform sampler2D _Waterfall; uniform float4 _Waterfall_ST;
            uniform float4 _WaterfallColor;
            uniform sampler2D _BaseColor; uniform float4 _BaseColor_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 node_2257 = _Time + _TimeEditor;
                float2 node_3228 = (i.uv0+node_2257.g*float2(0,0.5));
                float4 _Waterfall_var = tex2D(_Waterfall,TRANSFORM_TEX(node_3228, _Waterfall));
                float2 node_6690 = (i.uv0+node_2257.g*float2(0.01,-0.02));
                float4 node_2260 = tex2D(_BaseColor,TRANSFORM_TEX(node_6690, _BaseColor));
                float2 node_1334 = (i.uv0+node_2257.g*float2(-0.015,0.01));
                float4 _MainTex = tex2D(_BaseColor,TRANSFORM_TEX(node_1334, _BaseColor));
                float node_3980 = i.normalDir.g;
                float node_8805 = (node_3980*node_3980);
                float3 diffColor = lerp((_WaterfallColor.rgb*_Waterfall_var.rgb),(saturate((node_2260.rgb+_MainTex.rgb))*_Color.rgb),node_8805);
                float3 specColor = (_Shiny*UNITY_LIGHTMODEL_AMBIENT.rgb*5.0);
                float specularMonochrome = max(max(specColor.r, specColor.g),specColor.b);
                diffColor *= (1.0-specularMonochrome);
                float roughness = _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
