Shader "Dixidiasoft/Detailed Reflective" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
	_SaturationAmount ("Saturation Amount", Range(0.0, 1.0)) = 1.0
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Detail ("Detail (normal) (RGB)", 2D) = "gray" {}
	_Detail3 ("Detail (decal) (RGB)", 2D) = "gray" {}
	_ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
	_DetailMask ("Detail (decal mask) (RGB)", 2D) = "gray" {}
	_Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 250
	
CGPROGRAM
#pragma surface surf Lambert
#pragma target 3.0

sampler2D _MainTex;
sampler2D _Detail;
sampler2D _Detail3;
sampler2D _DetailMask;
samplerCUBE _Cube;
fixed4 _Color;
fixed4 _ReflectColor;
half _Shininess;
uniform float _SaturationAmount;

struct Input {
	float2 uv_MainTex;
	float2 uv_Detail;
	float2 uv_Detail3;
	float3 worldRefl;
	float2 uv_DetailMask;
};
void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
											
	c.rgb *= tex2D(_Detail,IN.uv_DetailMask).rgb*2;
	
	fixed4 g = tex2D(_Detail3,IN.uv_MainTex) * _Color;
	fixed4 f = tex2D(_DetailMask,IN.uv_DetailMask) * _Color;
	
	fixed4 reflcol = texCUBE (_Cube, IN.worldRefl);
	reflcol *= f;

	o.Albedo = lerp(c,g,f);
	o.Emission = reflcol.rgb * _ReflectColor.rgb;
	o.Alpha = reflcol.a * _ReflectColor.a;
	o.Specular = _Shininess;
}
ENDCG
}

Fallback "Diffuse"
}
