// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,culm:2,bsrc:3,bdst:7,dpts:6,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:4,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1873,x:33566,y:32874,varname:node_1873,prsc:2|emission-9628-OUT;n:type:ShaderForge.SFN_SceneDepth,id:8825,x:31600,y:33285,varname:node_8825,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1403,x:31818,y:33406,varname:node_1403,prsc:2|A-8825-OUT,B-5563-OUT;n:type:ShaderForge.SFN_Slider,id:7319,x:31175,y:33476,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_7319,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:9129,x:32784,y:33137,varname:node_9129,prsc:2|A-2029-RGB,B-1578-RGB,T-5020-OUT;n:type:ShaderForge.SFN_Tex2d,id:2029,x:32264,y:33014,ptovrint:False,ptlb:BackText,ptin:_BackText,varname:node_2029,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1578,x:32264,y:32808,ptovrint:False,ptlb:FrontText,ptin:_FrontText,varname:node_1578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Divide,id:5563,x:31606,y:33474,varname:node_5563,prsc:2|A-7319-OUT,B-1104-OUT;n:type:ShaderForge.SFN_Vector1,id:1104,x:31375,y:33654,varname:node_1104,prsc:2,v1:2.5;n:type:ShaderForge.SFN_Tex2d,id:1220,x:31706,y:33662,ptovrint:False,ptlb:Debris,ptin:_Debris,varname:node_1220,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:f2e02d8286ac04497961e964165d4ed8,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Multiply,id:4813,x:31895,y:33636,varname:node_4813,prsc:2|A-7319-OUT,B-1220-RGB;n:type:ShaderForge.SFN_Clamp01,id:5020,x:32597,y:33293,varname:node_5020,prsc:2|IN-1279-OUT;n:type:ShaderForge.SFN_Blend,id:7273,x:32056,y:33497,varname:node_7273,prsc:2,blmd:10,clmp:True|SRC-1403-OUT,DST-4813-OUT;n:type:ShaderForge.SFN_Power,id:1279,x:32245,y:33455,varname:node_1279,prsc:2|VAL-7273-OUT,EXP-167-OUT;n:type:ShaderForge.SFN_Vector1,id:167,x:31895,y:33809,varname:node_167,prsc:2,v1:12;n:type:ShaderForge.SFN_OneMinus,id:6805,x:32256,y:33620,varname:node_6805,prsc:2|IN-1279-OUT;n:type:ShaderForge.SFN_Multiply,id:3561,x:32421,y:33620,varname:node_3561,prsc:2|A-1279-OUT,B-6805-OUT;n:type:ShaderForge.SFN_Add,id:9628,x:33347,y:33208,varname:node_9628,prsc:2|A-9129-OUT,B-3337-OUT;n:type:ShaderForge.SFN_Add,id:3776,x:32624,y:33620,varname:node_3776,prsc:2|A-3561-OUT,B-3561-OUT,C-3561-OUT,D-3561-OUT,E-3561-OUT;n:type:ShaderForge.SFN_Power,id:5134,x:32624,y:33758,varname:node_5134,prsc:2|VAL-3776-OUT,EXP-1104-OUT;n:type:ShaderForge.SFN_Clamp01,id:7784,x:32836,y:33758,varname:node_7784,prsc:2|IN-5134-OUT;n:type:ShaderForge.SFN_Multiply,id:3337,x:33074,y:33651,varname:node_3337,prsc:2|A-5501-RGB,B-7784-OUT;n:type:ShaderForge.SFN_Tex2d,id:5501,x:32854,y:33577,ptovrint:False,ptlb:TransitionEffect,ptin:_TransitionEffect,varname:node_5501,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;proporder:7319-1578-2029-1220-5501;pass:END;sub:END;*/

Shader "Shader Forge/Test_Transition" {
    Properties {
        _Opacity ("Opacity", Range(0, 1)) = 0
        _FrontText ("FrontText", 2D) = "white" {}
        _BackText ("BackText", 2D) = "white" {}
        _Debris ("Debris", 2D) = "black" {}
        _TransitionEffect ("TransitionEffect", 2D) = "white" {}
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float _Opacity;
            uniform sampler2D _BackText; uniform float4 _BackText_ST;
            uniform sampler2D _FrontText; uniform float4 _FrontText_ST;
            uniform sampler2D _Debris; uniform float4 _Debris_ST;
            uniform sampler2D _TransitionEffect; uniform float4 _TransitionEffect_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 projPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _BackText_var = tex2D(_BackText,TRANSFORM_TEX(i.uv0, _BackText));
                float4 _FrontText_var = tex2D(_FrontText,TRANSFORM_TEX(i.uv0, _FrontText));
                float node_1104 = 2.5;
                float4 _Debris_var = tex2D(_Debris,TRANSFORM_TEX(i.uv0, _Debris));
                float3 node_1279 = pow(saturate(( (_Opacity*_Debris_var.rgb) > 0.5 ? (1.0-(1.0-2.0*((_Opacity*_Debris_var.rgb)-0.5))*(1.0-(sceneZ*(_Opacity/node_1104)))) : (2.0*(_Opacity*_Debris_var.rgb)*(sceneZ*(_Opacity/node_1104))) )),12.0);
                float4 _TransitionEffect_var = tex2D(_TransitionEffect,TRANSFORM_TEX(i.uv0, _TransitionEffect));
                float3 node_3561 = (node_1279*(1.0 - node_1279));
                float3 emissive = (lerp(_BackText_var.rgb,_FrontText_var.rgb,saturate(node_1279))+(_TransitionEffect_var.rgb*saturate(pow((node_3561+node_3561+node_3561+node_3561+node_3561),node_1104))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
