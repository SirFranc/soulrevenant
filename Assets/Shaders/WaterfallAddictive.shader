﻿Shader "Dixidiasoft/WaterfallAdditive" {
Properties {
	_Color ("Color", Color) = (0.5,0.5,0.5,0.5)
	_MainTex ("Diffuse", 2D) = "white" {}
	_Alpha2 ("Alpha2 (RGB)", 2D) = "white" {}
	_Alpha1 ("Alpha1 (RGB)", 2D) = "white" {}
	_Opacity ("Opacità",Range(0,1))=0.5
}
Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha One
	AlphaTest Greater .01
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _Color;
			sampler2D _Alpha1;
			sampler2D _Alpha2;
			uniform float _Alpha1CTRL;
			uniform float _Opacity;
			
			struct vertexInput {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 alpha1 :TEXCOORD1;
				float2 alpha2 :TEXCOORD2;
			};

			struct vertexOutput {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 alpha1 :TEXCOORD1;
				float2 alpha2 :TEXCOORD2;
			};
			
			float4 _MainTex_ST;
			float4 _Alpha1_ST;
			float4 _Alpha2_ST;

			vertexOutput vert (vertexInput v)
			{
				vertexOutput o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				o.alpha1 = TRANSFORM_TEX(v.alpha1,_Alpha1);
				o.alpha2 = TRANSFORM_TEX(v.alpha2,_Alpha2);
				return o;
			}

			sampler2D _CameraDepthTexture;
			
			fixed4 frag (vertexOutput i) : COLOR
			{
				float3 mainTex=_Color*tex2D(_MainTex, i.texcoord);
				float3 alpha1Tex=tex2D(_Alpha1, i.alpha1);
				float3 alpha2Tex=tex2D(_Alpha2, i.alpha2);
				float3 AlphaComb = alpha1Tex * alpha2Tex;
				
				float3 colore=2.0f * i.color * _Color * mainTex * AlphaComb;
				return float4(colore,_Opacity);
			}
			ENDCG 
		}
	}	
}
}
