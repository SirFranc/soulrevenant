Shader "Toon/Blended" {

	Properties {
		_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex ("Underlying Texture", 2D) = "white" {}
		_Color ("Toon Tint Color", Color) = (.5,.5,.5,1)
		_MainToonTex ("Main Toon Texture", 2D) = "white" {}
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "white" { Texgen CubeNormal }
	}
	
	SubShader {
		
		Pass {
			Blend One One
			AlphaTest Greater .01
			Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	
			SetTexture [_MainTex] {
				constantColor [_TintColor]
				combine constant
			}
			SetTexture [_MainTex] {
				combine texture * previous DOUBLE
			}
		}
		
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			ColorMask RGBA
			ZWrite On
			
			SetTexture [_MainToonTex] {
				constantColor [_Color]
				combine texture * constant
			}
			SetTexture [_ToonShade] {
				combine texture * previous DOUBLE, previous
			}
		}
		
	}
	
}