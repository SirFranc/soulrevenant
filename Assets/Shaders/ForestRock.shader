// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:2,spmd:0,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:32719,y:32712,varname:node_2865,prsc:2|diff-7320-OUT,spec-7007-OUT,normal-6295-OUT;n:type:ShaderForge.SFN_Tex2d,id:7736,x:31454,y:32789,ptovrint:True,ptlb:Base Color,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:25e6081ee46a07a43b43954c6169ba5c,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5964,x:31721,y:33248,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:77d332d951175d348863659e7339c8f8,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Color,id:8887,x:31454,y:32613,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_NormalVector,id:6362,x:30976,y:32955,prsc:2,pt:True;n:type:ShaderForge.SFN_ComponentMask,id:8571,x:31158,y:32958,varname:node_8571,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-6362-OUT;n:type:ShaderForge.SFN_Multiply,id:8171,x:31684,y:32958,varname:node_8171,prsc:2|A-1317-OUT,B-2773-OUT;n:type:ShaderForge.SFN_Multiply,id:4655,x:31653,y:32704,varname:node_4655,prsc:2|A-8887-RGB,B-7736-RGB;n:type:ShaderForge.SFN_Lerp,id:7320,x:32328,y:32811,varname:node_7320,prsc:2|A-7645-OUT,B-9001-RGB,T-9885-OUT;n:type:ShaderForge.SFN_Tex2d,id:9001,x:31547,y:33182,ptovrint:False,ptlb:Snow,ptin:_Snow,varname:node_2950,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9e8bf54fdad83f44fbb0f0fa23bc474a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:2773,x:31222,y:33142,ptovrint:False,ptlb:SnowAmount,ptin:_SnowAmount,varname:node_2066,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2.65263,max:10;n:type:ShaderForge.SFN_Clamp01,id:9885,x:32090,y:32958,varname:node_9885,prsc:2|IN-5047-OUT;n:type:ShaderForge.SFN_Multiply,id:1317,x:31522,y:32958,varname:node_1317,prsc:2|A-9211-OUT,B-9211-OUT;n:type:ShaderForge.SFN_Power,id:5047,x:31892,y:32958,varname:node_5047,prsc:2|VAL-8171-OUT,EXP-5043-OUT;n:type:ShaderForge.SFN_Vector1,id:5043,x:31684,y:33104,varname:node_5043,prsc:2,v1:5;n:type:ShaderForge.SFN_Desaturate,id:7645,x:32241,y:32575,varname:node_7645,prsc:2|COL-4392-OUT,DES-3717-OUT;n:type:ShaderForge.SFN_Slider,id:3717,x:31454,y:32311,ptovrint:False,ptlb:Desaturation,ptin:_Desaturation,varname:node_7000,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Tex2d,id:2073,x:31454,y:32412,ptovrint:False,ptlb:DetailText,ptin:_DetailText,varname:_MainText_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Clamp01,id:9211,x:31347,y:32958,varname:node_9211,prsc:2|IN-8571-OUT;n:type:ShaderForge.SFN_Tex2d,id:7632,x:31547,y:33388,ptovrint:False,ptlb:NormalDetail,ptin:_NormalDetail,varname:_DetailText_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:19ba82174c2679f42b646d9ba30d7c8c,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Lerp,id:6295,x:32193,y:33311,varname:node_6295,prsc:2|A-5964-RGB,B-7632-RGB,T-4207-OUT;n:type:ShaderForge.SFN_Vector1,id:4207,x:31964,y:33441,varname:node_4207,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Vector1,id:5656,x:32471,y:32631,varname:node_5656,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Vector1,id:4187,x:32198,y:33082,varname:node_4187,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:1353,x:32365,y:33026,varname:node_1353,prsc:2|A-9885-OUT,B-4187-OUT;n:type:ShaderForge.SFN_Multiply,id:7188,x:31695,y:32445,varname:node_7188,prsc:2|A-8887-RGB,B-2073-RGB;n:type:ShaderForge.SFN_Add,id:9750,x:31845,y:32687,varname:node_9750,prsc:2|A-4655-OUT,B-4655-OUT;n:type:ShaderForge.SFN_Multiply,id:4392,x:32048,y:32585,varname:node_4392,prsc:2|A-7188-OUT,B-9750-OUT;n:type:ShaderForge.SFN_AmbientLight,id:9893,x:32120,y:32353,varname:node_9893,prsc:2;n:type:ShaderForge.SFN_Lerp,id:7007,x:32538,y:32472,varname:node_7007,prsc:2|A-149-OUT,B-5656-OUT,T-9885-OUT;n:type:ShaderForge.SFN_Multiply,id:149,x:32371,y:32318,varname:node_149,prsc:2|A-3438-OUT,B-9893-RGB;n:type:ShaderForge.SFN_Vector1,id:3438,x:32176,y:32481,varname:node_3438,prsc:2,v1:0.4;proporder:5964-7736-8887-9001-2773-3717-2073-7632;pass:END;sub:END;*/

Shader "Shader Forge/ForestRock" {
    Properties {
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _MainTex ("Base Color", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Snow ("Snow", 2D) = "white" {}
        _SnowAmount ("SnowAmount", Range(0, 10)) = 2.65263
        _Desaturation ("Desaturation", Range(0, 1)) = 0.5
        _DetailText ("DetailText", 2D) = "gray" {}
        _NormalDetail ("NormalDetail", 2D) = "bump" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float4 _Color;
            uniform sampler2D _Snow; uniform float4 _Snow_ST;
            uniform float _SnowAmount;
            uniform float _Desaturation;
            uniform sampler2D _DetailText; uniform float4 _DetailText_ST;
            uniform sampler2D _NormalDetail; uniform float4 _NormalDetail_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 _NormalDetail_var = UnpackNormal(tex2D(_NormalDetail,TRANSFORM_TEX(i.uv0, _NormalDetail)));
                float3 normalLocal = lerp(_BumpMap_var.rgb,_NormalDetail_var.rgb,0.3);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 1.0 - 0.5; // Convert roughness to gloss
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                d.boxMax[0] = unity_SpecCube0_BoxMax;
                d.boxMin[0] = unity_SpecCube0_BoxMin;
                d.probePosition[0] = unity_SpecCube0_ProbePosition;
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.boxMax[1] = unity_SpecCube1_BoxMax;
                d.boxMin[1] = unity_SpecCube1_BoxMin;
                d.probePosition[1] = unity_SpecCube1_ProbePosition;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_5656 = 0.1;
                float node_9211 = saturate(normalDirection.g);
                float node_9885 = saturate(pow(((node_9211*node_9211)*_SnowAmount),5.0));
                float3 specularColor = lerp((0.4*UNITY_LIGHTMODEL_AMBIENT.rgb),float3(node_5656,node_5656,node_5656),node_9885);
                float3 directSpecular = 1 * pow(max(0,dot(reflect(-lightDirection, normalDirection),viewDirection)),specPow)*specularColor;
                float3 indirectSpecular = (gi.indirect.specular)*specularColor;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float4 _DetailText_var = tex2D(_DetailText,TRANSFORM_TEX(i.uv0, _DetailText));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_4655 = (_Color.rgb*_MainTex_var.rgb);
                float4 _Snow_var = tex2D(_Snow,TRANSFORM_TEX(i.uv0, _Snow));
                float3 diffuseColor = lerp(lerp(((_Color.rgb*_DetailText_var.rgb)*(node_4655+node_4655)),dot(((_Color.rgb*_DetailText_var.rgb)*(node_4655+node_4655)),float3(0.3,0.59,0.11)),_Desaturation),_Snow_var.rgb,node_9885);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float4 _Color;
            uniform sampler2D _Snow; uniform float4 _Snow_ST;
            uniform float _SnowAmount;
            uniform float _Desaturation;
            uniform sampler2D _DetailText; uniform float4 _DetailText_ST;
            uniform sampler2D _NormalDetail; uniform float4 _NormalDetail_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 _NormalDetail_var = UnpackNormal(tex2D(_NormalDetail,TRANSFORM_TEX(i.uv0, _NormalDetail)));
                float3 normalLocal = lerp(_BumpMap_var.rgb,_NormalDetail_var.rgb,0.3);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = 1.0 - 0.5; // Convert roughness to gloss
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float node_5656 = 0.1;
                float node_9211 = saturate(normalDirection.g);
                float node_9885 = saturate(pow(((node_9211*node_9211)*_SnowAmount),5.0));
                float3 specularColor = lerp((0.4*UNITY_LIGHTMODEL_AMBIENT.rgb),float3(node_5656,node_5656,node_5656),node_9885);
                float3 directSpecular = attenColor * pow(max(0,dot(reflect(-lightDirection, normalDirection),viewDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _DetailText_var = tex2D(_DetailText,TRANSFORM_TEX(i.uv0, _DetailText));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_4655 = (_Color.rgb*_MainTex_var.rgb);
                float4 _Snow_var = tex2D(_Snow,TRANSFORM_TEX(i.uv0, _Snow));
                float3 diffuseColor = lerp(lerp(((_Color.rgb*_DetailText_var.rgb)*(node_4655+node_4655)),dot(((_Color.rgb*_DetailText_var.rgb)*(node_4655+node_4655)),float3(0.3,0.59,0.11)),_Desaturation),_Snow_var.rgb,node_9885);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Color;
            uniform sampler2D _Snow; uniform float4 _Snow_ST;
            uniform float _SnowAmount;
            uniform float _Desaturation;
            uniform sampler2D _DetailText; uniform float4 _DetailText_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                o.Emission = 0;
                
                float4 _DetailText_var = tex2D(_DetailText,TRANSFORM_TEX(i.uv0, _DetailText));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_4655 = (_Color.rgb*_MainTex_var.rgb);
                float4 _Snow_var = tex2D(_Snow,TRANSFORM_TEX(i.uv0, _Snow));
                float node_9211 = saturate(normalDirection.g);
                float node_9885 = saturate(pow(((node_9211*node_9211)*_SnowAmount),5.0));
                float3 diffColor = lerp(lerp(((_Color.rgb*_DetailText_var.rgb)*(node_4655+node_4655)),dot(((_Color.rgb*_DetailText_var.rgb)*(node_4655+node_4655)),float3(0.3,0.59,0.11)),_Desaturation),_Snow_var.rgb,node_9885);
                float node_5656 = 0.1;
                float3 specColor = lerp((0.4*UNITY_LIGHTMODEL_AMBIENT.rgb),float3(node_5656,node_5656,node_5656),node_9885);
                o.Albedo = diffColor + specColor * 0.125; // No gloss connected. Assume it's 0.5
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
