// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:True,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32915,y:32641,varname:node_4795,prsc:2|emission-1096-OUT;n:type:ShaderForge.SFN_Tex2d,id:4578,x:31939,y:32876,ptovrint:False,ptlb:Fog02,ptin:_Fog02,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-8068-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2517,x:32353,y:32856,varname:node_2517,prsc:2|A-4578-RGB,B-4215-OUT,C-9910-RGB,D-3463-OUT,E-4930-OUT;n:type:ShaderForge.SFN_Tex2d,id:9910,x:31939,y:33067,ptovrint:False,ptlb:FogMask,ptin:_FogMask,varname:node_9910,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Panner,id:9754,x:31744,y:32703,varname:node_9754,prsc:2,spu:0.01,spv:0.005|UVIN-853-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:9401,x:31939,y:32703,ptovrint:False,ptlb:Fog01,ptin:_Fog01,varname:_MainTex_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-9754-UVOUT;n:type:ShaderForge.SFN_Panner,id:8068,x:31744,y:32876,varname:node_8068,prsc:2,spu:-0.003,spv:-0.004|UVIN-853-UVOUT;n:type:ShaderForge.SFN_Slider,id:4930,x:31864,y:33289,ptovrint:False,ptlb:OpacityMultiply,ptin:_OpacityMultiply,varname:node_4930,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:5;n:type:ShaderForge.SFN_DepthBlend,id:3463,x:32173,y:33112,varname:node_3463,prsc:2|DIST-6592-OUT;n:type:ShaderForge.SFN_Slider,id:6592,x:31864,y:33387,ptovrint:False,ptlb:Softness,ptin:_Softness,varname:_OpacityMultiply_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:10;n:type:ShaderForge.SFN_Color,id:1416,x:32001,y:32507,ptovrint:False,ptlb:Fog_Color,ptin:_Fog_Color,varname:node_1416,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:1096,x:32525,y:32793,varname:node_1096,prsc:2|A-1416-RGB,B-2517-OUT;n:type:ShaderForge.SFN_TexCoord,id:853,x:31407,y:32731,varname:node_853,prsc:2,uv:0;n:type:ShaderForge.SFN_AmbientLight,id:1408,x:31442,y:32414,varname:node_1408,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4215,x:31635,y:32534,varname:node_4215,prsc:2|A-1408-RGB,B-5371-OUT;n:type:ShaderForge.SFN_Vector1,id:5371,x:31418,y:32578,varname:node_5371,prsc:2,v1:0.5;proporder:4578-9910-9401-4930-6592-1416;pass:END;sub:END;*/

Shader "Shader Forge/DistantClouds" {
    Properties {
        _Fog02 ("Fog02", 2D) = "white" {}
        _FogMask ("FogMask", 2D) = "white" {}
        _Fog01 ("Fog01", 2D) = "white" {}
        _OpacityMultiply ("OpacityMultiply", Range(0, 5)) = 0.5
        _Softness ("Softness", Range(1, 10)) = 1
        _Fog_Color ("Fog_Color", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform sampler2D _Fog02; uniform float4 _Fog02_ST;
            uniform sampler2D _FogMask; uniform float4 _FogMask_ST;
            uniform float _OpacityMultiply;
            uniform float _Softness;
            uniform float4 _Fog_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 projPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
////// Lighting:
////// Emissive:
                float4 node_8488 = _Time + _TimeEditor;
                float2 node_8068 = (i.uv0+node_8488.g*float2(-0.003,-0.004));
                float4 _Fog02_var = tex2D(_Fog02,TRANSFORM_TEX(node_8068, _Fog02));
                float4 _FogMask_var = tex2D(_FogMask,TRANSFORM_TEX(i.uv0, _FogMask));
                float3 emissive = (_Fog_Color.rgb*(_Fog02_var.rgb*(UNITY_LIGHTMODEL_AMBIENT.rgb*0.5)*_FogMask_var.rgb*saturate((sceneZ-partZ)/_Softness)*_OpacityMultiply));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
