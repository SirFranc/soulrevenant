// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:3,qpre:2,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|diff-104-OUT,spec-2481-OUT,gloss-8271-OUT,normal-3529-OUT,alpha-3674-OUT,refract-9560-OUT;n:type:ShaderForge.SFN_DepthBlend,id:6373,x:33263,y:33078,varname:node_6373,prsc:2|DIST-7430-OUT;n:type:ShaderForge.SFN_Slider,id:7430,x:32919,y:33117,ptovrint:False,ptlb:Fade Strength,ptin:_FadeStrength,varname:_FadeStrength,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Get,id:3674,x:32495,y:32971,varname:node_3674,prsc:2|IN-8441-OUT;n:type:ShaderForge.SFN_Set,id:8441,x:34619,y:33024,varname:BaseOpacity,prsc:2|IN-4114-OUT;n:type:ShaderForge.SFN_Color,id:2741,x:32253,y:31424,ptovrint:False,ptlb:Water Color,ptin:_WaterColor,varname:_WaterColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2078432,c2:0.5254902,c3:0.6117647,c4:0.659;n:type:ShaderForge.SFN_Slider,id:2005,x:33106,y:33219,ptovrint:False,ptlb:Max Opacity,ptin:_MaxOpacity,varname:_MaxOpacity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Clamp,id:657,x:33576,y:33072,varname:node_657,prsc:2|IN-6373-OUT,MIN-7592-OUT,MAX-2005-OUT;n:type:ShaderForge.SFN_Vector1,id:7592,x:33263,y:33021,varname:node_7592,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2dAsset,id:7308,x:33044,y:33489,ptovrint:False,ptlb:Water Normal,ptin:_WaterNormal,varname:_WaterNormal,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8666308f7856b624e8dec1d7288151e9,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:3956,x:33337,y:33415,varname:node_3956,prsc:2,tex:8666308f7856b624e8dec1d7288151e9,ntxv:0,isnm:False|UVIN-8130-OUT,TEX-7308-TEX;n:type:ShaderForge.SFN_Tex2d,id:9007,x:33337,y:33538,varname:node_9007,prsc:2,tex:8666308f7856b624e8dec1d7288151e9,ntxv:0,isnm:False|UVIN-8946-OUT,TEX-7308-TEX;n:type:ShaderForge.SFN_ComponentMask,id:7426,x:34203,y:33507,varname:node_7426,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-6495-OUT;n:type:ShaderForge.SFN_Get,id:9560,x:32495,y:33022,varname:node_9560,prsc:2|IN-5025-OUT;n:type:ShaderForge.SFN_Set,id:5025,x:35247,y:33526,varname:Refraction,prsc:2|IN-6965-OUT;n:type:ShaderForge.SFN_Slider,id:9610,x:34000,y:33422,ptovrint:False,ptlb:Refraction Power,ptin:_RefractionPower,varname:_RefractionPower,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:6113,x:34371,y:33496,varname:node_6113,prsc:2|A-9610-OUT,B-7426-OUT;n:type:ShaderForge.SFN_TexCoord,id:9795,x:32160,y:33410,varname:node_9795,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:8030,x:32528,y:33410,varname:node_8030,prsc:2,spu:0.07,spv:0.12|UVIN-3181-OUT;n:type:ShaderForge.SFN_Panner,id:8195,x:32528,y:33561,varname:node_8195,prsc:2,spu:-0.13,spv:-0.05|UVIN-7173-OUT;n:type:ShaderForge.SFN_Add,id:9678,x:33554,y:33485,varname:node_9678,prsc:2|A-3956-RGB,B-9007-RGB;n:type:ShaderForge.SFN_Multiply,id:265,x:34543,y:33475,varname:node_265,prsc:2|A-6327-OUT,B-6113-OUT;n:type:ShaderForge.SFN_Vector1,id:6327,x:34331,y:33422,varname:node_6327,prsc:2,v1:0.05;n:type:ShaderForge.SFN_TexCoord,id:9935,x:32160,y:33563,varname:node_9935,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:3181,x:32375,y:33410,varname:node_3181,prsc:2|A-9795-UVOUT,B-412-OUT;n:type:ShaderForge.SFN_Multiply,id:7173,x:32363,y:33563,varname:node_7173,prsc:2|A-9935-UVOUT,B-9809-OUT;n:type:ShaderForge.SFN_Vector1,id:412,x:32160,y:33348,varname:node_412,prsc:2,v1:0.9;n:type:ShaderForge.SFN_Vector1,id:9809,x:32160,y:33710,varname:node_9809,prsc:2,v1:1.3;n:type:ShaderForge.SFN_Add,id:8130,x:32831,y:33429,varname:node_8130,prsc:2|A-8030-UVOUT,B-1554-OUT;n:type:ShaderForge.SFN_Add,id:8946,x:32831,y:33571,varname:node_8946,prsc:2|A-8195-UVOUT,B-1554-OUT;n:type:ShaderForge.SFN_Append,id:3740,x:32707,y:33874,varname:node_3740,prsc:2|A-2910-OUT,B-44-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3820,x:32160,y:33812,ptovrint:False,ptlb:Flow X,ptin:_FlowX,varname:_FlowX,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_ValueProperty,id:907,x:32160,y:33897,ptovrint:False,ptlb:Flow Y,ptin:_FlowY,varname:_FlowY,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Get,id:1554,x:32645,y:33504,varname:node_1554,prsc:2|IN-1524-OUT;n:type:ShaderForge.SFN_Set,id:1524,x:32892,y:33874,varname:Flow,prsc:2|IN-3740-OUT;n:type:ShaderForge.SFN_Time,id:9718,x:32160,y:33962,varname:node_9718,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2910,x:32363,y:33812,varname:node_2910,prsc:2|A-3820-OUT,B-9718-TSL;n:type:ShaderForge.SFN_Multiply,id:44,x:32363,y:33941,varname:node_44,prsc:2|A-907-OUT,B-9718-TSL;n:type:ShaderForge.SFN_Get,id:7149,x:31748,y:31808,varname:node_7149,prsc:2|IN-8283-OUT;n:type:ShaderForge.SFN_Set,id:8283,x:33044,y:33035,varname:Fade,prsc:2|IN-7430-OUT;n:type:ShaderForge.SFN_DepthBlend,id:3639,x:32306,y:31795,varname:node_3639,prsc:2|DIST-8691-OUT;n:type:ShaderForge.SFN_Multiply,id:8691,x:32128,y:31795,varname:node_8691,prsc:2|A-7149-OUT,B-3685-OUT;n:type:ShaderForge.SFN_Lerp,id:151,x:32737,y:31662,varname:node_151,prsc:2|A-4858-OUT,B-2650-OUT,T-3639-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3685,x:31748,y:31886,ptovrint:False,ptlb:Shore Fade,ptin:_ShoreFade,varname:_ShoreFade,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:480,x:31748,y:31973,ptovrint:False,ptlb:Depth Fade,ptin:_DepthFade,varname:_DepthFade,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Lerp,id:6622,x:32977,y:31662,varname:node_6622,prsc:2|A-151-OUT,B-9679-OUT,T-4168-OUT;n:type:ShaderForge.SFN_DepthBlend,id:4168,x:32306,y:31973,varname:node_4168,prsc:2|DIST-3421-OUT;n:type:ShaderForge.SFN_Multiply,id:3421,x:32128,y:31973,varname:node_3421,prsc:2|A-7149-OUT,B-4118-OUT;n:type:ShaderForge.SFN_Add,id:4118,x:31925,y:31973,varname:node_4118,prsc:2|A-480-OUT,B-3685-OUT;n:type:ShaderForge.SFN_Get,id:7082,x:33242,y:32950,varname:node_7082,prsc:2|IN-596-OUT;n:type:ShaderForge.SFN_Set,id:596,x:32456,y:32009,varname:DeepWater,prsc:2|IN-4168-OUT;n:type:ShaderForge.SFN_Add,id:6437,x:33807,y:33045,varname:node_6437,prsc:2|A-3607-OUT,B-657-OUT;n:type:ShaderForge.SFN_Clamp01,id:1928,x:33992,y:33045,varname:node_1928,prsc:2|IN-6437-OUT;n:type:ShaderForge.SFN_Multiply,id:9846,x:33471,y:32946,varname:node_9846,prsc:2|A-2005-OUT,B-7082-OUT;n:type:ShaderForge.SFN_Multiply,id:9679,x:32692,y:31506,varname:node_9679,prsc:2|A-2741-RGB,B-1810-OUT;n:type:ShaderForge.SFN_Vector1,id:1810,x:32489,y:31531,varname:node_1810,prsc:2,v1:0.6;n:type:ShaderForge.SFN_Tex2dAsset,id:1732,x:31472,y:31509,ptovrint:False,ptlb:Shore Line,ptin:_ShoreLine,varname:_ShoreLine,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4383,x:31698,y:31384,varname:node_4383,prsc:2,ntxv:0,isnm:False|UVIN-5088-OUT,TEX-1732-TEX;n:type:ShaderForge.SFN_Tex2d,id:9441,x:31683,y:31610,varname:node_9441,prsc:2,ntxv:0,isnm:False|UVIN-4522-OUT,TEX-1732-TEX;n:type:ShaderForge.SFN_Add,id:1780,x:32078,y:31516,varname:node_1780,prsc:2|A-941-OUT,B-8142-OUT;n:type:ShaderForge.SFN_Set,id:3453,x:32810,y:33374,varname:Panning1,prsc:2|IN-8030-UVOUT;n:type:ShaderForge.SFN_Set,id:6006,x:32810,y:33703,varname:Panning2,prsc:2|IN-8195-UVOUT;n:type:ShaderForge.SFN_Set,id:4952,x:33037,y:33677,varname:Flow2,prsc:2|IN-8946-OUT;n:type:ShaderForge.SFN_Set,id:149,x:33044,y:33385,varname:Flow1,prsc:2|IN-8130-OUT;n:type:ShaderForge.SFN_Get,id:5088,x:31451,y:31420,varname:node_5088,prsc:2|IN-3453-OUT;n:type:ShaderForge.SFN_Get,id:4522,x:31451,y:31664,varname:node_4522,prsc:2|IN-6006-OUT;n:type:ShaderForge.SFN_Multiply,id:3607,x:33655,y:32946,varname:node_3607,prsc:2|A-8719-OUT,B-9846-OUT;n:type:ShaderForge.SFN_Vector1,id:8719,x:33405,y:32862,varname:node_8719,prsc:2,v1:0.7;n:type:ShaderForge.SFN_Set,id:7814,x:33791,y:31530,varname:Albedo,prsc:2|IN-5302-OUT;n:type:ShaderForge.SFN_Get,id:104,x:32506,y:32689,varname:node_104,prsc:2|IN-7814-OUT;n:type:ShaderForge.SFN_Multiply,id:941,x:31893,y:31384,varname:node_941,prsc:2|A-4383-RGB,B-4383-RGB;n:type:ShaderForge.SFN_Multiply,id:8142,x:31893,y:31610,varname:node_8142,prsc:2|A-9441-RGB,B-9441-RGB;n:type:ShaderForge.SFN_Vector1,id:5171,x:32078,y:31447,varname:node_5171,prsc:2,v1:0.7;n:type:ShaderForge.SFN_Multiply,id:4858,x:32253,y:31569,varname:node_4858,prsc:2|A-1780-OUT,B-5171-OUT;n:type:ShaderForge.SFN_Slider,id:8645,x:31778,y:31283,ptovrint:False,ptlb:Clearness,ptin:_Clearness,varname:_Clearness,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1.5;n:type:ShaderForge.SFN_Tex2d,id:6985,x:31472,y:31167,varname:node_6985,prsc:2,ntxv:0,isnm:False|UVIN-8797-OUT,TEX-1119-TEX;n:type:ShaderForge.SFN_Tex2d,id:9468,x:31494,y:30992,varname:node_9468,prsc:2,ntxv:0,isnm:False|UVIN-269-OUT,TEX-1119-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:1119,x:31272,y:31049,ptovrint:False,ptlb:Clear Water,ptin:_ClearWater,varname:_ClearWater,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Blend,id:4262,x:31688,y:31066,varname:node_4262,prsc:2,blmd:12,clmp:True|SRC-9468-RGB,DST-6985-RGB;n:type:ShaderForge.SFN_Add,id:8026,x:31923,y:31084,varname:node_8026,prsc:2|A-4262-OUT,B-4262-OUT;n:type:ShaderForge.SFN_Multiply,id:1892,x:32134,y:31189,varname:node_1892,prsc:2|A-8026-OUT,B-8645-OUT;n:type:ShaderForge.SFN_Add,id:2650,x:32522,y:31309,varname:node_2650,prsc:2|A-1250-OUT,B-2741-RGB;n:type:ShaderForge.SFN_FragmentPosition,id:6608,x:32994,y:32509,varname:node_6608,prsc:2;n:type:ShaderForge.SFN_Panner,id:7694,x:33581,y:32455,varname:node_7694,prsc:2,spu:0.07,spv:0.12|UVIN-945-OUT;n:type:ShaderForge.SFN_Panner,id:4940,x:33581,y:32608,varname:node_4940,prsc:2,spu:-0.13,spv:-0.05|UVIN-6950-OUT;n:type:ShaderForge.SFN_Multiply,id:945,x:33402,y:32455,varname:node_945,prsc:2|A-6296-OUT,B-1736-OUT;n:type:ShaderForge.SFN_Multiply,id:6950,x:33390,y:32608,varname:node_6950,prsc:2|A-6296-OUT,B-6528-OUT;n:type:ShaderForge.SFN_Vector1,id:6528,x:33187,y:32755,varname:node_6528,prsc:2,v1:0.4;n:type:ShaderForge.SFN_Vector1,id:1736,x:33187,y:32393,varname:node_1736,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Set,id:8211,x:33838,y:32419,varname:WorldUV1,prsc:2|IN-7694-UVOUT;n:type:ShaderForge.SFN_Set,id:6570,x:33837,y:32748,varname:WorldUV2,prsc:2|IN-4940-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:6296,x:33187,y:32499,varname:node_6296,prsc:2,cc1:2,cc2:0,cc3:-1,cc4:-1|IN-6608-XYZ;n:type:ShaderForge.SFN_Get,id:269,x:31287,y:30976,varname:node_269,prsc:2|IN-8211-OUT;n:type:ShaderForge.SFN_Get,id:8797,x:31272,y:31217,varname:node_8797,prsc:2|IN-6570-OUT;n:type:ShaderForge.SFN_Lerp,id:4227,x:33195,y:31660,varname:node_4227,prsc:2|A-6622-OUT,B-5323-OUT,T-7769-OUT;n:type:ShaderForge.SFN_Multiply,id:5323,x:32943,y:31495,varname:node_5323,prsc:2|A-99-OUT,B-9679-OUT;n:type:ShaderForge.SFN_Vector1,id:99,x:32737,y:31442,varname:node_99,prsc:2,v1:0.6;n:type:ShaderForge.SFN_Multiply,id:305,x:32254,y:32155,varname:node_305,prsc:2|A-7149-OUT,B-6993-OUT;n:type:ShaderForge.SFN_Add,id:6993,x:32074,y:32166,varname:node_6993,prsc:2|A-4118-OUT,B-480-OUT;n:type:ShaderForge.SFN_DepthBlend,id:7769,x:32419,y:32155,varname:node_7769,prsc:2|DIST-305-OUT;n:type:ShaderForge.SFN_VertexColor,id:1302,x:31308,y:30814,varname:node_1302,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9586,x:31641,y:30708,varname:node_9586,prsc:2|A-7681-OUT,B-1302-G;n:type:ShaderForge.SFN_Tex2dAsset,id:7890,x:30878,y:30650,ptovrint:False,ptlb:Flow Texture,ptin:_FlowTexture,varname:_FlowTexture,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Get,id:8754,x:30878,y:30575,varname:node_8754,prsc:2|IN-149-OUT;n:type:ShaderForge.SFN_Get,id:7057,x:30878,y:30813,varname:node_7057,prsc:2|IN-4952-OUT;n:type:ShaderForge.SFN_Tex2d,id:8361,x:31138,y:30590,varname:node_8361,prsc:2,ntxv:0,isnm:False|UVIN-8754-OUT,TEX-7890-TEX;n:type:ShaderForge.SFN_Tex2d,id:5558,x:31130,y:30750,varname:node_5558,prsc:2,ntxv:0,isnm:False|UVIN-7057-OUT,TEX-7890-TEX;n:type:ShaderForge.SFN_Add,id:7681,x:31401,y:30616,varname:node_7681,prsc:2|A-8361-RGB,B-5558-RGB;n:type:ShaderForge.SFN_Add,id:5695,x:33391,y:31564,varname:node_5695,prsc:2|A-5382-OUT,B-4227-OUT;n:type:ShaderForge.SFN_Multiply,id:130,x:31912,y:30730,varname:node_130,prsc:2|A-9586-OUT,B-9062-OUT;n:type:ShaderForge.SFN_Get,id:9062,x:31620,y:30835,varname:node_9062,prsc:2|IN-848-OUT;n:type:ShaderForge.SFN_Set,id:848,x:32474,y:31840,varname:Foam,prsc:2|IN-3639-OUT;n:type:ShaderForge.SFN_Multiply,id:1105,x:32107,y:30762,varname:node_1105,prsc:2|A-130-OUT,B-130-OUT;n:type:ShaderForge.SFN_Slider,id:2480,x:31890,y:30645,ptovrint:False,ptlb:Flow Intensity,ptin:_FlowIntensity,varname:_FlowIntensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:8839,x:32329,y:30778,varname:node_8839,prsc:2|A-2480-OUT,B-1105-OUT;n:type:ShaderForge.SFN_Get,id:3529,x:32506,y:32806,varname:node_3529,prsc:2|IN-7333-OUT;n:type:ShaderForge.SFN_Set,id:7333,x:34223,y:33650,varname:Normals,prsc:2|IN-6495-OUT;n:type:ShaderForge.SFN_NormalVector,id:3222,x:32918,y:32184,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:6205,x:33096,y:32185,varname:node_6205,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-3222-OUT;n:type:ShaderForge.SFN_OneMinus,id:6728,x:33281,y:32188,varname:node_6728,prsc:2|IN-6205-OUT;n:type:ShaderForge.SFN_Vector1,id:5595,x:33655,y:32295,varname:node_5595,prsc:2,v1:1;n:type:ShaderForge.SFN_Add,id:5024,x:33473,y:32175,varname:node_5024,prsc:2|A-6728-OUT,B-6728-OUT;n:type:ShaderForge.SFN_Add,id:1314,x:33655,y:32175,varname:node_1314,prsc:2|A-5024-OUT,B-5024-OUT;n:type:ShaderForge.SFN_Set,id:497,x:34405,y:32158,varname:WaterfallMask,prsc:2|IN-9442-OUT;n:type:ShaderForge.SFN_AmbientLight,id:9994,x:31316,y:33538,varname:node_9994,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:8271,x:32389,y:32770,varname:node_8271,prsc:2;n:type:ShaderForge.SFN_Multiply,id:482,x:31621,y:33552,varname:node_482,prsc:2|A-9994-RGB,B-6040-OUT;n:type:ShaderForge.SFN_Add,id:6250,x:31297,y:33714,varname:node_6250,prsc:2|A-7932-OUT,B-7150-OUT;n:type:ShaderForge.SFN_Vector1,id:7932,x:31110,y:33687,varname:node_7932,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Slider,id:7150,x:30953,y:33765,ptovrint:False,ptlb:Specular Strength,ptin:_SpecularStrength,varname:_SpecularStrength,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Multiply,id:5382,x:32530,y:30821,varname:node_5382,prsc:2|A-8905-OUT,B-8839-OUT;n:type:ShaderForge.SFN_Get,id:6702,x:31767,y:30931,varname:node_6702,prsc:2|IN-497-OUT;n:type:ShaderForge.SFN_OneMinus,id:8905,x:31974,y:30931,varname:node_8905,prsc:2|IN-6702-OUT;n:type:ShaderForge.SFN_Clamp01,id:9041,x:34066,y:32183,varname:node_9041,prsc:2|IN-1310-OUT;n:type:ShaderForge.SFN_Multiply,id:1250,x:32322,y:31204,varname:node_1250,prsc:2|A-8905-OUT,B-1892-OUT;n:type:ShaderForge.SFN_Add,id:9948,x:33537,y:33749,varname:node_9948,prsc:2|A-896-RGB,B-4826-RGB;n:type:ShaderForge.SFN_VertexColor,id:1491,x:33518,y:33895,varname:node_1491,prsc:2;n:type:ShaderForge.SFN_Lerp,id:3216,x:33769,y:33653,varname:node_3216,prsc:2|A-9948-OUT,B-9678-OUT,T-1491-G;n:type:ShaderForge.SFN_Lerp,id:6495,x:34026,y:33704,varname:node_6495,prsc:2|A-3216-OUT,B-830-OUT,T-8984-OUT;n:type:ShaderForge.SFN_Get,id:8984,x:33748,y:33785,varname:node_8984,prsc:2|IN-497-OUT;n:type:ShaderForge.SFN_Set,id:391,x:31994,y:33718,varname:Specular,prsc:2|IN-482-OUT;n:type:ShaderForge.SFN_Get,id:2481,x:32506,y:32738,varname:node_2481,prsc:2|IN-391-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:181,x:32053,y:34191,varname:node_181,prsc:2;n:type:ShaderForge.SFN_ComponentMask,id:3114,x:32325,y:34075,varname:node_3114,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-181-XYZ;n:type:ShaderForge.SFN_ComponentMask,id:6537,x:32325,y:34284,varname:node_6537,prsc:2,cc1:1,cc2:2,cc3:-1,cc4:-1|IN-181-XYZ;n:type:ShaderForge.SFN_NormalVector,id:825,x:32325,y:34427,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:5427,x:32490,y:34427,varname:node_5427,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-825-OUT;n:type:ShaderForge.SFN_Clamp01,id:1168,x:32817,y:34427,varname:node_1168,prsc:2|IN-7666-OUT;n:type:ShaderForge.SFN_Power,id:7666,x:32660,y:34427,varname:node_7666,prsc:2|VAL-5427-OUT,EXP-373-OUT;n:type:ShaderForge.SFN_Vector1,id:373,x:32325,y:34596,varname:node_373,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:6603,x:33495,y:34167,varname:node_6603,prsc:2|A-5064-RGB,B-9985-RGB,T-1168-OUT;n:type:ShaderForge.SFN_Tex2d,id:896,x:33314,y:33719,varname:node_896,prsc:2,tex:8666308f7856b624e8dec1d7288151e9,ntxv:0,isnm:False|UVIN-8030-UVOUT,TEX-7308-TEX;n:type:ShaderForge.SFN_Tex2d,id:4826,x:33325,y:33890,varname:node_4826,prsc:2,tex:8666308f7856b624e8dec1d7288151e9,ntxv:0,isnm:False|UVIN-8195-UVOUT,TEX-7308-TEX;n:type:ShaderForge.SFN_Tex2d,id:5064,x:33274,y:34072,varname:node_5064,prsc:2,tex:8666308f7856b624e8dec1d7288151e9,ntxv:0,isnm:False|UVIN-7696-UVOUT,TEX-7308-TEX;n:type:ShaderForge.SFN_Tex2d,id:9985,x:33274,y:34264,varname:node_9985,prsc:2,tex:8666308f7856b624e8dec1d7288151e9,ntxv:0,isnm:False|UVIN-9731-UVOUT,TEX-7308-TEX;n:type:ShaderForge.SFN_Panner,id:7696,x:33064,y:34072,varname:node_7696,prsc:2,spu:0,spv:2|UVIN-3319-OUT;n:type:ShaderForge.SFN_Panner,id:9731,x:33064,y:34264,varname:node_9731,prsc:2,spu:2,spv:0|UVIN-5546-OUT;n:type:ShaderForge.SFN_Multiply,id:3319,x:32852,y:34072,varname:node_3319,prsc:2|A-3114-OUT,B-5315-OUT;n:type:ShaderForge.SFN_Multiply,id:5546,x:32852,y:34264,varname:node_5546,prsc:2|A-5315-OUT,B-6537-OUT;n:type:ShaderForge.SFN_Vector1,id:5315,x:32671,y:34192,varname:node_5315,prsc:2,v1:0.7;n:type:ShaderForge.SFN_Add,id:1310,x:33838,y:32194,varname:node_1310,prsc:2|A-1314-OUT,B-1314-OUT,C-1314-OUT;n:type:ShaderForge.SFN_Multiply,id:830,x:33731,y:34113,varname:node_830,prsc:2|A-6603-OUT,B-8537-OUT;n:type:ShaderForge.SFN_Vector1,id:8537,x:33480,y:34048,varname:node_8537,prsc:2,v1:3;n:type:ShaderForge.SFN_Tex2d,id:6709,x:33954,y:33205,ptovrint:False,ptlb:Waterfall alpha,ptin:_Waterfallalpha,varname:node_6709,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2765,x:34186,y:33142,varname:node_2765,prsc:2|A-1928-OUT,B-6709-RGB;n:type:ShaderForge.SFN_Get,id:4071,x:34165,y:33081,varname:node_4071,prsc:2|IN-497-OUT;n:type:ShaderForge.SFN_Lerp,id:4114,x:34373,y:33055,varname:node_4114,prsc:2|A-1928-OUT,B-2765-OUT,T-4071-OUT;n:type:ShaderForge.SFN_Multiply,id:6467,x:34666,y:33710,varname:node_6467,prsc:2|A-6113-OUT,B-918-OUT;n:type:ShaderForge.SFN_Lerp,id:5567,x:34863,y:33608,varname:node_5567,prsc:2|A-265-OUT,B-6467-OUT,T-2653-OUT;n:type:ShaderForge.SFN_Get,id:2653,x:34548,y:33626,varname:node_2653,prsc:2|IN-497-OUT;n:type:ShaderForge.SFN_Multiply,id:918,x:34497,y:33730,varname:node_918,prsc:2|A-6327-OUT,B-6709-RGB;n:type:ShaderForge.SFN_Add,id:5302,x:33577,y:31491,varname:node_5302,prsc:2|A-8902-OUT,B-5695-OUT;n:type:ShaderForge.SFN_Get,id:6777,x:33024,y:31322,varname:node_6777,prsc:2|IN-497-OUT;n:type:ShaderForge.SFN_Vector1,id:8518,x:33162,y:31407,varname:node_8518,prsc:2,v1:1.2;n:type:ShaderForge.SFN_Multiply,id:8902,x:33394,y:31379,varname:node_8902,prsc:2|A-5082-OUT,B-8518-OUT;n:type:ShaderForge.SFN_Multiply,id:5082,x:33228,y:31259,varname:node_5082,prsc:2|A-6777-OUT,B-8148-OUT;n:type:ShaderForge.SFN_Get,id:8148,x:33024,y:31229,varname:node_8148,prsc:2|IN-5813-OUT;n:type:ShaderForge.SFN_Tex2d,id:7622,x:31175,y:30244,varname:node_7622,prsc:2,ntxv:0,isnm:False|UVIN-9348-UVOUT,TEX-7890-TEX;n:type:ShaderForge.SFN_TexCoord,id:1047,x:30121,y:30218,varname:node_1047,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:9348,x:30609,y:30223,varname:node_9348,prsc:2,spu:0.07,spv:1.2|UVIN-4353-OUT;n:type:ShaderForge.SFN_Panner,id:2988,x:30609,y:30374,varname:node_2988,prsc:2,spu:-0.13,spv:0.8|UVIN-4655-OUT;n:type:ShaderForge.SFN_TexCoord,id:5313,x:30121,y:30371,varname:node_5313,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:4353,x:30444,y:30223,varname:node_4353,prsc:2|A-1047-UVOUT,B-9198-OUT;n:type:ShaderForge.SFN_Multiply,id:4655,x:30444,y:30376,varname:node_4655,prsc:2|A-5313-UVOUT,B-3951-OUT;n:type:ShaderForge.SFN_Add,id:7979,x:31415,y:30348,varname:node_7979,prsc:2|A-7622-RGB,B-4054-RGB;n:type:ShaderForge.SFN_Set,id:5813,x:31601,y:30348,varname:WaterfallFroth,prsc:2|IN-7979-OUT;n:type:ShaderForge.SFN_Vector2,id:9198,x:30229,y:30126,varname:node_9198,prsc:2,v1:2,v2:0.7;n:type:ShaderForge.SFN_Vector2,id:3951,x:30228,y:30501,varname:node_3951,prsc:2,v1:1.5,v2:1.2;n:type:ShaderForge.SFN_Tex2d,id:4054,x:31151,y:30409,varname:node_4054,prsc:2,ntxv:0,isnm:False|UVIN-2988-UVOUT,TEX-7890-TEX;n:type:ShaderForge.SFN_Multiply,id:6040,x:31472,y:33702,varname:node_6040,prsc:2|A-6250-OUT,B-9247-OUT;n:type:ShaderForge.SFN_Vector1,id:9247,x:31225,y:33897,varname:node_9247,prsc:2,v1:2;n:type:ShaderForge.SFN_ToggleProperty,id:5942,x:34066,y:32063,ptovrint:False,ptlb:EnableWaterfall,ptin:_EnableWaterfall,varname:node_5942,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Multiply,id:9442,x:34252,y:32108,varname:node_9442,prsc:2|A-5942-OUT,B-9041-OUT;n:type:ShaderForge.SFN_Multiply,id:6965,x:35034,y:33476,varname:node_6965,prsc:2|A-8694-OUT,B-5567-OUT;n:type:ShaderForge.SFN_Multiply,id:8694,x:34740,y:33247,varname:node_8694,prsc:2|A-4114-OUT,B-5070-OUT;n:type:ShaderForge.SFN_Vector1,id:5070,x:34554,y:33306,varname:node_5070,prsc:2,v1:2;proporder:2741-7308-1732-1119-7890-7430-2005-9610-8645-2480-7150-3820-907-3685-480-6709-5942;pass:END;sub:END;*/

Shader "Shader Forge/River_Advanced" {
    Properties {
        _WaterColor ("Water Color", Color) = (0.2078432,0.5254902,0.6117647,0.659)
        _WaterNormal ("Water Normal", 2D) = "bump" {}
        _ShoreLine ("Shore Line", 2D) = "white" {}
        _ClearWater ("Clear Water", 2D) = "black" {}
        _FlowTexture ("Flow Texture", 2D) = "black" {}
        _FadeStrength ("Fade Strength", Range(0, 1)) = 0
        _MaxOpacity ("Max Opacity", Range(0, 1)) = 0.5
        _RefractionPower ("Refraction Power", Range(0, 1)) = 0.5
        _Clearness ("Clearness", Range(0, 1.5)) = 0.5
        _FlowIntensity ("Flow Intensity", Range(0, 1)) = 0
        _SpecularStrength ("Specular Strength", Range(0, 1)) = 0.5
        _FlowX ("Flow X", Float ) = 0.2
        _FlowY ("Flow Y", Float ) = 0
        _ShoreFade ("Shore Fade", Float ) = 2
        _DepthFade ("Depth Fade", Float ) = 1
        _Waterfallalpha ("Waterfall alpha", 2D) = "white" {}
        [MaterialToggle] _EnableWaterfall ("EnableWaterfall", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="AlphaTest+3"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float _FadeStrength;
            uniform float4 _WaterColor;
            uniform float _MaxOpacity;
            uniform sampler2D _WaterNormal; uniform float4 _WaterNormal_ST;
            uniform float _RefractionPower;
            uniform float _FlowX;
            uniform float _FlowY;
            uniform float _ShoreFade;
            uniform float _DepthFade;
            uniform sampler2D _ShoreLine; uniform float4 _ShoreLine_ST;
            uniform float _Clearness;
            uniform sampler2D _ClearWater; uniform float4 _ClearWater_ST;
            uniform sampler2D _FlowTexture; uniform float4 _FlowTexture_ST;
            uniform float _FlowIntensity;
            uniform float _SpecularStrength;
            uniform sampler2D _Waterfallalpha; uniform float4 _Waterfallalpha_ST;
            uniform fixed _EnableWaterfall;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD6;
                UNITY_FOG_COORDS(7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_1045 = _Time + _TimeEditor;
                float2 node_8030 = ((i.uv0*0.9)+node_1045.g*float2(0.07,0.12));
                float3 node_896 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8030, _WaterNormal)));
                float2 node_8195 = ((i.uv0*1.3)+node_1045.g*float2(-0.13,-0.05));
                float3 node_4826 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8195, _WaterNormal)));
                float4 node_9718 = _Time + _TimeEditor;
                float2 Flow = float2((_FlowX*node_9718.r),(_FlowY*node_9718.r));
                float2 node_1554 = Flow;
                float2 node_8130 = (node_8030+node_1554);
                float3 node_3956 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8130, _WaterNormal)));
                float2 node_8946 = (node_8195+node_1554);
                float3 node_9007 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8946, _WaterNormal)));
                float node_5315 = 0.7;
                float2 node_7696 = ((i.posWorld.rgb.rg*node_5315)+node_1045.g*float2(0,2));
                float3 node_5064 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_7696, _WaterNormal)));
                float2 node_9731 = ((node_5315*i.posWorld.rgb.gb)+node_1045.g*float2(2,0));
                float3 node_9985 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_9731, _WaterNormal)));
                float node_6728 = (1.0 - i.normalDir.g);
                float node_5024 = (node_6728+node_6728);
                float node_1314 = (node_5024+node_5024);
                float WaterfallMask = (_EnableWaterfall*saturate((node_1314+node_1314+node_1314)));
                float3 node_6495 = lerp(lerp((node_896.rgb+node_4826.rgb),(node_3956.rgb+node_9007.rgb),i.vertexColor.g),(lerp(node_5064.rgb,node_9985.rgb,saturate(pow(i.normalDir.r,1.0)))*3.0),WaterfallMask);
                float3 Normals = node_6495;
                float3 normalLocal = Normals;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float Fade = _FadeStrength;
                float node_7149 = Fade;
                float node_4118 = (_DepthFade+_ShoreFade);
                float node_4168 = saturate((sceneZ-partZ)/(node_7149*node_4118));
                float DeepWater = node_4168;
                float node_1928 = saturate(((0.7*(_MaxOpacity*DeepWater))+clamp(saturate((sceneZ-partZ)/_FadeStrength),0.0,_MaxOpacity)));
                float4 _Waterfallalpha_var = tex2D(_Waterfallalpha,TRANSFORM_TEX(i.uv0, _Waterfallalpha));
                float3 node_4114 = lerp(float3(node_1928,node_1928,node_1928),(node_1928*_Waterfallalpha_var.rgb),WaterfallMask);
                float node_6327 = 0.05;
                float2 node_6113 = (_RefractionPower*node_6495.rg);
                float3 Refraction = ((node_4114*2.0)*lerp(float3((node_6327*node_6113),0.0),(float3(node_6113,0.0)*(node_6327*_Waterfallalpha_var.rgb)),WaterfallMask));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + Refraction.rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = attenuation;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 node_482 = (UNITY_LIGHTMODEL_AMBIENT.rgb*((1.5+_SpecularStrength)*2.0));
                float3 Specular = node_482;
                float3 specularColor = Specular;
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float2 node_9348 = ((i.uv0*float2(2,0.7))+node_1045.g*float2(0.07,1.2));
                float4 node_7622 = tex2D(_FlowTexture,TRANSFORM_TEX(node_9348, _FlowTexture));
                float2 node_2988 = ((i.uv0*float2(1.5,1.2))+node_1045.g*float2(-0.13,0.8));
                float4 node_4054 = tex2D(_FlowTexture,TRANSFORM_TEX(node_2988, _FlowTexture));
                float3 WaterfallFroth = (node_7622.rgb+node_4054.rgb);
                float node_8905 = (1.0 - WaterfallMask);
                float2 Flow1 = node_8130;
                float2 node_8754 = Flow1;
                float4 node_8361 = tex2D(_FlowTexture,TRANSFORM_TEX(node_8754, _FlowTexture));
                float2 Flow2 = node_8946;
                float2 node_7057 = Flow2;
                float4 node_5558 = tex2D(_FlowTexture,TRANSFORM_TEX(node_7057, _FlowTexture));
                float node_3639 = saturate((sceneZ-partZ)/(node_7149*_ShoreFade));
                float Foam = node_3639;
                float3 node_130 = (((node_8361.rgb+node_5558.rgb)*i.vertexColor.g)*Foam);
                float2 Panning1 = node_8030;
                float2 node_5088 = Panning1;
                float4 node_4383 = tex2D(_ShoreLine,TRANSFORM_TEX(node_5088, _ShoreLine));
                float2 Panning2 = node_8195;
                float2 node_4522 = Panning2;
                float4 node_9441 = tex2D(_ShoreLine,TRANSFORM_TEX(node_4522, _ShoreLine));
                float2 node_6296 = i.posWorld.rgb.br;
                float2 WorldUV1 = ((node_6296*0.3)+node_1045.g*float2(0.07,0.12));
                float2 node_269 = WorldUV1;
                float4 node_9468 = tex2D(_ClearWater,TRANSFORM_TEX(node_269, _ClearWater));
                float2 WorldUV2 = ((node_6296*0.4)+node_1045.g*float2(-0.13,-0.05));
                float2 node_8797 = WorldUV2;
                float4 node_6985 = tex2D(_ClearWater,TRANSFORM_TEX(node_8797, _ClearWater));
                float3 node_4262 = saturate((node_9468.rgb > 0.5 ?  (1.0-(1.0-2.0*(node_9468.rgb-0.5))*(1.0-node_6985.rgb)) : (2.0*node_9468.rgb*node_6985.rgb)) );
                float3 node_9679 = (_WaterColor.rgb*0.6);
                float3 Albedo = (((WaterfallMask*WaterfallFroth)*1.2)+((node_8905*(_FlowIntensity*(node_130*node_130)))+lerp(lerp(lerp((((node_4383.rgb*node_4383.rgb)+(node_9441.rgb*node_9441.rgb))*0.7),((node_8905*((node_4262+node_4262)*_Clearness))+_WaterColor.rgb),node_3639),node_9679,node_4168),(0.6*node_9679),saturate((sceneZ-partZ)/(node_7149*(node_4118+_DepthFade))))));
                float3 diffuseColor = Albedo;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                float3 BaseOpacity = node_4114;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,BaseOpacity),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _GrabTexture;
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float _FadeStrength;
            uniform float4 _WaterColor;
            uniform float _MaxOpacity;
            uniform sampler2D _WaterNormal; uniform float4 _WaterNormal_ST;
            uniform float _RefractionPower;
            uniform float _FlowX;
            uniform float _FlowY;
            uniform float _ShoreFade;
            uniform float _DepthFade;
            uniform sampler2D _ShoreLine; uniform float4 _ShoreLine_ST;
            uniform float _Clearness;
            uniform sampler2D _ClearWater; uniform float4 _ClearWater_ST;
            uniform sampler2D _FlowTexture; uniform float4 _FlowTexture_ST;
            uniform float _FlowIntensity;
            uniform float _SpecularStrength;
            uniform sampler2D _Waterfallalpha; uniform float4 _Waterfallalpha_ST;
            uniform fixed _EnableWaterfall;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_556 = _Time + _TimeEditor;
                float2 node_8030 = ((i.uv0*0.9)+node_556.g*float2(0.07,0.12));
                float3 node_896 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8030, _WaterNormal)));
                float2 node_8195 = ((i.uv0*1.3)+node_556.g*float2(-0.13,-0.05));
                float3 node_4826 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8195, _WaterNormal)));
                float4 node_9718 = _Time + _TimeEditor;
                float2 Flow = float2((_FlowX*node_9718.r),(_FlowY*node_9718.r));
                float2 node_1554 = Flow;
                float2 node_8130 = (node_8030+node_1554);
                float3 node_3956 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8130, _WaterNormal)));
                float2 node_8946 = (node_8195+node_1554);
                float3 node_9007 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_8946, _WaterNormal)));
                float node_5315 = 0.7;
                float2 node_7696 = ((i.posWorld.rgb.rg*node_5315)+node_556.g*float2(0,2));
                float3 node_5064 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_7696, _WaterNormal)));
                float2 node_9731 = ((node_5315*i.posWorld.rgb.gb)+node_556.g*float2(2,0));
                float3 node_9985 = UnpackNormal(tex2D(_WaterNormal,TRANSFORM_TEX(node_9731, _WaterNormal)));
                float node_6728 = (1.0 - i.normalDir.g);
                float node_5024 = (node_6728+node_6728);
                float node_1314 = (node_5024+node_5024);
                float WaterfallMask = (_EnableWaterfall*saturate((node_1314+node_1314+node_1314)));
                float3 node_6495 = lerp(lerp((node_896.rgb+node_4826.rgb),(node_3956.rgb+node_9007.rgb),i.vertexColor.g),(lerp(node_5064.rgb,node_9985.rgb,saturate(pow(i.normalDir.r,1.0)))*3.0),WaterfallMask);
                float3 Normals = node_6495;
                float3 normalLocal = Normals;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float Fade = _FadeStrength;
                float node_7149 = Fade;
                float node_4118 = (_DepthFade+_ShoreFade);
                float node_4168 = saturate((sceneZ-partZ)/(node_7149*node_4118));
                float DeepWater = node_4168;
                float node_1928 = saturate(((0.7*(_MaxOpacity*DeepWater))+clamp(saturate((sceneZ-partZ)/_FadeStrength),0.0,_MaxOpacity)));
                float4 _Waterfallalpha_var = tex2D(_Waterfallalpha,TRANSFORM_TEX(i.uv0, _Waterfallalpha));
                float3 node_4114 = lerp(float3(node_1928,node_1928,node_1928),(node_1928*_Waterfallalpha_var.rgb),WaterfallMask);
                float node_6327 = 0.05;
                float2 node_6113 = (_RefractionPower*node_6495.rg);
                float3 Refraction = ((node_4114*2.0)*lerp(float3((node_6327*node_6113),0.0),(float3(node_6113,0.0)*(node_6327*_Waterfallalpha_var.rgb)),WaterfallMask));
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + Refraction.rg;
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = attenuation;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 node_482 = (UNITY_LIGHTMODEL_AMBIENT.rgb*((1.5+_SpecularStrength)*2.0));
                float3 Specular = node_482;
                float3 specularColor = Specular;
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float2 node_9348 = ((i.uv0*float2(2,0.7))+node_556.g*float2(0.07,1.2));
                float4 node_7622 = tex2D(_FlowTexture,TRANSFORM_TEX(node_9348, _FlowTexture));
                float2 node_2988 = ((i.uv0*float2(1.5,1.2))+node_556.g*float2(-0.13,0.8));
                float4 node_4054 = tex2D(_FlowTexture,TRANSFORM_TEX(node_2988, _FlowTexture));
                float3 WaterfallFroth = (node_7622.rgb+node_4054.rgb);
                float node_8905 = (1.0 - WaterfallMask);
                float2 Flow1 = node_8130;
                float2 node_8754 = Flow1;
                float4 node_8361 = tex2D(_FlowTexture,TRANSFORM_TEX(node_8754, _FlowTexture));
                float2 Flow2 = node_8946;
                float2 node_7057 = Flow2;
                float4 node_5558 = tex2D(_FlowTexture,TRANSFORM_TEX(node_7057, _FlowTexture));
                float node_3639 = saturate((sceneZ-partZ)/(node_7149*_ShoreFade));
                float Foam = node_3639;
                float3 node_130 = (((node_8361.rgb+node_5558.rgb)*i.vertexColor.g)*Foam);
                float2 Panning1 = node_8030;
                float2 node_5088 = Panning1;
                float4 node_4383 = tex2D(_ShoreLine,TRANSFORM_TEX(node_5088, _ShoreLine));
                float2 Panning2 = node_8195;
                float2 node_4522 = Panning2;
                float4 node_9441 = tex2D(_ShoreLine,TRANSFORM_TEX(node_4522, _ShoreLine));
                float2 node_6296 = i.posWorld.rgb.br;
                float2 WorldUV1 = ((node_6296*0.3)+node_556.g*float2(0.07,0.12));
                float2 node_269 = WorldUV1;
                float4 node_9468 = tex2D(_ClearWater,TRANSFORM_TEX(node_269, _ClearWater));
                float2 WorldUV2 = ((node_6296*0.4)+node_556.g*float2(-0.13,-0.05));
                float2 node_8797 = WorldUV2;
                float4 node_6985 = tex2D(_ClearWater,TRANSFORM_TEX(node_8797, _ClearWater));
                float3 node_4262 = saturate((node_9468.rgb > 0.5 ?  (1.0-(1.0-2.0*(node_9468.rgb-0.5))*(1.0-node_6985.rgb)) : (2.0*node_9468.rgb*node_6985.rgb)) );
                float3 node_9679 = (_WaterColor.rgb*0.6);
                float3 Albedo = (((WaterfallMask*WaterfallFroth)*1.2)+((node_8905*(_FlowIntensity*(node_130*node_130)))+lerp(lerp(lerp((((node_4383.rgb*node_4383.rgb)+(node_9441.rgb*node_9441.rgb))*0.7),((node_8905*((node_4262+node_4262)*_Clearness))+_WaterColor.rgb),node_3639),node_9679,node_4168),(0.6*node_9679),saturate((sceneZ-partZ)/(node_7149*(node_4118+_DepthFade))))));
                float3 diffuseColor = Albedo;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                float3 BaseOpacity = node_4114;
                fixed4 finalRGBA = fixed4(finalColor * BaseOpacity,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
