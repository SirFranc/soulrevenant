// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.17 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.17;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:-499,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4013,x:32854,y:32730,varname:node_4013,prsc:2|diff-6396-OUT,spec-4420-OUT,gloss-9118-OUT,normal-9061-OUT,alpha-1394-A;n:type:ShaderForge.SFN_Color,id:1304,x:32029,y:32779,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:1394,x:31917,y:32604,ptovrint:False,ptlb:MainText,ptin:_MainText,varname:node_1394,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fc5c46c2ac38b7b4bb6f0fd7fd986649,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3137,x:31903,y:32938,ptovrint:False,ptlb:DetailText,ptin:_DetailText,varname:node_3137,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:272e5a8ce774b0843b3a389a6aea111a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5352,x:31903,y:33137,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_5352,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:1155,x:31903,y:33323,ptovrint:False,ptlb:DetailNormal,ptin:_DetailNormal,varname:node_1155,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:1456,x:32329,y:32695,varname:node_1456,prsc:2|A-1394-RGB,B-1304-RGB;n:type:ShaderForge.SFN_Multiply,id:9320,x:32359,y:32876,varname:node_9320,prsc:2|A-1304-RGB,B-3137-RGB;n:type:ShaderForge.SFN_Blend,id:6396,x:32533,y:32764,varname:node_6396,prsc:2,blmd:10,clmp:True|SRC-1456-OUT,DST-9320-OUT;n:type:ShaderForge.SFN_Multiply,id:8857,x:32240,y:33140,varname:node_8857,prsc:2|A-1094-OUT,B-5352-RGB;n:type:ShaderForge.SFN_Multiply,id:6423,x:32240,y:33317,varname:node_6423,prsc:2|A-1094-OUT,B-1155-RGB;n:type:ShaderForge.SFN_Slider,id:1094,x:32051,y:33064,ptovrint:False,ptlb:NormalIntensity,ptin:_NormalIntensity,varname:node_1094,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Blend,id:9061,x:32488,y:33140,varname:node_9061,prsc:2,blmd:10,clmp:True|SRC-8857-OUT,DST-6423-OUT;n:type:ShaderForge.SFN_AmbientLight,id:8061,x:32296,y:32518,varname:node_8061,prsc:2;n:type:ShaderForge.SFN_Slider,id:9118,x:32163,y:32455,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_NormalIntensity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:4420,x:32586,y:32536,varname:node_4420,prsc:2|A-9680-OUT,B-8061-RGB;n:type:ShaderForge.SFN_Slider,id:9680,x:32163,y:32363,ptovrint:False,ptlb:SpecularIntensity,ptin:_SpecularIntensity,varname:_Gloss_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;proporder:1304-1394-3137-5352-1094-1155-9118-9680;pass:END;sub:END;*/

Shader "Shader Forge/AlphaSortedTransparent" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainText ("MainText", 2D) = "white" {}
        _DetailText ("DetailText", 2D) = "white" {}
        _Normal ("Normal", 2D) = "bump" {}
        _NormalIntensity ("NormalIntensity", Range(0, 1)) = 1
        _DetailNormal ("DetailNormal", 2D) = "bump" {}
        _Gloss ("Gloss", Range(0, 1)) = 1
        _SpecularIntensity ("SpecularIntensity", Range(0, 1)) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent-499"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainText; uniform float4 _MainText_ST;
            uniform sampler2D _DetailText; uniform float4 _DetailText_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _DetailNormal; uniform float4 _DetailNormal_ST;
            uniform float _NormalIntensity;
            uniform float _Gloss;
            uniform float _SpecularIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 _DetailNormal_var = UnpackNormal(tex2D(_DetailNormal,TRANSFORM_TEX(i.uv0, _DetailNormal)));
                float3 normalLocal = saturate(( (_NormalIntensity*_DetailNormal_var.rgb) > 0.5 ? (1.0-(1.0-2.0*((_NormalIntensity*_DetailNormal_var.rgb)-0.5))*(1.0-(_NormalIntensity*_Normal_var.rgb))) : (2.0*(_NormalIntensity*_DetailNormal_var.rgb)*(_NormalIntensity*_Normal_var.rgb)) ));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = (_SpecularIntensity*UNITY_LIGHTMODEL_AMBIENT.rgb);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _MainText_var = tex2D(_MainText,TRANSFORM_TEX(i.uv0, _MainText));
                float4 _DetailText_var = tex2D(_DetailText,TRANSFORM_TEX(i.uv0, _DetailText));
                float3 diffuseColor = saturate(( (_Color.rgb*_DetailText_var.rgb) > 0.5 ? (1.0-(1.0-2.0*((_Color.rgb*_DetailText_var.rgb)-0.5))*(1.0-(_MainText_var.rgb*_Color.rgb))) : (2.0*(_Color.rgb*_DetailText_var.rgb)*(_MainText_var.rgb*_Color.rgb)) ));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor,_MainText_var.a);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainText; uniform float4 _MainText_ST;
            uniform sampler2D _DetailText; uniform float4 _DetailText_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _DetailNormal; uniform float4 _DetailNormal_ST;
            uniform float _NormalIntensity;
            uniform float _Gloss;
            uniform float _SpecularIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 _DetailNormal_var = UnpackNormal(tex2D(_DetailNormal,TRANSFORM_TEX(i.uv0, _DetailNormal)));
                float3 normalLocal = saturate(( (_NormalIntensity*_DetailNormal_var.rgb) > 0.5 ? (1.0-(1.0-2.0*((_NormalIntensity*_DetailNormal_var.rgb)-0.5))*(1.0-(_NormalIntensity*_Normal_var.rgb))) : (2.0*(_NormalIntensity*_DetailNormal_var.rgb)*(_NormalIntensity*_Normal_var.rgb)) ));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = (_SpecularIntensity*UNITY_LIGHTMODEL_AMBIENT.rgb);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _MainText_var = tex2D(_MainText,TRANSFORM_TEX(i.uv0, _MainText));
                float4 _DetailText_var = tex2D(_DetailText,TRANSFORM_TEX(i.uv0, _DetailText));
                float3 diffuseColor = saturate(( (_Color.rgb*_DetailText_var.rgb) > 0.5 ? (1.0-(1.0-2.0*((_Color.rgb*_DetailText_var.rgb)-0.5))*(1.0-(_MainText_var.rgb*_Color.rgb))) : (2.0*(_Color.rgb*_DetailText_var.rgb)*(_MainText_var.rgb*_Color.rgb)) ));
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * _MainText_var.a,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
