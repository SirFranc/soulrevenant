using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class FilterHierarchyWindow : EditorWindow {
	
	static FilterHierarchyWindow curWindow;
	List<GameObject> selectedObj;
	List<Component> presentComp;

	GameObject groupObj;
	string grpName="New Group";
	bool[]	toggle = new bool[25];

	
	//seleziono gli oggetti
	//prendo la lista dei componenti presenti nella lista di oggetti
	//seleziono i componenti che deve avere
	//deselezioni gli oggetti che non rientrano in qiesta lista
	//creo un nuovo oggetto e ci metto dentro quelli rimasti
	
	#region Main methods
	[MenuItem("Luca's Tool/FilterHierarchy")]
	static void Init()
	{
		//funzione chiamata al click della voce nel menu
		curWindow=(FilterHierarchyWindow)EditorWindow.GetWindow(typeof(FilterHierarchyWindow));
        curWindow.titleContent = new GUIContent("Filter Hierarchy Window");
		curWindow.minSize=new Vector2(300,180);
		curWindow.maxSize=new Vector2(350,768);
	}
	
	void DrawComponentToggle(int i){
		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		GUILayout.EndHorizontal();
	}
	void OnGUI()
	{
		selectedObj=Selection.gameObjects.ToList();

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		
		GUILayout.Label("Filter Hierarchy Window:",EditorStyles.boldLabel);
		
		//disegno i toggle per i singoli componenti

		
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		DrawCompElement(selectedObj);
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();


		GUILayout.Space(10);

		GUIContent tooltipA= new GUIContent("Filter selection","Lascia nella selezione solo gli oggetti con i componenti selezionati");
		if(selectedObj.Count>0)
		{
			if(GUILayout.Button(tooltipA,GUILayout.Height(40)))
			{
				FilterSelection(selectedObj,toggle,presentComp);
			}
		}


		GUIContent tooltipB= new GUIContent("Filter by exclusion","Esclude solo gli oggetti che hanno anche un solo componente tra quelli selezionati");
		if(selectedObj.Count>0)
		{
			if(GUILayout.Button(tooltipB,GUILayout.Height(40)))
			{
				FilterByExclusion(selectedObj,toggle,presentComp);
			}
		}


		GUIContent tooltipC= new GUIContent("Filter by selected","Lascia nella selezione solo quelli con i componenti selezionati");
		if(selectedObj.Count>0)
		{
			if(GUILayout.Button(tooltipC,GUILayout.Height(40)))
			{
				FilterBySelected(selectedObj,toggle,presentComp);
			}
		}

		
		
		GUILayout.Space(30);


		//definisco il bottone e la sua funzione

		//object field
		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		GUILayout.BeginVertical();
		groupObj=(GameObject)EditorGUILayout.ObjectField("Set group object",groupObj,typeof(GameObject),true);
		GUILayout.EndVertical();
		GUILayout.Space(10);
		GUILayout.EndHorizontal();



		GUIContent tooltip= new GUIContent("Parent to Existing Object","Parent all selected object to previous specified object");
		if(selectedObj.Count>0)
		{
			if(GUILayout.Button(tooltip,GUILayout.Height(40)))
			{
				ParentToExistingObject(selectedObj);
			}
		}



		GUILayout.Space(30);



		if(selectedObj.Count>0)
		{
			grpName=EditorGUILayout.TextField(grpName);
			if(GUILayout.Button("Parent to New Group",GUILayout.Height(40))&& grpName!=null)
			{
				ParentToNewObject(selectedObj);
			}
		}

		
		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		
		
		Repaint();
	}
	

	private void DrawCompElement(List<GameObject> selectedObj)
		//disegna un toggle per ogni componente di un'oggetto
	{
		if(selectedObj.Count!=0 )
		{

			presentComp =EditorUtils.ExistingComponents(selectedObj);
			for (int i=0;i<presentComp.Count;i++) 
			{
				
				GUILayout.BeginHorizontal();
				GUILayout.BeginVertical();
				
				string nameType=presentComp[i].GetType().ToString();

				//TAGLIO LA PARTE DI NOME CHE NON MI INTERESSA
				if(nameType.Contains("."))
				{
					int index=nameType.LastIndexOf(".")+1;
					nameType=nameType.Substring(index);
				}

				toggle[i]=GUILayout.Toggle(toggle[i],nameType);
//				else if(obj==destinationObj)
//				{
//					toggleMaintains[i]=GUILayout.Toggle(toggleMaintains[i],nameType);
//				}
				
				
				
				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
				
			}
		}
	}



	private void FilterSelection(List<GameObject> selectedObj,bool[] toggle,List<Component> presentComp)
	{
		List<GameObject> newSelection=new List<GameObject>();
		List<Component> checkedComp=CheckedComponents(toggle,presentComp);

		foreach (GameObject obj  in selectedObj) {
			if(EditorUtils.CheckIfObjHasComponent(obj,checkedComp))
			{
				newSelection.Add(obj);
			}
		}
		Selection.objects=newSelection.ToArray();
	}


	private void FilterByExclusion(List<GameObject> selectedObj,bool[] toggle,List<Component> presentComp)
	{
		List<Component> checkedComp=CheckedComponents(toggle,presentComp);
		List<GameObject> newSelection=new List<GameObject>();

		foreach (GameObject obj  in selectedObj) {
			if(!EditorUtils.CheckIfObjHasOneOfComponent(obj,checkedComp))
			{
				newSelection.Add(obj);
			}
		}

		Selection.objects=newSelection.ToArray();
	}

	private void FilterBySelected(List<GameObject> selectedObj,bool[] toggle,List<Component> presentComp)
	{
		List<Component> checkedComp=CheckedComponents(toggle,presentComp);
		List<GameObject> newSelection=new List<GameObject>();
		
		foreach (GameObject obj  in selectedObj) {
			if(EditorUtils.CheckIfObjHasThisComponent(obj,checkedComp))
			{
				newSelection.Add(obj);
			}
		}
		
		Selection.objects=newSelection.ToArray();
	}


	private void ParentToExistingObject(List<GameObject> selectedObj)
	{
		try {
			EditorUtils.GroupObject(selectedObj,groupObj);
			grpName=null;
		} catch {
			Debug.Log("You need to set group object to execute this operation");
		}

	}

	private void ParentToNewObject(List<GameObject> selectedObj)
	{
		GameObject newGrp=new GameObject();
		Undo.RegisterCreatedObjectUndo(newGrp,"new grp created");
		if(grpName==null)grpName="New Group";
		newGrp.name=grpName;
		EditorUtils.GroupObject(selectedObj,newGrp);

	}


	#endregion	


	#region Utility
	private List<Component> CheckedComponents(bool[] toggle,List<Component> presentComp)
	{
		List<Component> checkedComp=new List<Component>();
		
		for (int i = 0; i < presentComp.Count; i++) {
			if(toggle[i])
			{
				checkedComp.Add(presentComp.ElementAt(i));
			}
		}
		return checkedComp;
	}

	#endregion
	
}
