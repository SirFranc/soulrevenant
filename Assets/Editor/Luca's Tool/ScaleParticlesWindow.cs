﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ScaleParticlesWindow : EditorWindow {

	static ScaleParticlesWindow curWindow;
	GameObject userDefObj;
	float mScaleAmount;
	


	#region Main methods
	[MenuItem("Luca's Tool/ScaleParticles")]
	static void Init()
	{
		//funzione chiamata al click della voce nel menu
		curWindow=(ScaleParticlesWindow)EditorWindow.GetWindow(typeof(ScaleParticlesWindow));
        curWindow.titleContent = new GUIContent("Scale Particles");
		curWindow.minSize=new Vector2(436,170);
		curWindow.maxSize=new Vector2(436,170);
	}

	void OnGUI()
	{

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

		GUILayout.Label("Scale Particles",EditorStyles.boldLabel);
		//object field
		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		GUILayout.BeginVertical();
		userDefObj=(GameObject)EditorGUILayout.ObjectField("Set object to scale",userDefObj,typeof(GameObject),true);
		GUILayout.EndVertical();
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//float filed
		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		mScaleAmount=(float)EditorGUILayout.FloatField("Scale Amount",mScaleAmount);
		GUILayout.EndHorizontal();

		//definisco il bottone e la sua funzione
		if(userDefObj!=null)
		{
			if(GUILayout.Button("Scale",GUILayout.Height(40)))
			{
				EditorUtils.ScaleParticles(userDefObj,mScaleAmount);
			}
		}



		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		

		Repaint();
	}
	#endregion
	
}
