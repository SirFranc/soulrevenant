﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class MultyCopyComponentsWindow : EditorWindow {

	static MultyCopyComponentsWindow curWindow;
	GameObject sourceObj;
	GameObject destinationObj;

	bool[]	toggleCopy = new bool[20];
	bool[]  toggleMaintains =new bool[20];

	//prende un game object in ingresso
	//prende un game object in uscita
	//prende la lista dei componenti del game object
	//copia i componenti nel destinazione


    

	#region Main methods
	[MenuItem("Luca's Tool/MultyCopyComponents")]
	static void Init()
	{
		//funzione chiamata al click della voce nel menu
		curWindow=(MultyCopyComponentsWindow)EditorWindow.GetWindow(typeof(MultyCopyComponentsWindow));
        curWindow.titleContent = new GUIContent("Multy Copy Components");
		curWindow.minSize=new Vector2(436,180);
		curWindow.maxSize=new Vector2(436,768);
	}

	void DrawComponentToggle(int i){
		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		//mComponent[i]=(bool)EditorGUILayout.Toggle(typeof(mComponent),mComponent[i]);
		GUILayout.EndHorizontal();
	}

	void OnGUI()
	{

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

		GUILayout.Label("Multy Copy Component:",EditorStyles.boldLabel);
		//object field
		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		GUILayout.BeginVertical();
		sourceObj=(GameObject)EditorGUILayout.ObjectField("Set sorce object",sourceObj,typeof(GameObject),true);
		GUILayout.EndVertical();
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Space(10);
		GUILayout.BeginVertical();
		destinationObj=(GameObject)EditorGUILayout.ObjectField("Set destination object",destinationObj,typeof(GameObject),true);
		GUILayout.EndVertical();
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//disegno i toggle per i singoli componenti
		GUILayout.Space(10);

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		GUILayout.Label("Set components to copy",EditorStyles.boldLabel);
		GUILayout.Space(5);
		DrawCompElement(sourceObj);
		GUILayout.EndVertical();
		GUILayout.BeginVertical();
		GUILayout.Label("Set components to maintains",EditorStyles.boldLabel);
		GUILayout.Space(5);
		DrawCompElement(destinationObj);
		GUILayout.EndVertical();

		GUILayout.EndHorizontal();


		GUILayout.Space(20);

		//definisco il bottone e la sua funzione
		if(sourceObj!=null && destinationObj!=null)
		{
			if(GUILayout.Button("Copy all components in destination",GUILayout.Height(40)))
			{
				EditorUtils.MultyCopyComponents(sourceObj,destinationObj,toggleCopy,true);
			}

			if(GUILayout.Button("Copy selected components in destination",GUILayout.Height(40)))
			{
				EditorUtils.MultyCopyComponents(sourceObj,destinationObj,toggleCopy,false);
			}
		}

		if(destinationObj!=null)
		{
			if(GUILayout.Button("Clear selected components in destination",GUILayout.Height(40)))
			{
				EditorUtils.ClearComponents(destinationObj,toggleMaintains,false);
			}
			if(GUILayout.Button("Clear all component in destinatio",GUILayout.Height(40)))
			{
				EditorUtils.ClearComponents(destinationObj,toggleMaintains,true);
			}
		}

		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		

		Repaint();
	}

	private void DrawCompElement(GameObject obj)
		//disegna un toggle per ogni componente di un'oggetto
	{
		if(obj)
		{
			Component[] listComp=EditorUtils.ListComponents(obj);
			
			for (int i=0;i<listComp.Length;i++) 
			{
				
				GUILayout.BeginHorizontal();
				GUILayout.BeginVertical();
				
				string nameType=listComp[i].GetType().ToString();
				if(nameType.Contains("."))
				{
					int index=nameType.LastIndexOf(".")+1;
					nameType=nameType.Substring(index);
				}

				if(obj==sourceObj)
				{
					toggleCopy[i]=GUILayout.Toggle(toggleCopy[i],nameType);
				}
				else if(obj==destinationObj)
				{
					toggleMaintains[i]=GUILayout.Toggle(toggleMaintains[i],nameType);
				}

				
				
				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
				
			}
		}
	}
	#endregion
	
}
